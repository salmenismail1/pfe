import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../Services/login.service';
import { AppVarsGlobal } from '../../app.vars.global';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    visibility:boolean;
}


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(private vars: AppVarsGlobal) { }

  ngOnInit() {
    this.vars.type= this.vars.login();

     const ROUTES: RouteInfo[] = [
  
      { path: '/admin/dashboard', title: 'Dashboard',  icon: 'design_app', class: '',visibility :(this.vars.type!="client")},
      { path: '/admin/user', title: 'user',  icon:'users_single-02', class: '',visibility :(this.vars.type!="client") },
      { path: '/admin/user-profile', title: 'User Profile',  icon:'users_single-02', class: '',visibility :true },
      { path: '/admin/hotel', title: 'hotel',  icon:'users_single-02', class: '',visibility :(this.vars.type!="client") },
      { path: '/admin/voyage', title: 'voyage',  icon:'travel_istanbul', class: '',visibility :(this.vars.type!="client") },
      { path: '/admin/soiree', title: 'soiree',  icon:'clothes_tie-bow', class: '',visibility :(this.vars.type!="client") },
      { path: '/admin/circuit', title: 'circuit',  icon:'objects_planet', class: '',visibility :(this.vars.type!="client") },
      { path: '/admin/omra', title: 'omra',  icon:'users_single-02', class: '',visibility :(this.vars.type!="client") },
      { path: '/admin/contact', title: 'contact',  icon:'files_paper', class: '',visibility :(this.vars.type!="client") },

      { path: '/admin/home', title: 'home',  icon:'users_single-02', class: '' ,visibility :(this.vars.type!="client")},
      { path: '/admin/Historique', title: 'Historique',  icon:'files_single-copy-04', class: '' ,visibility :(this.vars.type=="client")},
      { path: '/admin/ReservationEnCour', title: 'Reservation en cour',  icon:'files_box', class: '' ,visibility :(this.vars.type=="client")},
      { path: '/admin/ReservationAccepter', title: 'Reservation accepter ',  icon:'files_box', class: '' ,visibility :(this.vars.type=="client")},
    
    
    
    ];
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ( window.innerWidth > 991) {
          return false;
      }
      return true;
  };
}
