import { Pipe, PipeTransform } from '@angular/core';
import { formatDate } from '@angular/common';

@Pipe({
  name: 'dateFilterSoiree'
})
export class DateFilterSoireePipe implements PipeTransform {

  transform(ListeVoyages: any, text:string): any {
    
    if( text === undefined) return ListeVoyages;

        return ListeVoyages.filter(function (v) {
          console.log(v.departsoirees[0].date);
          return (
          
          formatDate( new Date(v.departsoirees[0].date), 'yyyy/MM/dd', 'en'))>= (formatDate(text, 'yyyy/MM/dd', 'en'));
        })

  }

}
