import { Pipe, PipeTransform } from '@angular/core';
import { HotelsService } from '../Services/hotels.service';


@Pipe({
  name: 'filterprix'
})
export class FilterprixPipe implements PipeTransform {
  
  transform(ListeHotels: any, text1:number,text2:number,h:HotelsService): any {
    
    if( text1 === undefined && text2===undefined) return ListeHotels;
      return ListeHotels.filter(function (hotel) {
        let prix; 
        
   
    prix= h.getPrix(hotel) ;
  
  
    if(text1==undefined)
        return (text2>=prix);
    if(text2==undefined)
        return (text1<=prix);
    return (prix>=text1 && prix<=text2)
      })
    
    
      
  }

}
