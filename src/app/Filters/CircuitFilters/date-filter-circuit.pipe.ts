import { Pipe, PipeTransform } from '@angular/core';
import { formatDate } from '@angular/common';

@Pipe({
  name: 'dateFilterCircuit'
})
export class DateFilterCircuitPipe implements PipeTransform {

  transform(ListeCircuit: any, text:string): any {
    
    if( text === undefined || ListeCircuit===undefined) return ListeCircuit;

        return ListeCircuit.filter(function (v) {
          console.log(formatDate(text, 'yyyy/MM/dd', 'en')+"/////////////"+formatDate(v.datedepart, 'yyyy/MM/dd', 'en'));
          return (
          
          formatDate( v.datedepart, 'yyyy/MM/dd', 'en'))>= (formatDate(text, 'yyyy/MM/dd', 'en'));
        })

  }
}