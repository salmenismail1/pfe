import { Pipe, PipeTransform } from '@angular/core';
import { formatDate } from '@angular/common';

@Pipe({
  name: 'soireeDatePipe'
})
export class SoireeDatePipePipe implements PipeTransform {

  transform(soiree: any, text:string): any {
    if( text === undefined) return null;
      return soiree.filter(function (s) {return (formatDate(s.date, 'yyyy/MM/dd', 'en')).toLowerCase().includes(text.toLocaleLowerCase());})
    
  }
  

}
