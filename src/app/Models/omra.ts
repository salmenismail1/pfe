export class Omra {
    id:string;
    titre:string;
    date:string;
    prix:number;
    programme:string;
    agent:string;
    image:string[];
}
