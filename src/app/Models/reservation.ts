import { Chambre } from './chambre';
export class Reservation {
    
    
    IdClient:string;
    Etat:string;
    Titre:string;
    Nom: any;
    Email:any;
    Tel:any;
    DateDepart: string;
    DatRetour :string;
    DateReservation:string;
    Localisation:string;
    Prix:number;
    Chambre :  Chambre[];
    
    
}
