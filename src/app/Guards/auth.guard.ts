import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../Services/login.service';
import { LoginComponent as ModalComponent } from '../Components/login/login.component';
import { MatDialog, MatDialogConfig } from '@angular/material';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(public matDialog: MatDialog,private auth: LoginService,
    private router: Router) { }

    
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(!this.auth.loggedIn()){
        return true;
    } 
    this.openModal();
    return false;

  }
  openModal() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modal-component";
    this.matDialog.open(ModalComponent, {
      height: '600px',
      width: '500px',
    });
    
  }
}
