import { OmramodifComponent } from './Components/DashboardAdmin/Omra/omramodif/omramodif.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './DashbordBars/components.module';

import { AppComponent } from './app.component';

import { AdminLayoutComponent } from './Components/layouts/admin-layout/admin-layout.component';


import { BrowserModule } from '@angular/platform-browser';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';


import { NavbarComponent } from './Components/navbar/navbar.component';
import { FooterComponent } from './Components/footer/footer.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HomeComponent } from './Components/home/home.component';
import { ContactComponent } from './Components/contact/contact.component';
import { LoginComponent as ModalComponent } from './Components/login/login.component';
import { VoyageComponent } from './Components/voyage/voyage.component';
import { HotelsComponent } from './Components/hotels/hotels.component';
import { CalendarModule, DatePickerModule, TimePickerModule, DateRangePickerModule, DateTimePickerModule } from '@syncfusion/ej2-angular-calendars';
import { CircuitComponent } from './Components/circuit/circuit.component';
import { HreservationComponent } from './Components/hreservation/hreservation.component';
import { VreservationComponent } from './Components/vreservation/vreservation.component';

import { MatCardModule,MatSidenavModule,MatInputModule,MatDividerModule,MatListModule, MatTableModule,MatToolbarModule, MatMenuModule,MatIconModule,MatCheckboxModule,MatTabsModule, MatProgressSpinnerModule, MatGridListModule,MatSnackBarModule} from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { LayoutModule } from '@angular/cdk/layout';


import { FiltersPipe } from './Filters/filters.pipe';
import { FiltersearshPipe } from './Filters/filtersearsh.pipe';
import { FilteretoilePipe } from './Filters/filteretoile.pipe';
import { FilterprixPipe } from './Filters/filterprix.pipe';
import { SoireeComponent } from './Components/soiree/soiree.component';
import { SafeHtmlPipe } from './Filters/safe-html.pipe';
import { DureeVoyagePipe } from './Filters/duree-voyage.pipe';
import { MatSortModule } from '@angular/material/sort';
import { CircuitReservationComponent } from './Components/circuit-reservation/circuit-reservation.component';
import { SoireeReservationComponent } from './Components/soiree-reservation/soiree-reservation.component';
import { SoireeDatePipePipe } from './Filters/soiree-date-pipe.pipe';
import { RechercheFilterOmraPipe } from './Filters/OmraFilters/recherche-filter-omra.pipe';
import { RechercheFilterVoyagePipe } from './Filters/VoyageFilters/recherche-filter-voyage.pipe';
import { RechercheFilterSoireePipe } from './Filters/SoireeFilters/recherche-filter-soiree.pipe';
import { RechercheFilterCircuitPipe } from './Filters/CircuitFilters/recherche-filter-circuit.pipe';
import { DateFilterVoyagePipe } from './Filters/VoyageFilters/date-filter-voyage.pipe';
import { PrixFilterVoyagePipe } from './Filters/VoyageFilters/prix-filter-voyage.pipe';
import { PrixFilterSoireePipe } from './Filters/SoireeFilters/prix-filter-soiree.pipe';
import { DateFilterSoireePipe } from './Filters/SoireeFilters/date-filter-soiree.pipe';
import { DateFilterOmraPipe } from './Filters/OmraFilters/date-filter-omra.pipe';
import { PrixFilterOmraPipe } from './Filters/OmraFilters/prix-filter-omra.pipe';
import { AddHotelComponent } from './Components/DashboardAdmin/Hotel/add-hotel/add-hotel.component';
import { AddCircuitComponent } from './Components/DashboardAdmin/Circuit/add-circuit/add-circuit.component';
import { CircuitmodifComponent } from './Components/DashboardAdmin/Circuit/circuitmodif/circuitmodif.component';
import { AddSoireeComponent } from './Components/DashboardAdmin/Soiree/add-soiree/add-soiree.component';
import { SoireemodifComponent } from './Components/DashboardAdmin/Soiree/soireemodif/soireemodif.component';
import { VoyagemodifComponent } from './Components/DashboardAdmin/Voyage/voyagemodif/voyagemodif.component';
import { AddVoyageComponent } from './Components/DashboardAdmin/Voyage/add-voyage/add-voyage.component';
import { HotelmodifComponent } from './Components/DashboardAdmin/Hotel/hotelmodif/hotelmodif.component';
import { ConfirmReservationComponent } from './Components/confirm-reservation/confirm-reservation.component';
import { AppVarsGlobal } from './app.vars.global';
import { CircuitcontroleComponent } from './Components/DashboardAdmin/circuitcontrole/circuitcontrole.component';
import { OmraComponent } from './Components/omra/omra.component';
import { AddOmraComponent } from './Components/DashboardAdmin/Omra/add-omra/add-omra.component';
import { HistoriqueComponent } from './Components/DashboardClient/historique/historique.component';
import { ReservationAccepterComponent } from './Components/DashboardClient/reservation-accepter/reservation-accepter.component';
import { ReservationEnCourComponent } from './Components/DashboardClient/reservation-en-cour/reservation-en-cour.component';
import { ReservationEnCourDetailComponent } from './Components/DashboardClient/reservation-en-cour-detail/reservation-en-cour-detail.component';
import { ContactResponseComponent } from './Components/DashboardAdmin/contact-response/contact-response.component';
import { MessageComponent } from './Components/message/message.component';
import { PaysFiltreVoyagePipe } from './Filters/VoyageFilters/pays-filtre-voyage.pipe';
import { DateFilterCircuitPipe } from './Filters/CircuitFilters/date-filter-circuit.pipe';




@NgModule({
  imports: [
    MatSnackBarModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,ReactiveFormsModule,
    MDBBootstrapModule.forRoot(),
    CalendarModule, DatePickerModule, TimePickerModule, DateRangePickerModule, 
    DateTimePickerModule,BrowserAnimationsModule,
    MatButtonModule, MatTabsModule,
    MatCheckboxModule,
    MatDialogModule,MatCardModule,MatInputModule, MatTableModule,MatGridListModule,
    MatToolbarModule,MatSidenavModule, MatMenuModule,MatIconModule,MatDividerModule,MatListModule, 
    MatProgressSpinnerModule,NgbModule, MatPaginatorModule, MatSortModule, LayoutModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      progressBar:true,
      progressAnimation:'increasing'
    })
    
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    ContactComponent,
    ModalComponent,
    VoyageComponent,
    HotelsComponent,
    CircuitComponent,
    HreservationComponent,		
    VreservationComponent, FiltersPipe, FiltersearshPipe, FilteretoilePipe, FilterprixPipe, SoireeComponent, SafeHtmlPipe,
    DureeVoyagePipe, CircuitReservationComponent, SoireeReservationComponent, SoireeDatePipePipe, RechercheFilterOmraPipe, 
    RechercheFilterVoyagePipe, RechercheFilterSoireePipe, RechercheFilterCircuitPipe, DateFilterVoyagePipe, PrixFilterVoyagePipe,
    PrixFilterSoireePipe, DateFilterSoireePipe, DateFilterOmraPipe, PrixFilterOmraPipe, AddHotelComponent, VoyagemodifComponent, 
    AddVoyageComponent, AddSoireeComponent, SoireemodifComponent, CircuitmodifComponent, AddCircuitComponent,HotelmodifComponent,
    ConfirmReservationComponent, OmraComponent, AddOmraComponent,ReservationEnCourDetailComponent, ContactResponseComponent, MessageComponent,OmramodifComponent,
    DateFilterVoyagePipe,PaysFiltreVoyagePipe,PrixFilterVoyagePipe, DateFilterCircuitPipe
  ],
  providers: [AppVarsGlobal,AddOmraComponent,ReservationEnCourDetailComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
