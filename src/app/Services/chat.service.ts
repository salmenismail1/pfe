import { Chat } from './../Models/chat';
import { User } from './../Models/user';
import { Injectable } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private posts: any[] = [];
  private postsUpdated = new Subject<any[]>();
 
  private postsSub: Subscription;
  public sender: any;



  constructor(private toastr: ToastrService,private httpClient:HttpClient) {}


  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }

  getAdminsBase() {
    this.httpClient
      .get<{ message: string; posts: any }>(
        "http://localhost:3000/chat/api/admins"
        
      ).subscribe(transformedPosts => {
        console.log(transformedPosts.posts);
        this.posts = transformedPosts.posts;
        this.postsUpdated.next([...this.posts]);
      });
  }
  getOneAdminsBase(id) {
     this.httpClient.get<any>("http://localhost:3000/chat/api/chatSender/"+id)
      .pipe(map((postData) => {
        return postData.map(post => {
                    return {
                      _id : post._id,nom : post.nom
              
          };
        });
      }))
      .subscribe(transformedPosts => {
        this.sender = transformedPosts[0];
        console.log(transformedPosts[0]);
       
      });
  }


  addPost(item:any) {
    console.log(item);

    const post: Chat = {  reciverId :item.reciverId,sender :item.sender,reciver:item.reciver,message:item.message,
  };
    this.httpClient
      .post<{ message: string, postId: string }>("http://localhost:3000/chat/api/chat", post)
      .subscribe(responseData => {
        const id = responseData.postId;
      
      });
      this.toastr.success('', "message sended ");

    }
}
