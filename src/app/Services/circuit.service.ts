import { Injectable } from '@angular/core';
import { HttpHeaders ,HttpClient} from '@angular/common/http';
import { Voyages } from '../Models/voyages';
import { Observable, Subscription } from 'rxjs';
import { formatDate } from '@angular/common';
import { Circuit } from '../Models/circuit';
import {  Subject } from 'rxjs';
import { User } from '../Models/user';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Reservation } from '../Models/reservation';

const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};
const url = "https://topresa.ovh/api/departcircuits.json?date[before]=2020-06-18";
const deleteReservationHotelUrl ="http://localhost:3000/circuit/api/DeleteReservation/" ;
const UpgradePayementUrl = "http://localhost:3000/circuit/api/UpgradePayement/";

const ReservationAccepter ="http://localhost:3000/circuit/api/ReservationAccepter/" ;
const ReservationAccepterUrl = "http://localhost:3000/circuit/api/ReservationHotelAccepter";
const ReservationEnAttenteUrl = "http://localhost:3000/circuit/api/ReservationHotelEnAttente";

@Injectable({
  providedIn: 'root'
})
export class CircuitService {
  
  private posts: Circuit[] = [];
  private postsUpdated = new Subject<Circuit[]>();
 
  private posts2: any[] = [];
  private postsUpdated2 = new Subject<any[]>();
  private circuithomelist: Circuit[] = [];
  private circuithomelistUpdated = new Subject<Circuit[]>();
  private postsSub: Subscription;
  private Hexiste:string;


  constructor(private toastr: ToastrService,private httpClient:HttpClient) {}


  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }
  gethomecircuitUpdateListener() {
    return this.circuithomelistUpdated.asObservable();
  }
  getCircuit():Observable<Circuit>{
    
    return this.httpClient.get<Circuit>(url,httpOptions);
  }

  DeleteReservationHotel(id : string){
    this.httpClient.delete<{message: string }>(deleteReservationHotelUrl+id,httpOptions).subscribe((responseData) => {
    
     this.toastr.success('', responseData.message);

   });
 }
 UpgradePayement(id:any){
  this.httpClient.put<{message: string }>(UpgradePayementUrl+id,httpOptions).subscribe(responseData => {
    
    this.toastr.success('', responseData.message);

    });
  }
 ReservationAccepter(id : string){
    this.httpClient.put<{ data: any, message: string }>( ReservationAccepter+id,httpOptions).subscribe(responseData => {
     
   this.toastr.success('', responseData.message);

   });
 }

 getReservationAccepter(){
   return this.httpClient.get<Reservation>( ReservationAccepterUrl,httpOptions);
 }

 getReservationEnAttent(){
   return this.httpClient.get<Reservation>(ReservationEnAttenteUrl,httpOptions);
   
 }



 
  //**************njeimi */

  getCircuitBase() {
    this.httpClient
      .get<{ message: string; posts: any }>(
        "http://localhost:3000/circuit/api/circuit"
        
      )
      .pipe(map((postData) => {
        return postData.posts.map(post => {
                    return {
                      _id : post._id,id :  post.id,nom : post.nom,image : post.image,datedepart:post.datedepart,
                      dateretour:post.dateretour,description:post.programmme.description,libelle:post.programmme.libelle,programme:post.programmme.programme,venteadultesingle:post.venteadultesingle,venteadultedble:post.venteadultedble
                      ,vente3rdad:post.vente3rdad,venteenfant2ad:post.venteenfant2ad,tarifbebe:post.tarifbebe,venteenfant1ad:post.venteenfant1ad
            
          };
        });
      }))
      .subscribe(transformedPosts => {
        this.posts = transformedPosts;
        this.postsUpdated.next([...this.posts]);
      });
  }
  
  getImagesBase() {
    this.httpClient
      .get<{ message: string; posts: any }>(
        "http://localhost:3000/circuit/api/images"
        
      )
      .pipe(map((postData) => {
        return postData.posts.map(post => {
                    return {
                      _id : post._id,data:post.img.data,contentType:post.img.contentType
            
          };
        });
      }))
      .subscribe(transformedPosts => {
        this.posts2 = transformedPosts;
        this.postsUpdated2.next([...this.posts2]);
      });
  }
  
  addPost(item:any,images:any) {
    let existe="";
    this.httpClient
    .get<{  posts: any }>("http://localhost:3000/circuit/api/existe/"+item.id)
    .subscribe(responseData => {
      console.log("###zebi "+responseData);
  existe=""+responseData;
  this.Hexiste=existe;
      if(existe=="false"){

    const post: Circuit = {  id :  item.id,nom:item.nom,image : images,datedepart:item.datedepart,
      dateretour:item.dateretour,programmme:{description:item.description,libelle:item.libelle,programme:item.programme},venteadultesingle:item.venteadultesingle,venteadultedble:item.venteadultedble
      ,vente3rdad:item.vente3rdad,venteenfant2ad:item.venteenfant2ad,tarifbebe:item.tarifbebe,venteenfant1ad:item.venteenfant1ad
  };
  console.log(post.image);
    this.httpClient
      .post<{ message: string, postId: string }>("http://localhost:3000/circuit/api/circuit", post)
      .subscribe(responseData => {
        const id = responseData.postId;
        post.id = id;
        this.getCircuitBase();
        this.postsSub = this.getPostUpdateListener()
      .subscribe((posts: Circuit[]) => {
        this.posts = posts;
       
      
      });
              this.postsUpdated.next([...this.posts]);
      });
      this.toastr.success('', "Circuit added in the base");

    }else{

      console.log("##**#"+responseData.posts);

      this.toastr.error('', "Circuit existe in the base");

    }
  });
  }

  update(  item:any,images:any,idg:any) {
    const post: Circuit = {  id :  item.id,nom:item.nom,image : images,datedepart:item.datedepart,
      dateretour:item.dateretour,programmme:{description:item.description,libelle:item.libelle,programme:item.programme},venteadultesingle:item.venteadultesingle,venteadultedble:item.venteadultedble
      ,vente3rdad:item.vente3rdad,venteenfant2ad:item.venteenfant2ad,tarifbebe:item.tarifbebe,venteenfant1ad:item.venteenfant1ad
  };
    this.httpClient
      .put("http://localhost:3000/circuit/api/circuitModif/"+idg, post)
      .subscribe(responseData => {
        
        this.getCircuitBase();
        this.postsSub = this.getPostUpdateListener()
      .subscribe((posts: Circuit[]) => {
        this.posts = posts;
       
      
      });
                this.postsUpdated.next([...this.posts]);
      });
  }  

  deletePost(postId: string) {
    this.httpClient.delete("http://localhost:3000/circuit/api/posts/" + postId)
      .subscribe(() => {
        const updatedPosts = this.posts.filter(post => post.id !== postId);
        this.posts = updatedPosts;
        this.postsUpdated.next([...this.posts]);
      });
  }
  
  deleteall(){

    this.httpClient.delete("http://localhost:3000/circuit/api/deleteall/" + null)
    .subscribe(() => {
    console.log("dd");
    });
    
    }
    
    getCircuithomelist() {
    this.httpClient
      .get<{ message: string; posts: any }>(
        "http://localhost:3000/circuit/api/circuithomelist"
        
      )
      .pipe(map((postData) => {
        return postData.posts.map(post => {
                    return {
                      _id : post._id,
            id :  post.id,nom : post.nom,adultOnly : post.adultOnly,ville : post.ville,categorie : post.categorie,type : post.type,
      lpdvente : post.lpdvente,dpvente : post.dpvente,pcvente : post.pcvente,allinsoftvente : post.allinsoftvente,allinvente : post.allinvente,
      ultraallinvente : post.ultraallinvente,age_enf_gratuit : post.age_enf_gratuit,image:post.image
            
          };
        });
      }))
      .subscribe(transformedPosts => {
        this.posts = transformedPosts;
        this.postsUpdated.next([...this.posts]);
        this.circuithomelist = transformedPosts;
        this.circuithomelistUpdated.next([...this.posts]);
      });
    }
    
    circuithomelistpost(item:any) {
  
      
      const post: Circuit = {  id :  item.id,nom : item.nom,datedepart : item.datedepart,dateretour : item.dateretour,
        programmme : item.programmme,venteadultesingle : item.venteadultesingle,venteadultedble : item.venteadultedble,vente3rdad : item.vente3rdad,venteenfant2ad : item.venteenfant2ad,
        tarifbebe : item.tarifbebe,venteenfant1ad : item.venteenfant1ad,image:item.image
    };
      this.httpClient
        .post<{ message: string, postId: string }>("http://localhost:3000/circuit/api/circuithomelist",post) .subscribe(responseData => {
          const id = responseData.postId;
          post.id = id;
          this.circuithomelist.push(post);
          this.circuithomelistUpdated.next([...this.posts]);
        });
    }

  //**************njeimi */  
 
}

