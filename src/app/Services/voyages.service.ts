import { Injectable } from '@angular/core';
import { HttpHeaders ,HttpClient} from '@angular/common/http';
import { Voyages } from '../Models/voyages';
import { Observable, Subject, Subscription } from 'rxjs';
import { formatDate } from '@angular/common';
import { map } from 'rxjs/operators';
import { Reservation } from '../Models/reservation';
import { ToastrService } from 'ngx-toastr';
import { User } from '../Models/user';

const url = "https://topresa.ovh/api/voyages.json?departvoyages.datedepart[after]=2020-02-14 ";

const deleteReservationVoyageUrl ="http://localhost:3000/voyage/api/DeleteReservation/" ;
const ReservationAccepter ="http://localhost:3000/voyage/api/ReservationAccepter/" ;
const ReservationAccepterUrl = "http://localhost:3000/voyage/api/ReservationVoyageAccepter"
const ReservationEnAttenteUrl = "http://localhost:3000/voyage/api/ReservationVoyageEnAttente";
const UneReservationUrl ="http://localhost:3000/voyage/api/UneReservation/"
const UpgradePayementUrl = "http://localhost:3000/voyage/api/UpgradePayement/";

const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};



@Injectable({
  providedIn: 'root'
})
export class VoyagesService {

//*******njeimi */

  private posts: Voyages[] = [];
  private postsUpdated = new Subject<Voyages[]>();
  private voyagehomelist: Voyages[] = [];
  private voyagehomelistUpdated = new Subject<Voyages[]>();
  private postsSub: Subscription;
  private Hexiste:string;

  //*******njeimi */


  constructor(private toastr: ToastrService,private httpClient:HttpClient) {}

  getVoyage():Observable<Voyages>{
    
    return this.httpClient.get<Voyages>(url,httpOptions);
  }
  




  DeleteReservationVoyage(id : string){

    this.httpClient.delete<{message: string }>(deleteReservationVoyageUrl+id,httpOptions).subscribe((responseData) => {
     
      this.toastr.success('', responseData.message);

    });
  }
  UpgradePayement(id:any){
    this.httpClient.put<{message: string }>(UpgradePayementUrl+id,httpOptions).subscribe(responseData => {
      
      this.toastr.success('', responseData.message);
  
      });
  
  }
  
  ReservationAccepter(id : string){
    this.httpClient.put<{ data: any, message: string }>( ReservationAccepter+id,httpOptions).subscribe(responseData => {
      
      this.toastr.success('', responseData.message);
  
      });
  }

  getReservationAccepter(){
    return this.httpClient.get<Reservation>( ReservationAccepterUrl,httpOptions);
  }

  getReservationEnAttent(){
    return this.httpClient.get<Reservation>(ReservationEnAttenteUrl,httpOptions);
    
  }
  getUneReservation(id : string){
    
    return this.httpClient.get<Reservation>(UneReservationUrl+id,httpOptions);
  }

  //*******njeimi */

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }


  gethomevoyageUpdateListener() {
    return this.voyagehomelistUpdated.asObservable();
  }

  getVoyageBase() {
    this.httpClient
      .get<{ message: string; posts: any }>(
        "http://localhost:3000/voyage/api/voyage"
        
      )
      .pipe(map((postData) => {
        return postData.posts.map(post => {
                    return {
                      _id : post._id,
            id :  post.id,nom : post.nom,pays : post.pays,titre : post.titre,venteadultesingle : post.venteadultesingle,venteadultedble : post.venteadultedble,
            vente3rdad : post.vente3rdad,venteenfant2ad : post.venteenfant2ad,venteenfant1ad : post.venteenfant1ad,tarifbebe : post.tarifbebe,
            venteenfant12ans : post.venteenfant12ans, venteenfant6ans : post.venteenfant6ans,departvoyages : post.departvoyages,
            image:post.image,programme:post.programme
            
          };
        });
      }))
      .subscribe(transformedPosts => {
        this.posts = transformedPosts;
        this.postsUpdated.next([...this.posts]);
      });
  }
  addPost(id : string,nom : string,pays : string,titre : string,venteadultesingle : number,venteadultedble : number,
    vente3rdad : number,venteenfant2ad : number,venteenfant1ad : number,tarifbebe : number,venteenfant12ans : number,
    venteenfant6ans:number, datedepart:string,dateretour:string,images: any,programme:any ) {
      let existe="";
    this.httpClient
    .get<{  posts: any }>("http://localhost:3000/voyage/api/existe/"+id)
    .subscribe(responseData => {
      console.log("###zebi "+responseData);
  existe=""+responseData;
  this.Hexiste=existe;
      if(existe=="false"){
        
    const post: Voyages = {  id :  id,nom : nom,pays:pays,titre:titre,venteadultesingle:venteadultesingle,
      venteadultedble : venteadultedble,vente3rdad:vente3rdad,venteenfant2ad:venteenfant2ad,venteenfant1ad:venteenfant1ad,
      tarifbebe : tarifbebe,venteenfant12ans:venteenfant12ans,venteenfant6ans:venteenfant6ans,
      departvoyages:{datedepart,dateretour},image:images,programme:{programme:programme}
  };
    this.httpClient
      .post<{ message: string, postId: string }>("http://localhost:3000/voyage/api/voyage", post)
      .subscribe(responseData => {
        const id = responseData.postId;
        post.id = id;
        this.getVoyageBase();
        this.postsSub = this.getPostUpdateListener()
      .subscribe((posts: Voyages[]) => {
        this.posts = posts;
       
      
      });
              this.postsUpdated.next([...this.posts]);
      });
  
      this.toastr.success('', "Hotel added in the base");

    }else{

      console.log("##**#"+responseData.posts);

      this.toastr.error('', "Hotel existe in the base");

    }
    

  });
}

  update(  id : string,nom : string,pays : string,titre : string,venteadultesingle : number,venteadultedble : number,
    vente3rdad : number,venteenfant2ad : number,venteenfant1ad : number,tarifbebe : number,venteenfant12ans : number,
    venteenfant6ans:number, datedepart:string,dateretour:string,images: any,idg:any,programme:any) {
      const post: Voyages = {  id :  id,nom : nom,pays:pays,titre:titre,venteadultesingle:venteadultesingle,
        venteadultedble : venteadultedble,vente3rdad:vente3rdad,venteenfant2ad:venteenfant2ad,venteenfant1ad:venteenfant1ad,
        tarifbebe : tarifbebe,venteenfant12ans:venteenfant12ans,venteenfant6ans:venteenfant6ans,
        departvoyages:{datedepart,dateretour},image:images,programme:{programme:programme}
    };
    this.httpClient
      .put("http://localhost:3000/voyage/api/voyageModif/"+idg, post)
      .subscribe(responseData => {
        
this.getVoyageBase();
        this.postsSub = this.getPostUpdateListener()
      .subscribe((posts: Voyages[]) => {
        this.posts = posts;
       
      
      });
        this.postsUpdated.next([...this.posts]);
      });
  }  

  deletePost(postId: string) {
    this.httpClient.delete("http://localhost:3000/voyage/api/posts/" + postId)
      .subscribe(() => {
        const updatedPosts = this.posts.filter(post => post.id !== postId);
        this.posts = updatedPosts;
        this.postsUpdated.next([...this.posts]);
      });
  }
  deleteall(){
  
    this.httpClient.delete("http://localhost:3000/voyage/api/deleteall/" + null)
    .subscribe(() => {
  console.log("dd");
    });
  
  }


  getVoyagehomelist() {
    this.httpClient
      .get<{ message: string; posts: any }>(
        "http://localhost:3000/voyage/api/voyagehomelist"
        
      )
      .pipe(map((postData) => {
        return postData.posts.map(post => {
                    return {
                      _id : post._id,
                      id :  post.id,nom : post.nom,pays : post.pays,titre : post.titre,venteadultesingle : post.venteadultesingle,venteadultedble : post.venteadultedble,
                      vente3rdad : post.vente3rdad,venteenfant2ad : post.venteenfant2ad,venteenfant1ad : post.venteenfant1ad,tarifbebe : post.tarifbebe,
                      venteenfant12ans : post.venteenfant12ans, venteenfant6ans : post.venteenfant6ans,
                      departvoyages: {datedepart : post.datedepart,dateretour : post.dateretour},image:post.image
            
          };
        });
      }))
      .subscribe(transformedPosts => {
        this.posts = transformedPosts;
        this.postsUpdated.next([...this.posts]);
        this.voyagehomelist = transformedPosts;
        this.voyagehomelistUpdated.next([...this.posts]);
      });
  }
  voyagehomelistpost(item:any) {

let datedepart=item.datedepart;  
let dateretour=item.dateretour;    


const post: Voyages = {  id :  item.id,nom : item.nom,pays:item.pays,titre:item.titre,venteadultesingle:item.venteadultesingle,
  venteadultedble : item.venteadultedble,vente3rdad:item.vente3rdad,venteenfant2ad:item.venteenfant2ad,venteenfant1ad:item.venteenfant1ad,
  tarifbebe : item.tarifbebe,venteenfant12ans:item.venteenfant12ans,venteenfant6ans:item.venteenfant6ans,
  departvoyages:{datedepart,dateretour},image:item.image,programme:item.programme
};
    this.httpClient
      .post<{ message: string, postId: string }>("http://localhost:3000/voyage/api/voyagehomelist",post) .subscribe(responseData => {
        const id = responseData.postId;
        post.id = id;
        this.voyagehomelist.push(post);
        this.voyagehomelistUpdated.next([...this.posts]);
      });
      console.log(post);
  }

  //*******njeimi */
}
