import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Login } from '../Models/login';
import { Router } from '@angular/router';
import { User } from '../Models/user';
import { Reservation } from '../Models/reservation';

const reservationUrl ="http://localhost:3000/user/api/reservation/";
const UpgradePayementUrl = "http://localhost:3000/user/api/UpgradePayement/";
const logintUrl = "http://localhost:3000/user/connection";
const inscritUrl = "http://localhost:3000/user/in";
const UpgradeToAdminUrl = "http://localhost:3000/user/api/UpgradeToAdmin/";
const ClientUrl ="http://localhost:3000/user/api/client";
const AdminUrl = "http://localhost:3000/user/api/admin";
const DowngradeToClientUrl = "http://localhost:3000/user/api/DowngradeToClient/" ;
const deleteUserUrl = "http://localhost:3000/user/api/DeleteUser/";
const OneUserUrl = "http://localhost:3000/user/api/statut/";
const ModifUserUrl="http://localhost:3000/user/api/ModifUser/" ;
const ReservationVoyageClientEncourUrl ="http://localhost:3000/user/api/ReservationVoyageClientEncour/";
const ReservationHotelClientEncourUrl = "http://localhost:3000/user/api/ReservationHotelClientEncour/"
const HistoriqueUrl ="http://localhost:3000/user/api/Historique"
const removeReservationUrl ="http://localhost:3000/user/api/DeleteReservation/"
const notificationUrl ="http://localhost:3000/user/api/notifuser/"




@Injectable({
  providedIn: 'root'
})

export class LoginService {

  static type: string;
  constructor(private httpClient : HttpClient, private router : Router) {}

  getReservation(id : string){
    return this.httpClient.get<Reservation>(reservationUrl+id);
  }



  postLogin(login:Login){
    return this.httpClient.post<any>(logintUrl,login);
  }
  Inscrit(user:User):Observable<User>{
    return this.httpClient.post<User>(inscritUrl,user);
  }
  logOut(){
     localStorage.removeItem('token');
  }
  getToken(){
    return localStorage.getItem('token');
  }
  loggedIn(){
    return !!!this.getToken();
  }
  UpgradeToAdmin(user:User){
     this.httpClient.put<User>(UpgradeToAdminUrl+user,user);
  }
  DowngradeToClient(user:User){
     this.httpClient.put<User>(DowngradeToClientUrl+user,user);
  }
  DeleteUser(id : string){
     this.httpClient.delete<User>(deleteUserUrl+id);
  }
  ModifUser(id : string,user :User){
    return this.httpClient.put<User>(ModifUserUrl+id,user);
  }
  getAdmin(){
    return this.httpClient.get<any>(AdminUrl);
  }
  getClient(){
    return this.httpClient.get<any>(ClientUrl);
  }
  getOnUser(id:string){
    return this.httpClient.get<any>(OneUserUrl+id);
  }
  getReservationVoyageClientEncour(id : string){
    return this.httpClient.get<Reservation>(ReservationVoyageClientEncourUrl+id);
  }
  getReservationHotelClientEncour(id : string){
    return this.httpClient.get<Reservation>(ReservationHotelClientEncourUrl+id);
  }
  getHistorique(id : string,url:any){
    return this.httpClient.get<Reservation>(url+id);
  }
  getHistoriquee(){
    return this.httpClient.get<Reservation>(HistoriqueUrl);
  }
  RemoveReservation(id:string){
    return this.httpClient.delete<Reservation>(removeReservationUrl+id)
  }

  getNotif(id:string){
    return this.httpClient.delete<Reservation>(notificationUrl+id)

  }
  
}
