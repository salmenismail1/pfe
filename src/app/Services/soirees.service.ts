import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, Subject, Subscription } from 'rxjs';
import { Soiree } from '../Models/soiree';
import { formatDate } from '@angular/common';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Reservation } from '../Models/reservation';
import { User } from '../Models/user';

const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};
const deleteReservationVoyageUrl ="http://localhost:3000/soiree/api/DeleteReservation/" ;
const ReservationAccepter ="http://localhost:3000/soiree/api/ReservationAccepter/" ;
const ReservationAccepterUrl = "http://localhost:3000/soiree/api/ReservationSoireeAccepter"
const ReservationEnAttenteUrl = "http://localhost:3000/soiree/api/ReservationSoireeEnAttente";
//const url = "https://topresa.ovh/api/soirees.json?departsoirees.date[after]="+formatDate(new Date(), 'yyyy-MM-dd', 'en');
const url = "https://topresa.ovh/api/soirees.json?departsoirees.date[after]=17/06/2020";
const UpgradePayementUrl = "http://localhost:3000/soiree/api/UpgradePayement/";

@Injectable({
  providedIn: 'root'
})
export class SoireesService {

   //*********Njeimi */

  private posts: Soiree[] = [];
  private postsUpdated = new Subject<Soiree[]>();
  private soireehomelist: Soiree[] = [];
  private soireehomelistUpdated = new Subject<Soiree[]>();
  private postsSub: Subscription;
  private Hexiste:string;

  //*********Njeimi */

  
  constructor(private toastr: ToastrService,private httpClient:HttpClient) {}

  getSoiree():Observable<Soiree>{
    
    return this.httpClient.get<Soiree>(url,httpOptions);
  }

  DeleteReservationSoiree(id : string){

    this.httpClient.delete<{message: string }>(deleteReservationVoyageUrl+id,httpOptions).subscribe((responseData) => {
     
      this.toastr.success('', responseData.message);

    });
  }
  UpgradePayement(id:any){
    this.httpClient.put<{message: string }>(UpgradePayementUrl+id,httpOptions).subscribe(responseData => {
      
      this.toastr.success('', responseData.message);
  
      });
  
  }
  ReservationAccepter(id : string){
    this.httpClient.put<{ data: any, message: string }>( ReservationAccepter+id,httpOptions).subscribe(responseData => {
      
      this.toastr.success('', responseData.message);
  
      });
  }
  getReservationAccepter(){
    return this.httpClient.get<Reservation>( ReservationAccepterUrl,httpOptions);
  }

  getReservationEnAttent(){
    return this.httpClient.get<Reservation>(ReservationEnAttenteUrl,httpOptions);
    
  }

  //*********Njeimi */

  getSoireesBase() {
    this.httpClient
      .get<{ message: string; posts: any }>(
        "http://localhost:3000/soiree/api/soiree"
        
      )
      .pipe(map((postData) => {
        return postData.posts.map(post => {
                    return {
                      _id : post._id,id :  post.id,titre : post.titre,image : post.image, date : post.date,
                      venteadultesingle : post.venteadultesingle,venteadultedble : post.venteadultedble, 
                      vente3rdad : post.vente3rdad,venteenfant2ad : post.venteenfant2ad,tarifbebe : post.tarifbebe,
                      venteenfant1ad : post.venteenfant1ad,programme:post.programme
            
          };
        });
      }))
      .subscribe(transformedPosts => {
        this.posts = transformedPosts;
        this.postsUpdated.next([...this.posts]);
      });
  }
  addPost(    id : string,titre : string,venteadultesingle : number,venteadultedble:number,date:string,images: any,vente3rdad:number,venteenfant2ad:number,tarifbebe:number,venteenfant1ad:number,programme:any ) {
    
    let existe="";
    this.httpClient
    .get<{  posts: any }>("http://localhost:3000/soiree/api/existe/"+id)
    .subscribe(responseData => {
      console.log("###zebi "+responseData);
  existe=""+responseData;
  this.Hexiste=existe;
      if(existe=="false"){
    const post: Soiree = {  id :  id,titre:titre,image : images, date : date,venteadultesingle : venteadultesingle,
      venteadultedble :venteadultedble, vente3rdad : vente3rdad,venteenfant2ad : venteenfant2ad,tarifbebe : tarifbebe,
      venteenfant1ad : venteenfant1ad,programme:{programme:programme}
  };
    this.httpClient
      .post<{ message: string, postId: string }>("http://localhost:3000/soiree/api/soiree", post)
      .subscribe(responseData => {
        const id = responseData.postId;
        post.id = id;
        this.getSoireesBase();
        this.postsSub = this.getPostUpdateListener()
      .subscribe((posts: Soiree[]) => {
        this.posts = posts;
       
      
      });
              this.postsUpdated.next([...this.posts]);
      });
      this.toastr.success('', "Hotel added in the base");

    }else{

      console.log("##**#"+responseData.posts);

      this.toastr.error('', "Hotel existe in the base");

    }
    

  });
}
  
  update(  id : string,titre : string,venteadultesingle : number,venteadultedble:number,date:string,images: any,vente3rdad:number,venteenfant2ad:number,tarifbebe:number,venteenfant1ad:number,idg:any,programme:any) {
    const post: Soiree = {  id :  id,titre:titre,image : images, date : date,venteadultesingle : venteadultesingle,
      venteadultedble :venteadultedble, vente3rdad : vente3rdad,venteenfant2ad : venteenfant2ad,tarifbebe : tarifbebe,
      venteenfant1ad : venteenfant1ad,programme:{programme:programme}
  };
    this.httpClient
      .put("http://localhost:3000/soiree/api/soireeModif/"+idg, post)
      .subscribe(responseData => {
        
        this.getSoireesBase();
        this.postsSub = this.getPostUpdateListener()
      .subscribe((posts: Soiree[]) => {
        this.posts = posts;
       
      
      });
        this.postsUpdated.next([...this.posts]);
      });
  }  

  deletePost(postId: string) {
    this.httpClient.delete("http://localhost:3000/soiree/api/posts/" + postId)
      .subscribe(() => {
        const updatedPosts = this.posts.filter(post => post.id !== postId);
        this.posts = updatedPosts;
        this.postsUpdated.next([...this.posts]);
      });
  }

  deleteall(){
  
    this.httpClient.delete("http://localhost:3000/soiree/api/deleteall/" + null)
    .subscribe(() => {
  console.log("dd");
    });
  
  }
  existe(id:any){

    this.httpClient
      .get<{ message: string; posts: any }>("http://localhost:3000/soiree/api/existe/"+id)
      .subscribe(responseData => {
        return responseData;
      });
    
    }

  getSoireehomelist() {
    this.httpClient
      .get<{ message: string; posts: any }>(
        "http://localhost:3000/soiree/api/soireehomelist"
        
      )
      .pipe(map((postData) => {
        return postData.posts.map(post => {
                    return {
                      _id : post._id,
                      id :  post.id,titre:post.titre,image : post.image, date : post.date,venteadultesingle : post.venteadultesingle,
                      venteadultedble :post.venteadultedble, vente3rdad : post.vente3rdad,venteenfant2ad : post.venteenfant2ad,tarifbebe : post.tarifbebe,
                      venteenfant1ad : post.venteenfant1ad
            
          };
        });
      }))
      .subscribe(transformedPosts => {
        this.posts = transformedPosts;
        this.postsUpdated.next([...this.posts]);
        this.soireehomelist = transformedPosts;
        this.soireehomelistUpdated.next([...this.posts]);
      });
  }
  soireehomelistpost(item:any) {


    const post: Soiree = {   id :  item.id,titre:item.titre,image : item.image, date : item.date,venteadultesingle : item.venteadultesingle,
      venteadultedble :item.venteadultedble, vente3rdad : item.vente3rdad,venteenfant2ad : item.venteenfant2ad,tarifbebe : item.tarifbebe,
      venteenfant1ad : item.venteenfant1ad,programme:item.programme
};
    this.httpClient
      .post<{ message: string, postId: string }>("http://localhost:3000/soiree/api/soireehomelist",post) .subscribe(responseData => {
        const id = responseData.postId;
        post.id = id;
        this.soireehomelist.push(post);
        this.soireehomelistUpdated.next([...this.posts]);
      });
      console.log(post);
  }

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }

  gethomesoireeUpdateListener() {
    return this.soireehomelistUpdated.asObservable();
  }
  //*********Njeimi */

}
