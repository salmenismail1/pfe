import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Contact } from '../Models/contact';
import { Observable, Subject, Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';
/*
const contactUrl = "https://topresa.ovh/api/contactes";
const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};
*/
@Injectable({
  providedIn: 'root'
})

  
export class ContactService {
  
  private posts: Contact[] = [];
  private postsUpdated = new Subject<Contact[]>();
  private postsSub: Subscription;

  constructor(private toastr: ToastrService,private httpClient:HttpClient) {}

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }
  
  addPost(item:any) {
 
    const post: Contact = {  idClient :item.idClient,nom:item.nom,email : item.email,message:item.message,
      sujet:item.sujet,tel:item.tel,response:item.response
  };
    this.httpClient
      .post<{ message: string, postId: string }>("http://localhost:3000/contact/api/contact", post)
      .subscribe(responseData => {
        const id = responseData.postId;
      
      });
      this.toastr.success('', "contact added in the base");

    }
    getContactBase(response) {
      this.httpClient
        .get<{ message: string; posts: any }>(
          "http://localhost:3000/contact/api/contact/"+response
          
        )
        .pipe(map((postData) => {
          return postData.posts.map(post => {
                      return {
                        _id : post._id,idClient :post.idClient,nom:post.nom,email : post.email,message:post.message,
                        sujet:post.sujet,tel:post.tel,response:post.response
              
            };
          });
        }))
        .subscribe(transformedPosts => {
          console.log(transformedPosts);
          this.posts = transformedPosts;
          this.postsUpdated.next([...this.posts]);
        });
    }

    update(  item:any,response) {
      const post: Contact = {  idClient :item.idClient,nom:item.nom,email : item.email,message:item.message,
        sujet:item.sujet,tel:item.tel,response:item.response
    };
      this.httpClient
        .put("http://localhost:3000/contact/api/contactModif/"+item._id, post)
        .subscribe(responseData => {
          
          this.getContactBase(response);
          this.postsSub = this.getPostUpdateListener()
        .subscribe((posts: Contact[]) => {
          this.posts = posts;
         
        
        });
                  this.postsUpdated.next([...this.posts]);
        });
    }  
  
    deletePost(postId: string) {
      this.httpClient.delete("http://localhost:3000/contact/api/posts/" + postId)
        .subscribe(() => {
          const updatedPosts = this.posts.filter(post => post.idClient !== postId);
          this.posts = updatedPosts;
          this.postsUpdated.next([...this.posts]);
        });
    }
  
}
