import { Omra } from './../Models/omra';

import { formatDate } from '@angular/common';

import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Hotels } from '../Models/hotels';
import { Observable, Subject, Subscription } from 'rxjs';
import { User } from '../Models/user';
import { ReservationHotel } from '../Models/reservation-hotel';
import { map } from 'rxjs/operators';
import { Reservation } from '../Models/reservation';
import { ToastrService } from 'ngx-toastr';

const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};
const AjoutReservationUrl ="http://localhost:3000/Omra/api/reservation"


const ReservationHotelUrl ="http://localhost:3000/omra/api/ReservationOmra";
const deleteReservationHotelUrl ="http://localhost:3000/omra/api/DeleteReservation/" ;

const ReservationAccepter ="http://localhost:3000/omra/api/ReservationAccepter/" ;
const ReservationAccepterUrl = "http://localhost:3000/omra/api/ReservationOmraAccepter"
const ReservationEnAttenteUrl = "http://localhost:3000/omra/api/ReservationOmraEnAttente"

const DeleteHotelUrl = "http://localhost:3000/omra/api/omra/";//const url = "https://topresa.ovh/api/omras.json?date[after]="+formatDate(new Date(), 'yyyy-MM-dd', 'en');
const url = "https://topresa.ovh/api/omras.json?date[after]=11/11/1000";
const UpgradePayementUrl = "http://localhost:3000/omra/api/UpgradePayement/";


@Injectable({
  providedIn: 'root'
})
export class OmraService {
  
  private posts: Omra[] = [];
  private postsUpdated = new Subject<Omra[]>();
  private hotelhomelist: Omra[] = [];
  private hotelhomelistUpdated = new Subject<Omra[]>();
  private postsSub: Subscription;
  private Hexiste:string;

  //********njeimi */

  constructor(private toastr: ToastrService,private httpClient:HttpClient) {}



getHexiste(){
  return this.Hexiste;
}
  getReservationOmra(){
    return this.httpClient.get<Reservation>(ReservationHotelUrl,httpOptions);
  }

  deleteReservationOmra(id : string){
    this.httpClient.delete<{message: string }>(deleteReservationHotelUrl+id,httpOptions).subscribe((responseData) => {
    
     this.toastr.success('', responseData.message);

   });
 }
 UpgradePayement(id:any){
  this.httpClient.put<{message: string }>(UpgradePayementUrl+id,httpOptions).subscribe(responseData => {
    
    this.toastr.success('', responseData.message);

    });

}
 ReservationAccepter(id : string){
    this.httpClient.put<{ data: any, message: string }>( ReservationAccepter+id,httpOptions).subscribe(responseData => {
     
   this.toastr.success('', responseData.message);

   });
 }

 getReservationAccepter(){
   return this.httpClient.get<Reservation>( ReservationAccepterUrl,httpOptions);
 }

 getReservationEnAttent(){
   return this.httpClient.get<Reservation>(ReservationEnAttenteUrl,httpOptions);
   
 }
  
  
//*******************Njeimi**************************
  
getPostUpdateListener() {
  return this.postsUpdated.asObservable();
}


postContact(ReservationHotel:ReservationHotel){
  return this.httpClient.post<ReservationHotel>(AjoutReservationUrl,ReservationHotel);

}

getOmraBase() {
  this.httpClient
    .get<{ message: string; posts: any }>(
      "http://localhost:3000/omra/api/omra"
      
    )
    .pipe(map((postData) => {
      console.log('////////'+postData.posts);
      return postData.posts.map(post => {
                  return {
                    _id : post._id,
          id :  post.id,agent : post.agent,programme : post.programme,date : post.date,titre : post.titre,prix : post.prix,
   image:post.image
          
        };
      });
    }))
    .subscribe(transformedPosts => {
      this.posts = transformedPosts;
      this.postsUpdated.next([...this.posts]);
    });
}

addPost( hotel:Omra,images:any) {
  let existe="";
  this.httpClient
  .get<{  posts: any }>("http://localhost:3000/omra/api/existe/"+hotel.id)
  .subscribe(responseData => {
    console.log("###zebi "+responseData);
existe=""+responseData;
this.Hexiste=existe;
    if(existe=="false"){
      console.log("###"+responseData.posts);
      const post: Omra = hotel;
      post.image=images;
    
    
      this.httpClient
        .post<{ message: string, postId: string }>("http://localhost:3000/omra/api/AjoutOmra", post)
        .subscribe(responseData => {
          const id = responseData.postId;
          post.id = id;
          this.getOmraBase();
          this.postsSub = this.getPostUpdateListener()
        .subscribe((posts: Omra[]) => {
          this.posts = posts;
         
        
        });
                this.postsUpdated.next([...this.posts]);

        });
        this.toastr.success('', "Hotel added in the base");

    }else{

      console.log("##**#"+responseData.posts);

      this.toastr.error('', "Hotel existe in the base");

    }
    

  });
}

update( id :  string,titre : string,agent : string,programme : string,date : string,prix : number,image:any,idg:any) {
  const post: Omra = {    id : id,agent : agent,programme : programme,date : date,titre : titre,prix : prix,
    image:image
};
  this.httpClient
    .put("http://localhost:3000/omra/api/omraModif/"+idg, post)
    .subscribe(responseData => {
      this.getOmraBase();
      this.postsSub = this.getPostUpdateListener()
    .subscribe((posts: Omra[]) => {
      this.posts = posts;
     
    
    });
      this.postsUpdated.next([...this.posts]);
    });
}  

deletePost(postId: string) {
  this.httpClient.delete("http://localhost:3000/omra/api/posts/" + postId)
    .subscribe(() => {
      const updatedPosts = this.posts.filter(post => post.id !== postId);
      this.posts = updatedPosts;
      this.postsUpdated.next([...this.posts]);
    });
}

pubpost(item:any) {
   
  this.httpClient
    .post<{ message: string, postId: string }>("http://localhost:3000/omra/api/pub",item)
    .subscribe(responseData => {
      console.log(responseData.message);
    });
}



deleteall(){

this.httpClient.delete("http://localhost:3000/omra/api/deleteall/" + null)
.subscribe(() => {
console.log("dd");
});

}

  getOmra():Observable<Omra>{
    
    return this.httpClient.get<Omra>(url,httpOptions);
  }
 
}
