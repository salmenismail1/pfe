import { Component, OnInit, ViewChild } from '@angular/core';
import { LoginService } from '../../../Services/login.service';
import * as jwt_decode from 'jwt-decode';
import { MatDialog, MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';

const HistoriqueClientUrl = "http://localhost:3000/user/api/HistoriqueClient/"
const HistoriqueClientUrlH = "http://localhost:3000/user/api/HistoriqueClientHotel/"
const HistoriqueClientUrlC = "http://localhost:3000/user/api/HistoriqueClientCircuit/"
const HistoriqueClientUrlS = "http://localhost:3000/user/api/HistoriqueClientSoiree/"

@Component({
  selector: 'app-historique',
  templateUrl: './historique.component.html',
  styleUrls: ['./historique.component.scss']
})
export class HistoriqueComponent implements OnInit {

  constructor(private userServices: LoginService,public matDialog: MatDialog,private router:Router) { }

  listeReservationAccepter: MatTableDataSource<any>;
  HistoriqueColumns: string[] = ['Nom','Etat','actions'];
  ReservationAccepterColumns: string[] = ['Titre', 'Etat','Nom','actions'];

  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  searchKey;searchKey2;searchKey3;searchKey4;searchKey5;
  LReservationAccepter;
  choice;

  listeVoyage: MatTableDataSource<any>;
  listeSoiree: MatTableDataSource<any>;
  listeOmra: MatTableDataSource<any>;
  listeHotel: MatTableDataSource<any>;
  listeCircuit: MatTableDataSource<any>;
  

  LHotels;LVoyage;LOmra;Lsoiree;Lcircuit;

  ngOnInit() {
    this.GetListeReservationAccepter("Hotels");
    this.GetListeReservationAccepter("Voyages");
    this.GetListeReservationAccepter("Soiree");
    this.GetListeReservationAccepter("Circuits");
  }

  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
  onSearchClear2() {
    this.searchKey = "";
    this.applyFilter2();
  }
  onSearchClear3() {
    this.searchKey = "";
    this.applyFilter3();
  }
  onSearchClear4() {
    this.searchKey = "";
    this.applyFilter4();
  }
  onSearchClear5() {
    this.searchKey = "";
    this.applyFilter5();
  }

  applyFilter() {
    this.listeHotel.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter2() {
    this.listeVoyage.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter3() {
    this.listeSoiree.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter4() {
    this.listeCircuit.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter5() {
    this.listeReservationAccepter.filter = this.searchKey.trim().toLowerCase();
  }


  GetListeReservationAccepter(choice:any){

    console.log(choice)

    var decoded = jwt_decode(this.userServices.getToken());
    if(choice=="Hotels"){
      this.choice="Hotels";
      this.ReservationAccepterColumns = ['Titre', 'Etat','Nom','actions'];
    this.userServices.getHistorique(decoded.id,HistoriqueClientUrl).subscribe(      
      response =>{
        this.LHotels=response;
        this.listeHotel = new MatTableDataSource(this.LHotels);
        this.listeHotel.sort = this.sort;
        this.listeHotel.paginator = this.paginator;
        this.listeHotel.filterPredicate = (data, filter) => {
          return this.ReservationAccepterColumns.some(ele => {
            return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
          });
        };
        console.log(response);
      },
      err =>{"error"}
    );
    }else if(choice=="Voyages"){
      this.choice="Voyages";
      this.ReservationAccepterColumns = ['Titre', 'Etat','Nom','actions'];
      this.userServices.getHistorique(decoded.id,HistoriqueClientUrlH).subscribe(      
        response =>{
          this.LVoyage=response;
          this.listeVoyage = new MatTableDataSource(this.LVoyage);
          this.listeVoyage.sort = this.sort;
          this.listeVoyage.paginator = this.paginator;
          this.listeVoyage.filterPredicate = (data, filter) => {
            return this.ReservationAccepterColumns.some(ele => {
              return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
            });
          };
          console.log(response);
        },
        err =>{"error"}
      );
    }else if(choice=="Circuits"){
      this.choice="Circuits";
      this.ReservationAccepterColumns = ['Titre', 'Etat','Nom','actions'];
      this.userServices.getHistorique(decoded.id,HistoriqueClientUrlC).subscribe(      
        response =>{
          this.Lcircuit=response;
          this.listeCircuit = new MatTableDataSource(this.Lcircuit);
          this.listeCircuit.sort = this.sort;
          this.listeCircuit.paginator = this.paginator;
          this.listeCircuit.filterPredicate = (data, filter) => {
            return this.ReservationAccepterColumns.some(ele => {
              return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
            });
          };
          console.log(response);
        },
        err =>{"error"}
      );
    }else if(choice=="Soiree"){
      this.choice="Soiree";
      this.ReservationAccepterColumns = ['Titre', 'Etat','Nom','actions'];
      this.userServices.getHistorique(decoded.id,HistoriqueClientUrlS).subscribe(      
        response =>{
          this.Lsoiree=response;
          this.listeSoiree = new MatTableDataSource(this.Lsoiree);
          this.listeSoiree.sort = this.sort;
          this.listeSoiree.paginator = this.paginator;
          this.listeSoiree.filterPredicate = (data, filter) => {
            return this.ReservationAccepterColumns.some(ele => {
              return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
            });
          };
          console.log(response);
        },
        err =>{"error"}
      );
    }

    
  }
  
  

  deleteReservation(id:string,choice:string){
    this.userServices.RemoveReservation(id).subscribe(
      response =>{
        console.log(response);
        this.GetListeReservationAccepter(choice);
      }
    )
  }

}
