import { ReservationEnCourDetailComponent } from './../reservation-en-cour-detail/reservation-en-cour-detail.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { LoginService } from '../../../Services/login.service';
import * as jwt_decode from 'jwt-decode';
import { MatDialog, MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import * as html2pdf  from 'html2pdf.js';
import { Reservation } from '../../../Models/reservation';

const HistoriqueClientUrl = "http://localhost:3000/user/api/ReservationVoyageClientAccepter/"
const HistoriqueClientUrlH = "http://localhost:3000/user/api/ReservationHotelClientAccepter/"
const HistoriqueClientUrlC = "http://localhost:3000/user/api/ReservationCircuitClientAccepter/"
const HistoriqueClientUrlS = "http://localhost:3000/user/api/ReservationSoireeClientAccepter/"

@Component({
  selector: 'app-reservation-accepter',
  templateUrl: './reservation-accepter.component.html',
  styleUrls: ['./reservation-accepter.component.scss']
})
export class ReservationAccepterComponent implements OnInit {

  constructor(public dialog: MatDialog,private userServices: LoginService,public matDialog: MatDialog,private router:Router) { }

  listeReservationAccepter: MatTableDataSource<any>;
  HistoriqueColumns: string[] = ['Nom','Etat','actions'];
  ReservationAccepterColumns: string[] = ['Titre', 'Etat','Nom','actions'];

  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  searchKey;searchKey2;searchKey3;searchKey4;searchKey5;
  LReservationAccepter;
  choice;

  listeVoyage: MatTableDataSource<any>;
  listeSoiree: MatTableDataSource<any>;
  listeOmra: MatTableDataSource<any>;
  listeHotel: MatTableDataSource<any>;
  listeCircuit: MatTableDataSource<any>;
  

  LHotels;LVoyage;LOmra;Lsoiree;Lcircuit;

  ngOnInit() {
    this.GetListeReservationAccepter("Hotels");
    this.GetListeReservationAccepter("Voyages");
    this.GetListeReservationAccepter("Soiree");
    this.GetListeReservationAccepter("Circuits");
  }

  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
  onSearchClear2() {
    this.searchKey = "";
    this.applyFilter2();
  }
  onSearchClear3() {
    this.searchKey = "";
    this.applyFilter3();
  }
  onSearchClear4() {
    this.searchKey = "";
    this.applyFilter4();
  }
  onSearchClear5() {
    this.searchKey = "";
    this.applyFilter5();
  }

  applyFilter() {
    this.listeHotel.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter2() {
    this.listeVoyage.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter3() {
    this.listeSoiree.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter4() {
    this.listeCircuit.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter5() {
    this.listeReservationAccepter.filter = this.searchKey.trim().toLowerCase();
  }


  GetListeReservationAccepter(choice:any){

    console.log(choice)

    var decoded = jwt_decode(this.userServices.getToken());
    if(choice=="Hotels"){
      this.choice="Hotels";
      this.ReservationAccepterColumns = ['Titre', 'Etat','Nom','actions'];
    this.userServices.getHistorique(decoded.id,HistoriqueClientUrl).subscribe(      
      response =>{
        this.LHotels=response;
        this.listeHotel = new MatTableDataSource(this.LHotels);
        this.listeHotel.sort = this.sort;
        this.listeHotel.paginator = this.paginator;
        this.listeHotel.filterPredicate = (data, filter) => {
          return this.ReservationAccepterColumns.some(ele => {
            return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
          });
        };
        console.log(response);
      },
      err =>{"error"}
    );
    }else if(choice=="Voyages"){
      this.choice="Voyages";
      this.ReservationAccepterColumns = ['Titre', 'Etat','Nom','actions'];
      this.userServices.getHistorique(decoded.id,HistoriqueClientUrlH).subscribe(      
        response =>{
          this.LVoyage=response;
          this.listeVoyage = new MatTableDataSource(this.LVoyage);
          this.listeVoyage.sort = this.sort;
          this.listeVoyage.paginator = this.paginator;
          this.listeVoyage.filterPredicate = (data, filter) => {
            return this.ReservationAccepterColumns.some(ele => {
              return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
            });
          };
          console.log(response);
        },
        err =>{"error"}
      );
    }else if(choice=="Circuits"){
      this.choice="Circuits";
      this.ReservationAccepterColumns = ['Titre', 'Etat','Nom','actions'];
      this.userServices.getHistorique(decoded.id,HistoriqueClientUrlC).subscribe(      
        response =>{
          this.Lcircuit=response;
          this.listeCircuit = new MatTableDataSource(this.Lcircuit);
          this.listeCircuit.sort = this.sort;
          this.listeCircuit.paginator = this.paginator;
          this.listeCircuit.filterPredicate = (data, filter) => {
            return this.ReservationAccepterColumns.some(ele => {
              return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
            });
          };
          console.log(response);
        },
        err =>{"error"}
      );
    }else if(choice=="Soiree"){
      this.choice="Soiree";
      this.ReservationAccepterColumns = ['Titre', 'Etat','Nom','actions'];
      this.userServices.getHistorique(decoded.id,HistoriqueClientUrlS).subscribe(      
        response =>{
          this.Lsoiree=response;
          this.listeSoiree = new MatTableDataSource(this.Lsoiree);
          this.listeSoiree.sort = this.sort;
          this.listeSoiree.paginator = this.paginator;
          this.listeSoiree.filterPredicate = (data, filter) => {
            return this.ReservationAccepterColumns.some(ele => {
              return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
            });
          };
          console.log(response);
        },
        err =>{"error"}
      );
    }

    
  }
  
  pdf(){
    const options = {
      margin:       15,
      filename:     'myfile.pdf',
      image:        { type: 'jpeg' },
      jsPDF:        {orientation: 'landscape' }
    };
    var content = document.getElementById('content');
    html2pdf()
      .from(content)
      .set(options)
      .save();
  }

  deleteReservation(id:string){
    this.userServices.RemoveReservation(id).subscribe(
      response =>{
        console.log(response);
        this.GetListeReservationAccepter(this.choice);
      }
    )
  }

  openDialog(item:any) {
    const dialogRef = this.dialog.open(ReservationEnCourDetailComponent, {
      width: "330px",
      height: "400px",
      data: {
        item: item
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

}
