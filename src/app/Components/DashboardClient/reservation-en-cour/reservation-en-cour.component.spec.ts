import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationEnCourComponent } from './reservation-en-cour.component';

describe('ReservationEnCourComponent', () => {
  let component: ReservationEnCourComponent;
  let fixture: ComponentFixture<ReservationEnCourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationEnCourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationEnCourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
