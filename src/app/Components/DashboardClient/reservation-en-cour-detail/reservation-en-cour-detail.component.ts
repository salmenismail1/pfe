import { Reservation } from './../../../Models/reservation';
import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import * as html2pdf  from 'html2pdf.js';

@Component({
  selector: 'app-reservation-en-cour-detail',
  templateUrl: './reservation-en-cour-detail.component.html',
  styleUrls: ['./reservation-en-cour-detail.component.scss']
})
export class ReservationEnCourDetailComponent implements OnInit {

  reservation=new Reservation;
 
  constructor(
    public dialogRef: MatDialogRef<ReservationEnCourDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {
    console.log(this.data);
    this.reservation=this.data.item;
    this.data="3asba";
    console.log(this.data);

}
pdf(){
  const options = {
    margin:       15,
    filename:     'myfile.pdf',
    image:        { type: 'jpeg' },
    jsPDF:        {orientation: 'landscape' }
  };
  var content = document.getElementById('content');
  html2pdf()
    .from(content)
    .set(options)
    .save();
}

close() {
  this.dialogRef.close({
    data:this.data
  });
}

}
