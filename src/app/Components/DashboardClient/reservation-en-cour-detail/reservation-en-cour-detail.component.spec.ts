import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationEnCourDetailComponent } from './reservation-en-cour-detail.component';

describe('ReservationEnCourDetailComponent', () => {
  let component: ReservationEnCourDetailComponent;
  let fixture: ComponentFixture<ReservationEnCourDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationEnCourDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationEnCourDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
