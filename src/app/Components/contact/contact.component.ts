import { Component, OnInit } from '@angular/core';
import { ContactService } from '../../Services/contact.service';
import { Contact } from '../../Models/contact';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { LoginService } from '../../Services/login.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { LoginComponent as ModalComponent } from '../login/login.component';
import * as jwt_decode from 'jwt-decode';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  contact=new Contact;
  reactiveform :FormGroup;
  form:FormGroup;
  constructor(public matDialog: MatDialog,private loginserv:LoginService,private formbuilder:FormBuilder,private contactserv:ContactService) { }
  


  ngOnInit() {
    this.reactiveform=this.formbuilder.group({
      'nom' : new FormControl('', [Validators.required]),
      'email' : new FormControl('',[Validators.email,Validators.required]),
      'sujet' : new FormControl('', [Validators.required]),
      'tel' : new FormControl('', [Validators.required, Validators.minLength(8),Validators.pattern('[0-9]*')]),
      'message' : new FormControl('', [Validators.required]),
    });
    

  }
  afficher(){
    if( this.reactiveform.valid && this.loggued()){
      var decoded = jwt_decode(this.loginserv.getToken());

      this.contact.response="";
      this.contact.idClient=decoded.id+"";
      console.log(this.contact);
      this.contactserv.addPost(this.contact);
      this.reactiveform.reset();
    }else if(!this.loggued()){
      this.openModal();
    }else{
      console.log("error");
    }
    
  
  }
  
  loggued(){
    return !this.loginserv.loggedIn();
  }
  openModal() {
   
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modal-component";
    this.matDialog.open(ModalComponent, {
      height: '600px',
      width: '500px',
    });
    
  }
}
