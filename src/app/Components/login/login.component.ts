import { Component, OnInit } from '@angular/core';
import {FormControl,AbstractControl, FormGroup, Validators, FormBuilder} from "@angular/forms";
import { ModalDirective } from 'angular-bootstrap-md';
import { MatDialogRef } from '@angular/material/dialog';
import { LoginService } from '../../Services/login.service';
import { Login } from '../../Models/login';
import { User } from '../../Models/user';
import * as jwt_decode from 'jwt-decode';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppVarsGlobal } from '../../app.vars.global';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  log = new Login  ;
  user = new User;
  reactiveform :FormGroup;
  form:FormGroup;

  constructor(private toastr: ToastrService,public dialogRef: MatDialogRef<LoginComponent>,private loginserv:LoginService,private formbuilder:FormBuilder
    ,private router:Router, private vars: AppVarsGlobal) { }

  ngOnInit(){
    
    

    this.reactiveform=this.formbuilder.group({
      'username' : new FormControl('',[Validators.required]),
      'email' : new FormControl('', [Validators.email,Validators.required]),
      'password' : new FormControl('', [Validators.required]),
      'repassword' : new FormControl('',[Validators.required]),
      'cin' : new FormControl('', [Validators.required, Validators.minLength(8),Validators.pattern('[0-9]*')]),
      'tel' : new FormControl('', [Validators.required, Validators.minLength(8),Validators.pattern('[0-9]*')]),
      
    },
    {validator: this.passwordMatchValidator}
    )
    
    this.reactiveform.value.username="";
  }
  passwordMatchValidator(frm: FormGroup) {
    return frm.get('password').value === frm.get('repassword').value
       ? null : {'mismatch': true};
 }
  

  actionFunction() {
    alert("You have logged out.");
    this.closeModal();
  }

  closeModal() {
    this.dialogRef.close();
 
  }

  login(){
    this.loginserv.postLogin(this.log).subscribe(
      
      response =>{
            localStorage.setItem('token',response.token);
            let decoded : any = jwt_decode(response.token);
            this.dialogRef.close();
            //****** */

            this.redirect(decoded.id);

      },
      err =>{this.toastr.error('', 'Verifiez vos Donnees');}
    )
  }

  
   redirect(id){
    this.loginserv.getOnUser(id).subscribe(
      response => {
        console.log(response);

      if(response.type==="admin")
        {
          this.router.navigate(['/admin/dashboard']);
        }
        else{
          this.router.navigate(['/home']);
        }
        this.toastr.success('', 'Autontification avec succée');


        this.vars.type = response.type 
      });
  }
  






  inscription(){
    this.user.type="client";
    this.loginserv.Inscrit(this.user).subscribe(
      
      response =>{
            this.dialogRef.close();
      },
      err =>{console.log('erreur dinscription')}
    )
  }
 
  




}