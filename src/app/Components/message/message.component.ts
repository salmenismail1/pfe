import { User } from './../../Models/user';
import { Chat } from './../../Models/chat';
import { ChatService } from './../../Services/chat.service';
import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;


import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';
import { LoginService } from '../../Services/login.service';


@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  constructor(private loginserv:LoginService,private chatServices: ChatService,private router:Router) { } 

  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['_id', 'nom','actions'];
  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  searchKey: string;
  l: User[];
  private postsSub: Subscription;
  item;
  visible=false;
  message;
  ngOnInit() {
    var decoded = jwt_decode(this.loginserv.getToken());

    this.chatServices.getOneAdminsBase(decoded.id);

    this.getData();
     
  }

 
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  onMessage(item) {
   this.item=item;
   this.visible=true;

  }
  onEnvoyer() {

    var chat=new Chat();
    chat.sender=this.chatServices.sender.nom;
    chat.reciver=this.item.nom;
    chat.message=this.message;
    chat.reciverId=this.item._id;

    console.log(chat);
 

      this.chatServices.addPost(chat);
   }

  getData(){
    this.chatServices.getAdminsBase();
    this.postsSub = this.chatServices.getPostUpdateListener()
      .subscribe((posts: []) => {
        this.l = posts;
        this.listData = new MatTableDataSource(this.l);

        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele != 'actions' && (data[ele]+"").toString().toLowerCase().indexOf(filter) != -1;
          });
        };
      });
     
  }

}
