import { Component, OnInit } from '@angular/core';
import { OmraService } from '../../Services/omra.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Reservation } from '../../Models/reservation';
import { ReservationService } from '../../Services/reservation.service';
import { Chambre } from '../../Models/chambre';
import {FormControl, FormGroup, Validators, FormBuilder} from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import * as html2pdf  from 'html2pdf.js';

const AjoutReservationUrl = "http://localhost:3000/omra/api/reservationOmra";

@Component({
  selector: 'app-omra',
  templateUrl: './omra.component.html',
  styleUrls: ['./omra.component.scss']
})
export class OmraComponent implements OnInit {
  text;
  ListeOmra;ListeOmraBase;
  reactiveform :FormGroup;
  reservation = new Reservation;
  ListeReservation
  adult;enfant ;bb;
  id;
  unvoyage;
  voyage;
  x;
  nb;
  TChambre;nbChambre = new Array(5);
  prix : number;
  venteadultesingle:number;venteadultedble:number;tarifbebe:number;
  datedepart;dateretour;

  Adulte;Enfant;Bebe;nbrAdulte ; nbrEnfant ; nbrBebe; TAdulte ; TEnfant; TBebe;TnbrAdulte;TnbrEnfant;TnbrBebe;
  


  Tprix;program;
  date;min;max;test="";titre;

  constructor(private router:Router, private toastr: ToastrService,private omraService:OmraService,private serviceReservation:ReservationService) { }

  ngOnInit() {

    this.nb=4;

    console.log(this.text);

    this.omraService.getOmra().subscribe(        
      response =>{
        this.ListeOmra=response;
        console.log(this.ListeOmra)
        this.compare(this.ListeOmraBase,this.ListeOmra);

      },
      err =>{"error"}
      
    )

    this.omraService.getOmraBase();
      this.omraService.getPostUpdateListener().subscribe(        
        response =>{
      
        this.ListeOmraBase=response;
        console.log(this.ListeOmraBase)
        this.compare(this.ListeOmraBase,this.ListeOmra);
      },
      err =>{"error"}
    );
    

    this.reactiveform= new FormGroup({
      'Nom' : new FormControl('',[Validators.required]),
      'Email' : new FormControl('', [Validators.email,Validators.required]),
      'Tel' : new FormControl('', [Validators.required, Validators.minLength(8),Validators.pattern('[0-9]*')]),
      
    })
    
  }
  get Nom() { return this.reactiveform.get('Nom'); }
  get Email() { return this.reactiveform.get('Email'); }
  get Tel() { return this.reactiveform.get('Tel'); }
  

  chambre(){
    for(let i=0;i<this.nb;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+i);

      if(selectElement.hidden==true){
        selectElement.hidden=false;
        console.log(selectElement);
        console.log(this.adult)
        return;
      }
    }
  }
  delchambre(i:number){
    const selectElement = <HTMLSelectElement>document.getElementById('rowid'+i);
    selectElement.hidden=true;

    const select1 = <HTMLSelectElement>document.getElementById('homme'+i);
    select1.selectedIndex=0;

    const select2 = <HTMLSelectElement>document.getElementById('enfant'+i);
    console.log(select2.selectedIndex)
    select2.selectedIndex=0;

    const select3 = <HTMLSelectElement>document.getElementById('bebe'+i);
    select3.selectedIndex=0;
    console.log(selectElement);

    console.log(this.adult)
   

  }
  ngAfterViewInit(): void {
    
  }


  
  calculPrix(){
    
    this.Tprix=0;
     for(let j=0;j<this.nb;j++){
       
       const selectElement = <HTMLSelectElement>document.getElementById('rowid'+j);
 
       if(selectElement.hidden==false){
         
         const select1 = <HTMLSelectElement>document.getElementById('homme'+j);
         const select2 = <HTMLSelectElement>document.getElementById('enfant'+j);
         
 
         this.Tprix+= (this.prix*select1.selectedIndex) + (this.prix*select2.selectedIndex);
         console.log(this.prix);
       }
     }
     console.log(this.Tprix);
   }
 

  clear() {

    this.reactiveform.reset();
    
    for(let i=0;i<this.nb;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+i);
      selectElement.hidden=true;
      const firstElement = <HTMLSelectElement>document.getElementById('rowid'+0);
      firstElement.hidden=false;
        const select1 = <HTMLSelectElement>document.getElementById('homme'+i);
        select1.selectedIndex=0;
        const select2 = <HTMLSelectElement>document.getElementById('enfant'+i);
        select2.selectedIndex=0;
        const select3 = <HTMLSelectElement>document.getElementById('bebe'+i);
        select3.selectedIndex=0;

    }
    
  }
  
  DateTitre(titre:string,date:Date,prix:number,program:any){

    this.titre=titre;
    this.date=date;
    this.prix=prix;
    this.program=program;

    console.log(this.prix)

  }
  testing1(){
    console.log(this.test);
    this.test= ""; 
    console.log(this.test);
    this.reset();
  }
  
    testing4(){
      //console.log("min : "+this.min+"max : "+this.max);
      this.test= "f" ;
      console.log(this.test)
    }
    reset(){
      this.min=0;
      this.max=3000;

    }

    pdf(){
      const options = {
        margin:       15,
        filename:     'myfile.pdf',
        image:        { type: 'jpeg' },
        jsPDF:        {orientation: 'landscape' }
      };
      var content = document.getElementById('content');
  
      html2pdf()
        .from(content)
        .set(options)
        .save();
    }

  
    affiche(){

      this.TnbrAdulte= new Array();  this.TnbrEnfant= new Array();  this.TnbrBebe= new Array();
      this.nbrAdulte=0; this.nbrEnfant=0; this.nbrBebe=0;
      this.TChambre=[] ;
      let cmp = 0;
      
      for(let j=0;j<this.nb;j++){
        
        const selectElement = <HTMLSelectElement>document.getElementById('rowid'+j);
  
        
        if(selectElement.hidden==false){
          
           this.Adulte = <HTMLSelectElement>document.getElementById('homme'+j);
           this.Enfant = <HTMLSelectElement>document.getElementById('enfant'+j);
           this.Bebe = <HTMLSelectElement>document.getElementById('bebe'+j);
  
           this.nbrAdulte +=this.Adulte.selectedIndex ;
           this.nbrEnfant+= this.Enfant.selectedIndex;
           this.nbrBebe += this.Bebe.selectedIndex;
  
           this.TnbrAdulte[cmp]=this.Adulte.selectedIndex;
           this.TnbrEnfant[cmp]=this.Enfant.selectedIndex;
           this.TnbrBebe[cmp]=this.Bebe.selectedIndex;
  
           cmp+=1;
  
          let chambre= new Chambre();
        chambre.num=j+1+"";
        
        console.log(chambre)
        this.TChambre.push(chambre);
        }
  
        this.TAdulte=new Array(this.nbrAdulte)
        this.TEnfant=new Array(this.nbrEnfant)
        this.TBebe=new Array(this.nbrBebe)
  
      }
      
      this.reservation.DateReservation= new Date().toString();
      
      this.reservation.Prix=this.Tprix;
    this.reservation.DateDepart=this.date;
    this.reservation.Titre=this.titre;
      this.reservation.Nom=this.reactiveform.value.Nom;
      this.reservation.Email=this.reactiveform.value.Email;
      this.reservation.Tel=this.reactiveform.value.Tel;
      console.log(this.reservation.Nom);
    }
  
    teste(){
  
      if(this.nbrAdulte==0){
        this.TAdulte=new Array(this.nbrAdulte)
      }
      else{
        this.TAdulte=new Array(this.nbrAdulte-1)
      }
      if(this.nbrEnfant==0){
        this.TEnfant=new Array(this.nbrEnfant)
      }
      else{
        this.TEnfant=new Array(this.nbrEnfant-1)
      }

      let j=1;
      let k = 0;
      let t = [];
  
      for(let i=0;i<this.nbrAdulte;i++){
        
        const selectElement = <HTMLSelectElement>document.getElementById('Adulte'+i);
        
        if(j<this.TnbrAdulte[k] ){
  
          t.push(selectElement.value+"")
          j+=1;
  
        }
          else{
           
          t.push(selectElement.value+"")
          this.TChambre[k].adulte=t;
          t=[];
          j=1;
          k+=1;
  
          }
          
        
      }
         
      j=1;
      k = 0;
      t=[];
  
      for(let i=0;i<this.nbrEnfant;i++){
        console.log(i)
        const selectElement2 = <HTMLSelectElement>document.getElementById('Enfant'+i);
        
          if(j<this.TnbrEnfant[k] ){
  
            t.push(selectElement2.value+"")
            j+=1;
    
          }
            else{
             
            t.push(selectElement2.value+"")
            this.TChambre[k].enfant=t;
            t=[];
            j=1;
            k+=1;
            
        }
      }
  
      j=1;
      k = 0;
      t=[];
  
      for(let i=0;i<this.nbrBebe;i++){
        const selectElement = <HTMLSelectElement>document.getElementById('Bebe'+i);
        
        if(j<this.TnbrBebe[k] ){
  
          t.push(selectElement.value+"")
          j+=1;
  
        }
        else{
           
          t.push(selectElement.value+"")
          this.TChambre[k].bb=t;
          t=[];
          j=1;
          k+=1;
          
      }
          
        
      }
      this.reservation.Chambre=this.TChambre;
    }
  
    AjoutCambre(){
      this.serviceReservation.AjoutReservation(AjoutReservationUrl,this.reservation).subscribe(        
        response =>{
          console.log(response)
         this.toastr.success('Merci d\'attendre l\'acceptation de votre reservation', 'Réservation Voyage avec succée');
         
         
        },
        
        
        err =>{this.toastr.error('', 'Verifiez vos Donnees')}
        
      )
    }

    compare(lstbase:any,lstapi:any){
      if(lstbase.length>0 && lstapi.length>0){
  
      for(let item of lstbase){
      for(let item2 of lstapi){
        if(item.id==item2.id){
          if(item2.prix>=item.prix){
            lstapi[lstapi.indexOf(item2)]=[];
          }else{
            lstbase[lstbase.indexOf(item)]=[];

  
          }
        } 
      }
    }
  }
  this.ListeOmra=lstapi;
  this.ListeOmraBase=lstbase;

}

sourceBase(item:any){
  let data=JSON.stringify(item);
  localStorage.setItem('source',"base");
  localStorage.setItem('item',data);

}
sourceApi(item:any){
  let data=JSON.stringify(item);
  localStorage.setItem('source',"api");
  localStorage.setItem('item',data);
}



}
