import { Component, OnInit } from '@angular/core';
import { VoyagesService } from '../../Services/voyages.service';
import { Voyages } from '../../Models/voyages';
@Component({
  selector: 'app-voyage',
  templateUrl: './voyage.component.html',
  styleUrls: ['./voyage.component.css']
})
export class VoyageComponent implements OnInit {
  voyage = new Voyages;
  ListeVoyages;ListeVoyagesBase;
  min=0;max=3000;test="";text;d;
  listPays=[];pays;
  constructor(private voyageService:VoyagesService) { }
  ngOnInit() {
    this.voyageService.getVoyage().subscribe(        
      response =>{
        this.ListeVoyages=response;
        console.log(response)
        for(let i=0;i<this.ListeVoyages.length;i++){
          console.log("*********"+ this.ListeVoyages[i].imageVoyages[0]);
          if(this.ListeVoyages[i].pays[0]!=undefined){
            if(!this.listPays.includes(this.ListeVoyages[i].pays[0].libelle)){
                this.listPays.push(this.ListeVoyages[i].pays[0].libelle);
          }
        }
        }
      },
      err =>{"error"}
    )


      this.voyageService.getVoyageBase();
      this.voyageService.getPostUpdateListener().subscribe(        
        response =>{
        this.ListeVoyagesBase=response;
        console.log(this.ListeVoyagesBase)
        this.compare(this.ListeVoyagesBase,this.ListeVoyages);
      },
      err =>{"error"}
    );

  

}
  testing1(){
    console.log(this.test);
    this.test= ""; 
    this.reset();
  }
    testing4(){
      this.test="prix";
      console.log("min : "+this.min+"max : "+this.max);
      this.test= "f" ;
      this.text= ""; 
    }
    reset(){
      this.min=0;
      this.max=3000;
    }
    compare(lstbase:any,lstapi:any){
      if(lstbase.length>0 && lstapi.length>0){
      for(let item of lstbase){
      for(let item2 of lstapi){
        if(item.id==item2.id){
          if(item2.venteadultesingle>=item.venteadultesingle){
            lstapi[lstapi.indexOf(item2)]=[];
          }else{
            lstbase[lstbase.indexOf(item)]=[];

          }
        } 
      }
    }
  }
  this.ListeVoyages=lstapi;
  this.ListeVoyagesBase=lstbase;
    }
    sourceBase(item:any){
      let data=JSON.stringify(item);
      localStorage.setItem('source',"base");
      localStorage.setItem('item',data);
    }
    sourceApi(item:any){
      let data=JSON.stringify(item);
      localStorage.setItem('source',"api");
      localStorage.setItem('item',data);
    }
}