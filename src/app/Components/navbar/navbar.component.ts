import { Component, OnInit } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;
import { LoginComponent as ModalComponent } from '../login/login.component';
import { LoginService } from '../../Services/login.service';
import { Login } from '../../Models/login';
import * as jwt_decode from 'jwt-decode';
import { AppVarsGlobal } from '../../app.vars.global';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  
})
export class NavbarComponent implements OnInit {
  user = new Login ;

  constructor(public matDialog: MatDialog,private loginserv:LoginService,  private vars: AppVarsGlobal) { }

  taille=100;

  ngOnInit() {

   this.vars.type= this.vars.login();

    
  }
  openModal() {
   
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modal-component";
    this.matDialog.open(ModalComponent, {
      height: '600px',
      width: '500px',
    });
    
  }
  
  GetStatut(){
  if(this.loggued()){
      return this.vars.type;
    
    }
    return "";

}
  loggued(){
    return !this.loginserv.loggedIn();
  }

  logOut(){
      this.loginserv.logOut()
      this.vars.type='';
    }

}