import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoyagereservationComponent } from './voyagereservation.component';

describe('VoyagereservationComponent', () => {
  let component: VoyagereservationComponent;
  let fixture: ComponentFixture<VoyagereservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoyagereservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoyagereservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
