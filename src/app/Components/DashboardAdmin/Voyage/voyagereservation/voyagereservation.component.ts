import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;
import { AddVoyageComponent as ModalComponent } from '../add-voyage/add-voyage.component';
import { VoyagemodifComponent as ModalComponent2 } from '../voyagemodif/voyagemodif.component';

import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { VoyagesService } from '../../../../Services/voyages.service';
import { Voyages } from '../../../../Models/voyages';
import { LoginService } from '../../../../Services/login.service';
@Component({
  selector: 'app-voyagereservation',
  templateUrl: './voyagereservation.component.html',
  styleUrls: ['./voyagereservation.component.scss']
})
export class VoyagereservationComponent implements OnInit {

  constructor(private voyageServices: VoyagesService,private userServices: LoginService,public matDialog: MatDialog,private router:Router) { } 

  listData: MatTableDataSource<any>;
  listeReservationAccepter : MatTableDataSource<any>;
  listeReservationEnAttente : MatTableDataSource<any>;

  displayedColumns: string[] = ['id', 'nom','actions'];
  ReservationHotelColumns: string[] = ['Titre', 'Email','Prix','actions'];
  ReservationHotelColumns2: string[] = ['Titre', 'Email','Prix','actions'];

  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  searchKey: string;searchKey2;searchKey3;
  l: Voyages[];LReservationAccepter ;LReservationEnAttente ;
  private postsSub: Subscription;

  

  ngOnInit() {
    
    //this.getData();
    this.GetListeReservationAccepter();
    this.GetListeReservationEnAttente();
     
  }


  onSearchClear2() {
    this.searchKey2 = "";
    this.applyFilter2();
  }
  onSearchClear3() {
    this.searchKey3 = "";
    this.applyFilter3();
  }

  applyFilter2() {
    this.listeReservationAccepter.filter = this.searchKey2.toString().trim().toLowerCase();
  }
  applyFilter3() {
    this.listeReservationEnAttente.filter = this.searchKey3.toString().trim().toLowerCase();
  }

deleteReservationVoyage(id:string){
  console.log(id);
  console.log(id);
  const main=async () => {

    try {
     
      this.voyageServices.DeleteReservationVoyage(id);

    }
    catch (error) {
        console.error(error);
    }

};
main().then(() =>{ 
  this.GetListeReservationAccepter();
  this.GetListeReservationEnAttente();
});
}


 

  GetListeReservationAccepter(){
  
        this.voyageServices.getReservationAccepter().subscribe(        
          async response =>{
            this.LReservationAccepter= await response;
            this.listeReservationAccepter = new MatTableDataSource(this.LReservationAccepter);
            this.listeReservationAccepter.sort = this.sort;
            this.listeReservationAccepter.paginator = this.paginator;
            this.listeReservationAccepter.filterPredicate = (data, filter) => {
              console.log("-------"+filter)
              return this.ReservationHotelColumns.some(ele => {
                return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
              });
            };
            console.log(response);
          },
          err =>{"error"}
        );
            
  }

  GetListeReservationEnAttente(){
         
          this.voyageServices.getReservationEnAttent().subscribe(        
            async response =>{
              this.LReservationEnAttente= await response;
              this.listeReservationEnAttente = new MatTableDataSource(this.LReservationEnAttente);
              this.listeReservationEnAttente.sort = this.sort;
              this.listeReservationEnAttente.paginator = this.paginator;
              this.listeReservationEnAttente.filterPredicate = (data, filter) => {
                return this.ReservationHotelColumns2.some(ele => {
                  return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
                });
              };
              console.log(response);
            },
            err =>{"error"}
          );    
  }

  ReservationAccepter(id : string){
    
    const main=async () => {

      try {
       
        this.voyageServices.ReservationAccepter(id);
  
      }
      catch (error) {
          console.error(error);
      }
  
  };
  main().then(() =>{ 
    this.GetListeReservationAccepter();
    this.GetListeReservationEnAttente();
  });
  }

  UpgradePayement(id:any){
    const main=async () => {

      try {
       
        this.voyageServices.UpgradePayement(id);
        this.GetListeReservationAccepter();

      }
      catch (error) {
          console.error(error);
      }
  
  };
  main().then(() =>{ 
    this.GetListeReservationAccepter();

  });
  }

}
