import { Component, OnInit, Inject, ɵɵqueryRefresh } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HotelsService } from '../../../../Services/hotels.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { VoyagesService } from '../../../../Services/voyages.service';

@Component({
  selector: 'app-voyagemodif',
  templateUrl: './voyagemodif.component.html',
  styleUrls: ['./voyagemodif.component.scss']
})
export class VoyagemodifComponent implements OnInit {

  title = 'fileUpload';
  images;
  multipleImages = [];
  imagePreview: string='';
  form: FormGroup;
  imageverif=false;
  imagePreview2:string[]=null;
  formvalid=false;
    datacomp:any;

  constructor(public dialogRef: MatDialogRef<VoyagemodifComponent>,private voyageServices:VoyagesService,private http: HttpClient,@Inject(MAT_DIALOG_DATA) public data:any){}

  ngOnInit(){
    this.imagePreview2=null;

    this.form = new FormGroup({
      id: new FormControl(null, {
        validators: [Validators.required]
      }),
      nom: new FormControl(null, { validators: [Validators.required] }),
      pays: new FormControl(null, { validators: [Validators.required] }),
      titre: new FormControl(null, { validators: [Validators.required] }),
      venteadultesingle: new FormControl(null, { validators: [Validators.required] }),
      prix: new FormControl(null, { validators: [Validators.required] }),
      venteadultedble: new FormControl(null, { validators: [Validators.required] }),
      vente3rdad: new FormControl(null, { validators: [Validators.required] }),
      venteenfant2ad: new FormControl(null, { validators: [Validators.required] }),
      venteenfant1ad: new FormControl(null, { validators: [Validators.required] }),
      tarifbebe: new FormControl(null, { validators: [Validators.required] }),
      venteenfant12ans: new FormControl(null, { validators: [Validators.required] }),
      venteenfant6ans: new FormControl(null, { validators: [Validators.required] }),

      datedepart: new FormControl(null, { validators: [Validators.required] }),
      dateretour: new FormControl(null, { validators: [Validators.required] }),
    
      programme: new FormControl(null, { validators: [Validators.required] }),

      image: new FormControl(null, {
        validators: [Validators.required]
      })
    });
  }
 
  selectMultipleImage(event){
    this.valid();
    this.imagePreview2=[];
    if (event.target.files.length > 0) {
      this.multipleImages = event.target.files;
      for (var i = 0; i < event.target.files.length; i++) {  
        let file=event.target.files[i];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
        reader.onload = () => {
          this.imagePreview2.push( reader.result as string);
        };
        reader.readAsDataURL(event.target.files[i]);
        if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

          this.imageverif=true;
          this.valid();

          console.log(this.imageverif);
    
          }else{
            this.valid();

            this.imageverif=false;
            console.log(this.imageverif);
    
          }
      }  
      console.log(this.imagePreview2);
     
    }

  }
  valid(){
    if(this.form.value.id!="" && this.form.value.nom!="" && this.form.value.pays!="" && this.form.value.titre!="" &&
    this.form.value.venteadultesingle!=null && this.form.value.datedepart!="" && this.form.value.dateretour!="" && this.form.value.name!="" &&
    this.form.value.venteadultedble!=null && this.form.value.vente3rdad!=null && this.form.value.venteenfant2ad!=null && this.form.value.venteenfant1ad!=null &&
    this.form.value.tarifbebe!=null && this.form.value.venteenfant12ans!=null && this.form.value.venteenfant6ans!=null &&
    (this.imageverif==true || this.imagePreview2==null)
    ){
    this.formvalid=true;
    console.log(this.form.value.nom+"***");
    }else{
      this.formvalid=false;
      console.log(this.formvalid+"***"+this.form.value.pcvente);

    }
  }


  onSubmit(idg:string){
    console.log(idg);
    console.log("//////////**////"+this.imageverif);
    

    if(this.form.value.id!="" && this.form.value.nom!="" && this.form.value.pays!="" && this.form.value.titre!="" &&
    this.form.value.venteadultesingle!="" && this.form.value.datedepart!="" && this.form.value.dateretour!="" && this.form.value.name!="" &&
    this.form.value.venteadultedble!="" && this.form.value.vente3rdad!="" && this.form.value.venteenfant2ad!="" && this.form.value.venteenfant1ad!="" &&
    this.form.value.tarifbebe!="" && this.form.value.venteenfant12ans!="" && this.form.value.venteenfant6ans!="" &&
    (this.imageverif==true || this.imagePreview2==null)
    ){

      if(this.imagePreview2==null || this.imagePreview2==[]){
        this.voyageServices.update(this.form.value.id,this.form.value.nom,this.form.value.pays,
          this.form.value.titre,this.form.value.venteadultesingle,this.form.value.venteadultedble,this.form.value.vente3rdad,
          this.form.value.venteenfant2ad,this.form.value.venteenfant1ad,this.form.value.tarifbebe,this.form.value.venteenfant12ans,this.form.value.venteenfant6ans,
          this.form.value.datedepart , this.form.value.dateretour,this.data.comp.image,idg,this.form.value.programme);

}else{
  this.voyageServices.update(this.form.value.id,this.form.value.nom,this.form.value.pays,
    this.form.value.titre,this.form.value.venteadultesingle,this.form.value.venteadultedble,this.form.value.vente3rdad,
    this.form.value.venteenfant2ad,this.form.value.venteenfant1ad,this.form.value.tarifbebe,this.form.value.venteenfant12ans,this.form.value.venteenfant6ans,
    this.form.value.datedepart , this.form.value.dateretour,this.imagePreview2,idg,this.form.value.programme);
}
      
      
        this.close();

      /*ng serve --live-reload false
      location.reload();
            window.stop();

      */
     


    }else{
if(this.imagePreview2.length<1){
  alert('image non valid');

}else{
  alert('remplir tous les correctement');

}
    }
   
  }

  onMultipleSubmit(){
    const formData = new FormData();
    for(let img of this.multipleImages){
      formData.append('files', img);
    }

    this.http.post<any>('http://localhost:3000/multipleFiles', formData).subscribe(
      (res) => console.log(res),
      (err) => console.log(err)
    );
  }


  close(){
        this.dialogRef.close();


  }
}
