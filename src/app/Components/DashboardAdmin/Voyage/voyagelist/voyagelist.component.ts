import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;
import { AddVoyageComponent as ModalComponent } from '../add-voyage/add-voyage.component';
import { VoyagemodifComponent as ModalComponent2 } from '../voyagemodif/voyagemodif.component';

import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { VoyagesService } from '../../../../Services/voyages.service';
import { Voyages } from '../../../../Models/voyages';

@Component({
  selector: 'app-voyagelist',
  templateUrl: './voyagelist.component.html',
  styleUrls: ['./voyagelist.component.scss']
})
export class VoyagelistComponent implements OnInit {

  constructor(private voyageServices: VoyagesService,public matDialog: MatDialog,private router:Router) { } 

  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['id', 'nom','actions'];
  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  searchKey: string;
  l: Voyages[];
  private postsSub: Subscription;
  ngOnInit() {
    
    this.getData();
     
  }

 
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  openModal() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modal-component-voyage";
    const g=this.matDialog.open(ModalComponent, {
      height: '800px',
      width: '500px',
    });
    g.afterClosed().subscribe(() => {
     
      
        this.getData();
      
   

  });

  }
  openModifModal(item:any){
    console.log(item);
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modifmodal-component-voyage";
    const g=this.matDialog.open(ModalComponent2, {
      height: '900px',
      width: '500px',
      data: { comp: item },
    });
    
    g.afterClosed().subscribe(() => {

      
        this.getData();
      
            

  });
  
  console.log(item);

  }

delete(id:string){

  const main=async () => {

    try {
     
      this.voyageServices.deletePost(id);
      this.postsSub = this.voyageServices.getPostUpdateListener().subscribe((posts: Voyages[]) => {
        this.l = posts;
      });
    }
    catch (error) {
        console.error(error);
    }

};
main().then(() =>{ 
  this.listData = new MatTableDataSource(this.l);
  this.listData.sort = this.sort;
  this.listData.paginator = this.paginator;
  this.listData.filterPredicate = (data, filter) => {
    return this.displayedColumns.some(ele => {
      return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
    });
  };
});
     
}
  getData(){
   
    this.voyageServices.getVoyageBase();
    this.postsSub = this.voyageServices.getPostUpdateListener()
      .subscribe((posts: Voyages[]) => {
        const main=async () => {

          try {
           
            console.log("hethi el l 9bal matet3aba el l");
            console.log(this.l);
          this.l = posts;
          console.log("hethi el l ba3d matet3aba el l");
          console.log(this.l);
  
      
          }
          catch (error) {
              console.error(error);
          }
      
      };
      main().then(() =>{ 
        this.listData = new MatTableDataSource(this.l);
  
        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
          });
        };
      }).catch(() => console.error('Failed!'));
      });
  }

}
