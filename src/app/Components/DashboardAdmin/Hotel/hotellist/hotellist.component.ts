import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;
import { AddHotelComponent as ModalComponent } from '../add-hotel/add-hotel.component';
import { HotelmodifComponent as ModalComponent2 } from '../hotelmodif/hotelmodif.component';

import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { HotelsService } from '../../../../Services/hotels.service';
import { Subscription } from 'rxjs';
import { Hotels } from '../../../../Models/hotels';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hotellist',
  templateUrl: './hotellist.component.html',
  styleUrls: ['./hotellist.component.scss']
})
export class HotellistComponent implements OnInit {

  constructor(private hotelServices: HotelsService,public matDialog: MatDialog,private router:Router) { } 

  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['id', 'nom','actions'];
  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  searchKey: string;
  l: Hotels[];
  private postsSub: Subscription;
  ngOnInit() {
    
    this.getData();
     
  }

 
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  openModal() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modal-component";
    const g=this.matDialog.open(ModalComponent, {
      height: '800px',
      width: '500px',
    });
    g.afterClosed().subscribe(() => {
      // Do stuff after the dialog has closed
      
        this.getData();

  
     
      
    
      });

  }
  openModifModal(item:any,event){
    console.log(item);
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modifmodal-component";
    const g=this.matDialog.open(ModalComponent2, {
      height: '900px',
      width: '500px',
      data: { comp: item },
    });
    g.afterClosed().subscribe(() => {

      // Do stuff after the dialog has closed
     
        console.log("aaa");
        this.getData();

  
      

  });
  
  console.log(item);

  }

delete(id:string){

  const main=async () => {

    try {
     
      this.hotelServices.deletePost(id);
      this.postsSub = this.hotelServices.getPostUpdateListener().subscribe((posts: Hotels[]) => {
        this.l = posts;
      });
    }
    catch (error) {
        console.error(error);
    }

};
main().then(() =>{ 
  this.listData = new MatTableDataSource(this.l);
  this.listData.sort = this.sort;
  this.listData.paginator = this.paginator;
  this.listData.filterPredicate = (data, filter) => {
    return this.displayedColumns.some(ele => {
      return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
    });
  };
});
     
}

  getData(){
    const bool=false;
    this.hotelServices.getHotelBase();
    this.postsSub = this.hotelServices.getPostUpdateListener()
      .subscribe((posts: Hotels[]) => {
        const main=async () => {

          try {
           
            console.log("hethi el l 9bal matet3aba el l");
            console.log(this.l);
          this.l = posts;
          console.log("hethi el l ba3d matet3aba el l");
          console.log(this.l);
  
      
          }
          catch (error) {
              console.error(error);
          }
      
      };
      main().then(() =>{ 
        this.listData = new MatTableDataSource(this.l);
  
        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
          });
        };
      }).catch(() => console.error('Failed!'));
      });
  }

 
}
