import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HotelsService } from '../../../../Services/hotels.service';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material';
import { Hotels } from '../../../../Models/hotels';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-hotel',
  templateUrl: './add-hotel.component.html',
  styleUrls: ['./add-hotel.component.scss']
})
export class AddHotelComponent implements OnInit {

  title = 'fileUpload';
  images;
  multipleImages = [];
  imagePreview: string='';
  form: FormGroup;
  imageverif=false;
  imagePreview2:string[]=[];
  ListeHotels;
  formvalid=false;
  constructor(private toastr: ToastrService,private _snackBar: MatSnackBar,public dialogRef: MatDialogRef<AddHotelComponent>,private hotelServices:HotelsService,private http: HttpClient){}

  ngOnInit(){
    console.log("zebinon "+this.hotelServices.getHexiste());
      this.hotelServices.getHotel().subscribe(        
        response =>{
  
             
              this.ListeHotels=  response;
  
              console.log(response);
  
        
           
          });
        
  
          
  

    this.form = new FormGroup({
      id: new FormControl(null, {
        validators: [Validators.required]
      }),
      nom: new FormControl(null, { validators: [Validators.required] }),
      content: new FormControl(null, { validators: [Validators.required] }),
      adultOnly: new FormControl(null, { validators: [Validators.required] }),
      ville: new FormControl(null, { validators: [Validators.required] }),
      categorie: new FormControl(null, { validators: [Validators.required] }),
      type: new FormControl(null, { validators: [Validators.required] }),
      lpdvente: new FormControl(null, { validators: [Validators.required] }),
      dpvente: new FormControl(null, { validators: [Validators.required] }),
      pcvente: new FormControl(null, { validators: [Validators.required] }),
      allinsoftvente: new FormControl(null, { validators: [Validators.required] }),
      allinvente: new FormControl(null, { validators: [Validators.required] }),
      ultraallinvente: new FormControl(null, { validators: [Validators.required] }),
      age_enf_gratuit: new FormControl(null, { validators: [Validators.required] }),

      image: new FormControl(null, {
        validators: [Validators.required]
      })
    });
  }
 
  selectImage(event) {

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
      reader.onload = () => {
        this.imagePreview = reader.result as string;
      };
      reader.readAsDataURL(file);
      if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

      this.imageverif=true;
      console.log(this.imageverif);
      
      }else{
        
        this.imageverif=false;
        console.log(this.imageverif);

      }


    }
    this._snackBar.open('Cannonball!!', '', {
      duration: 5000,
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });

  }
  


  selectMultipleImage(event){
    this.valid();
    this.imagePreview2=[];
    if (event.target.files.length > 0) {
      this.multipleImages = event.target.files;
      for (var i = 0; i < event.target.files.length; i++) {  
        let file=event.target.files[i];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
        reader.onload = () => {
          this.imagePreview2.push( reader.result as string);
        };
        reader.readAsDataURL(event.target.files[i]);
        if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

          this.imageverif=true;
          this.valid();

          console.log(this.imageverif);
    
          }else{
            this.valid();

            this.imageverif=false;
            console.log(this.imageverif);
    
          }
      }  
      console.log(this.imagePreview2);
     
    }

  }
  valid(){
    if(this.form.value.id!="" && this.form.value.nom!="" && this.form.value.adultOnly!="" && this.form.value.ville!="" &&
    this.form.value.categorie!="" && this.form.value.type!="" && this.form.value.lpdvente!=null && this.form.value.dpvente!=null &&
    this.form.value.pcvente!=null && this.form.value.allinsoftvente!=null && this.form.value.allinvente!=null && this.form.value.ultraallinvente!=null &&
    this.form.value.age_enf_gratuit!=null && this.form.value.name!="" && this.imageverif==true
    ){
    this.formvalid=true;
    console.log(this.form.valid+"***");
    }else{
      this.formvalid=false;
      console.log(this.formvalid+"***"+this.form.value.pcvente);

    }
  }

  onSubmit(){
console.log(this.form.valid);


    if(this.form.value.id!="" && this.form.value.nom!="" && this.form.value.adultOnly!="" && this.form.value.ville!="" &&
    this.form.value.categorie!="" && this.form.value.type!="" && this.form.value.lpdvente!="" && this.form.value.dpvente!="" &&
    this.form.value.pcvente!="" && this.form.value.allinsoftvente!="" && this.form.value.allinvente!="" && this.form.value.ultraallinvente!="" &&
    this.form.value.age_enf_gratuit!="" && this.form.value.name!="" && this.imageverif==true
    ){
    

    let hotel=new Hotels;
hotel.id=this.form.value.id;
hotel.nom=this.form.value.nom;
hotel.adultOnly=this.form.value.adultOnly;
hotel.ville=this.form.value.ville;
hotel.categorie=this.form.value.categorie;
hotel.type=this.form.value.type;
hotel.lpdvente=this.form.value.lpdvente;
hotel.dpvente=this.form.value.dpvente;
hotel.pcvente=this.form.value.pcvente;
hotel.allinsoftvente=this.form.value.allinsoftvente;
hotel.allinvente=this.form.value.allinvente;
hotel.ultraallinvente=this.form.value.ultraallinvente;
hotel.age_enf_gratuit=this.form.value.age_enf_gratuit;
hotel.image=this.imagePreview2;

      this.hotelServices.addPost(hotel,this.imagePreview2);
         console.log(hotel);
         for(let i of this.ListeHotels){
          if(i.id==this.form.value.id){
            this.toastr.success('', "Hotel existe in the external api it will be choosen by the lowest prix");
            
            return;
          }
        }
      
    }else{
if(this.imageverif==false){
  alert('image non valid');

}else{
  alert('remplir tous les correctement');

}
    }
   
  }

  onMultipleSubmit(){
    const formData = new FormData();
    for(let img of this.multipleImages){
      formData.append('files', img);
    }

    this.http.post<any>('http://localhost:3000/multipleFiles', formData).subscribe(
      (res) => console.log(res),
      (err) => console.log(err)
    );
  }
  close(){
        this.dialogRef.close();


  }

}
