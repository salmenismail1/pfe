import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelmodifComponent } from './hotelmodif.component';

describe('HotelmodifComponent', () => {
  let component: HotelmodifComponent;
  let fixture: ComponentFixture<HotelmodifComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelmodifComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelmodifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
