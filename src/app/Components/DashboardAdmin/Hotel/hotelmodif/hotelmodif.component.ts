import { Component, OnInit, Inject, ɵɵqueryRefresh } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HotelsService } from '../../../../Services/hotels.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-hotelmodif',
  templateUrl: './hotelmodif.component.html',
  styleUrls: ['./hotelmodif.component.scss']
})
export class HotelmodifComponent implements OnInit {

  title = 'fileUpload';
  images;
  multipleImages = [];
  imagePreview: string='';
  form: FormGroup;
  imageverif=false;
  imagePreview2:string[]=null;
datacomp:any;
formvalid=false;

  constructor(public dialogRef: MatDialogRef<HotelmodifComponent>,private hotelServices:HotelsService,private http: HttpClient,@Inject(MAT_DIALOG_DATA) public data:any){}

  ngOnInit(){
    this.imagePreview2=null;
    this.datacomp=this.data;
console.log(this.imagePreview2);
    this.form = new FormGroup({
      id: new FormControl(null, {
        validators: [Validators.required]
      }),
      nom: new FormControl(null, { validators: [Validators.required] }),
      content: new FormControl(null, { validators: [Validators.required] }),
      adultOnly: new FormControl(null, { validators: [Validators.required] }),
      ville: new FormControl(null, { validators: [Validators.required] }),
      categorie: new FormControl(null, { validators: [Validators.required] }),
      type: new FormControl(null, { validators: [Validators.required] }),
      lpdvente: new FormControl(null, { validators: [Validators.required] }),
      dpvente: new FormControl(null, { validators: [Validators.required] }),
      pcvente: new FormControl(null, { validators: [Validators.required] }),
      allinsoftvente: new FormControl(null, { validators: [Validators.required] }),
      allinvente: new FormControl(null, { validators: [Validators.required] }),
      ultraallinvente: new FormControl(null, { validators: [Validators.required] }),
      age_enf_gratuit: new FormControl(null, { validators: [Validators.required] }),

      image: new FormControl(null, {
        validators: [Validators.required]
      })
    });
  }
 
  selectImage(event) {

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
      reader.onload = () => {
        this.imagePreview = reader.result as string;
      };
      reader.readAsDataURL(file);
      if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

      this.imageverif=true;
      console.log(this.imageverif);
      
      }else{
        
        this.imageverif=false;
        console.log(this.imageverif);

      }


    }

  }
  


  selectMultipleImage(event){
    this.valid();
    this.imagePreview2=[];
    if (event.target.files.length > 0) {
      this.multipleImages = event.target.files;
      for (var i = 0; i < event.target.files.length; i++) {  
        let file=event.target.files[i];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
        reader.onload = () => {
          this.imagePreview2.push( reader.result as string);
        };
        reader.readAsDataURL(event.target.files[i]);
        if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

          this.imageverif=true;
          this.valid();

          console.log(this.imageverif);
    
          }else{
            this.valid();

            this.imageverif=false;
            console.log(this.imageverif);
    
          }
      }  
      console.log(this.imagePreview2);
     
    }

  }
  valid(){
    if(this.form.value.id!="" && this.form.value.nom!="" && this.form.value.adultOnly!="" && this.form.value.ville!="" &&
    this.form.value.categorie!="" && this.form.value.type!="" && this.form.value.lpdvente!=null && this.form.value.dpvente!=null &&
    this.form.value.pcvente!=null && this.form.value.allinsoftvente!=null && this.form.value.allinvente!=null && this.form.value.ultraallinvente!=null &&
    this.form.value.age_enf_gratuit!=null && (this.imageverif==true || this.imagePreview2==null)
    ){
    this.formvalid=true;
    console.log(this.form.value.nom+"***");
    }else{
      this.formvalid=false;
      console.log(this.formvalid+"***"+this.form.value.pcvente);

    }
  }
  onSubmit(idg:string){
    console.log(idg);
    console.log("//////////**////"+this.imageverif);

    if(this.form.value.id!="" && this.form.value.nom!="" && this.form.value.adultOnly!="" && this.form.value.ville!="" &&
    this.form.value.categorie!="" && this.form.value.type!="" && this.form.value.lpdvente!=null && this.form.value.dpvente!=null &&
    this.form.value.pcvente!=null && this.form.value.allinsoftvente!=null && this.form.value.allinvente!=null && this.form.value.ultraallinvente!=null &&
    this.form.value.age_enf_gratuit!=null && (this.imageverif==true || this.imagePreview2==null)
    ){
      if(this.imagePreview2==null || this.imagePreview2.length<1){
        this.hotelServices.update(this.form.value.id,this.form.value.nom,this.form.value.adultOnly,
          this.form.value.ville,this.form.value.categorie,this.form.value.type , this.form.value.lpdvente,
           this.form.value.dpvente, this.form.value.pcvente,this.form.value.allinsoftvente,this.form.value.allinvente,
           this.form.value.ultraallinvente,this.form.value.age_enf_gratuit,this.data.comp.image,idg);
      }else{
        this.hotelServices.update(this.form.value.id,this.form.value.nom,this.form.value.adultOnly,
          this.form.value.ville,this.form.value.categorie,this.form.value.type , this.form.value.lpdvente,
           this.form.value.dpvente, this.form.value.pcvente,this.form.value.allinsoftvente,this.form.value.allinvente,
           this.form.value.ultraallinvente,this.form.value.age_enf_gratuit,this.imagePreview2,idg);      
      }
     
   this.close();
      /*ng serve --live-reload false
      location.reload();
            window.stop();

      */
     


    }else{
if(this.imagePreview2.length<1){
  alert('image non valid');

}else{
  alert('remplir tous les correctement');

}
    }
   
  }

  
  close(){
        this.dialogRef.close();


  }

}

