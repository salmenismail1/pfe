import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelreservationComponent } from './hotelreservation.component';

describe('HotelreservationComponent', () => {
  let component: HotelreservationComponent;
  let fixture: ComponentFixture<HotelreservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelreservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelreservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
