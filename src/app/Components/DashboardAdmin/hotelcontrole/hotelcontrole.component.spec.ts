import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelcontroleComponent } from './hotelcontrole.component';

describe('HotelcontroleComponent', () => {
  let component: HotelcontroleComponent;
  let fixture: ComponentFixture<HotelcontroleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelcontroleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelcontroleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
