import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;

import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { Subscription } from 'rxjs'
import { User } from '../../../../../Models/user';
import { Router } from '@angular/router';
import { LoginService } from '../../../../../Services/login.service';
import * as jwt_decode from 'jwt-decode';
import { AppVarsGlobal } from '../../../../../app.vars.global';

@Component({
  selector: 'app-userliste',
  templateUrl: './userliste.component.html',
  styleUrls: ['./userliste.component.scss']
})
export class UserlisteComponent implements OnInit {

  
  constructor(private userServices: LoginService,public matDialog: MatDialog,private router:Router,private vars: AppVarsGlobal) { } 

  listeAdmin: MatTableDataSource<any>;
  listeclient : MatTableDataSource<any>;

  
  AdminColumns: string[] = ['nom', 'cin','tel','type','actions'];
  ClientColumns: string[] = ['nom', 'cin','tel','type','actions'];

  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  
  searchKey;searchKey2;

  LAdmin ;LClient ;

  userType
  ngOnInit() {

    
    this.userType=this.vars.type;
    console.log(this.userType)

    

    this.GetListeAdmin();
    this.GetListeClient();
  }

 
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
  onSearchClear2() {
    this.searchKey2 = "";
    this.applyFilter2();
  }
  

  applyFilter() {
    this.listeAdmin.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter2() {
    this.listeclient.filter = this.searchKey2.trim().toLowerCase();
  }
 


  DeleteUser(id:string,source:string){
    console.log(id);
    this.userServices.DeleteUser(id);
    if(source=="admin"){
      this.GetListeAdmin();

    }else{
      this.GetListeClient();

    }
  
}

  

  GetListeClient(){
    this.userServices.getClient().subscribe(        
      response =>{
        this.LClient=response;
        this.listeclient = new MatTableDataSource(this.LClient);
        this.listeclient.sort = this.sort;
        this.listeclient.paginator = this.paginator;
        this.listeclient.filterPredicate = (data, filter) => {
          return this.ClientColumns.some(ele => {
            return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
          });
        };
        console.log(response);
      },
      err =>{"error"}
    )
    
      
  }

  GetListeAdmin(){
    this.userServices.getAdmin().subscribe(        
      response =>{
        this.LAdmin=response;
        this.listeAdmin = new MatTableDataSource(this.LAdmin);
        this.listeAdmin.sort = this.sort;
        this.listeAdmin.paginator = this.paginator;
        this.listeAdmin.filterPredicate = (data, filter) => {
          return this.AdminColumns.some(ele => {
            return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
          });
  
      }
        console.log(response);
      },
      err =>{"error"}
    )
    
      
  }

  UpgradeToAdmin(user:User){

    this.userServices.UpgradeToAdmin(user);
    this.GetListeAdmin();
    this.GetListeClient();

  }
  DowngradeToClient(user:User){
    this.userServices.DowngradeToClient(user);
    this.GetListeAdmin();
    this.GetListeClient();
  }

 

}

