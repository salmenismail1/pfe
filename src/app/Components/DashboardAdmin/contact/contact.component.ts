import { ContactService } from './../../../Services/contact.service';
import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;
import { ContactResponseComponent as ModalComponent } from '../contact-response/contact-response.component';

import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Contact } from '../../../Models/contact';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

 
  constructor(private contactServices: ContactService,public matDialog: MatDialog,private router:Router) { } 

  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['idClient', 'nom','sujet','actions'];
  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  searchKey: string;
  l: Contact[];
  private postsSub: Subscription;
  selectFilter="no response";
  ngOnInit() {
    
    this.getData();
  }

 
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }


  
  openModifModal(item:any,event){
    console.log(item);
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modifmodal-component";
    const g=this.matDialog.open(ModalComponent, {
      height: '500px',
      width: '500px',
      data: { comp: item,
      response:this.selectFilter
      },
    });
    g.afterClosed().subscribe(() => {

      // Do stuff after the dialog has closed
     
        console.log("aaa");
        this.getData();

  
      

  });
  
  console.log(item);

  }

delete(id:string){


      this.contactServices.deletePost(id);
      this.postsSub = this.contactServices.getPostUpdateListener().subscribe((posts: Contact[]) => {
        this.l = posts;
      });
      setTimeout(() => 
      {
        console.log(this.l);
        this.listData = new MatTableDataSource(this.l);
        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
          });
        };

      },
      2000);
}

  getData(){
    const bool=false;
    
    this.contactServices.getContactBase(this.selectFilter);
    this.postsSub = this.contactServices.getPostUpdateListener()
      .subscribe((posts: Contact[]) => {
        const main=async () => {

          try {
           
            console.log("hethi el l 9bal matet3aba el l");
            console.log(this.l);
          this.l = posts;
          console.log("hethi el l ba3d matet3aba el l");
          console.log(this.l);
  
      
          }
          catch (error) {
              console.error(error);
          }
      
      };
      main().then(() =>{ 
        this.listData = new MatTableDataSource(this.l);

        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
          });
        };

      }).catch(() => console.error('Failed!'));
      });
  }


}
