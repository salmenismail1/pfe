import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { SoireesService } from '../../../../Services/soirees.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-soiree',
  templateUrl: './add-soiree.component.html',
  styleUrls: ['./add-soiree.component.scss']
})
export class AddSoireeComponent implements OnInit {

  title = 'fileUpload';
  images;
  multipleImages = [];
  imagePreview: string='';
  form: FormGroup;
  imageverif=false;
  imagePreview2:string[]=[];
  ListeSoiree;
  formvalid=false;

  constructor(private toastr: ToastrService,public dialogRef: MatDialogRef<AddSoireeComponent>,private soireeServices:SoireesService,private http: HttpClient){}
  
      
  ngOnInit(){
    this.soireeServices.getSoiree().subscribe(        
      response =>{

           
            this.ListeSoiree=  response;

            console.log(response);

      
         
        });

    this.form = new FormGroup({
      id: new FormControl(null, {
        validators: [Validators.required]
      }),
      titre: new FormControl(null, { validators: [Validators.required] }),
      date: new FormControl(null, { validators: [Validators.required] }),
      venteadultesingle: new FormControl(null, { validators: [Validators.required] }),
      venteadultedble: new FormControl(null, { validators: [Validators.required] }),
      vente3rdad: new FormControl(null, { validators: [Validators.required] }),
      venteenfant2ad: new FormControl(null, { validators: [Validators.required] }),
      tarifbebe: new FormControl(null, { validators: [Validators.required] }),
      venteenfant1ad: new FormControl(null, { validators: [Validators.required] }),
      programme: new FormControl(null, { validators: [Validators.required] }),

      image: new FormControl(null, {
        validators: [Validators.required]
      })
    });
  }
 
  selectImage(event) {

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
      reader.onload = () => {
        this.imagePreview = reader.result as string;
      };
      reader.readAsDataURL(file);
      if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

      this.imageverif=true;
      console.log(this.imageverif);
      
      }else{
        
        this.imageverif=false;
        console.log(this.imageverif);

      }


    }

  }
  


  selectMultipleImage(event){
    this.valid();
    this.imagePreview2=[];
    if (event.target.files.length > 0) {
      this.multipleImages = event.target.files;
      for (var i = 0; i < event.target.files.length; i++) {  
        let file=event.target.files[i];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
        reader.onload = () => {
          this.imagePreview2.push( reader.result as string);
        };
        reader.readAsDataURL(event.target.files[i]);
        if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

          this.imageverif=true;
          this.valid();

          console.log(this.imageverif);
    
          }else{
            this.valid();

            this.imageverif=false;
            console.log(this.imageverif);
    
          }
      }  
      console.log(this.imagePreview2);
     
    }

  }
  valid(){
    if(this.form.value.id!="" && this.form.value.titre!="" && this.form.value.date!="" && this.form.value.venteadultesingle!=null && this.form.value.programme!="" &&
    this.form.value.venteadultedble!=null && this.form.value.vente3rdad!=null && this.form.value.venteenfant2ad!=null && this.imageverif==true
    && this.form.value.tarifbebe!=null && this.form.value.venteenfant1ad!=null
    ){
    this.formvalid=true;
    console.log(this.form.valid+"***");
    }else{
      this.formvalid=false;
      console.log(this.formvalid+"***"+this.form.value.pcvente);

    }
  }
  onSubmit(){
    if(this.form.value.id!="" && this.form.value.titre!="" && this.form.value.date!="" && this.form.value.venteadultesingle!="" && this.form.value.programme!="" &&
    this.form.value.venteadultedble!="" && this.form.value.vente3rdad!="" && this.form.value.venteenfant2ad!="" && this.form.value.name!="" && this.imageverif==true
    && this.form.value.tarifbebe!="" && this.form.value.venteenfant1ad!=""
    ){

      this.soireeServices.addPost(this.form.value.id,this.form.value.titre,
        this.form.value.venteadultesingle,this.form.value.venteadultedble,this.form.value.date,this.imagePreview2,this.form.value.vente3rdad , this.form.value.venteenfant2ad,this.form.value.tarifbebe,this.form.value.venteenfant1ad,this.form.value.programme);
         const file=this.imagePreview;
         for(let i of this.ListeSoiree){
          console.log(i.id);
          if(i.id==this.form.value.id){
            this.toastr.success('', "Soiree existe in the external api it will be choosen by the lowest prix");
            return;
          }
        }
      
         this.close();
       
    }else{
if(this.imageverif==false){
  alert('image non valid');

}else{
  alert('remplir tous les correctement');

}
    }
   
  }

  
  close(){
        this.dialogRef.close();


  }
}
