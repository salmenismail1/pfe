import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;
import { AddSoireeComponent as ModalComponent } from '../add-soiree/add-soiree.component';
import { SoireemodifComponent as ModalComponent2 } from '../soireemodif/soireemodif.component';

import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Soiree } from '../../../../Models/soiree';
import { SoireesService } from '../../../../Services/soirees.service';

@Component({
  selector: 'app-soireelist',
  templateUrl: './soireelist.component.html',
  styleUrls: ['./soireelist.component.scss']
})
export class SoireelistComponent implements OnInit {

  constructor(private soireeServices: SoireesService,public matDialog: MatDialog,private router:Router) { } 

  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['id', 'nom','actions'];
  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  searchKey: string;
  l: Soiree[];
  private postsSub: Subscription;
  ngOnInit() {
    
    this.getData();
     
  }

 
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  openModal() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modal-component-voyage";
    const g=this.matDialog.open(ModalComponent, {
      height: '800px',
      width: '500px',
    });
    g.afterClosed().subscribe(() => {
      // Do stuff after the dialog has closed

     
        this.getData();
      
     

    });

  }
  openModifModal(item:any){
    console.log(item);
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = false;
    dialogConfig.id = "modifmodal-component-voyage";
    const g=this.matDialog.open(ModalComponent2, {
      height: '800px',
      width: '500px',
      data: { comp: item },
    });
    
    g.afterClosed().subscribe(() => {

    
        this.getData();
      
        

  });
  
  console.log(item);

  }

delete(id:string){

  const main=async () => {

    try {
     
      this.soireeServices.deletePost(id);
      this.postsSub = this.soireeServices.getPostUpdateListener().subscribe((posts: Soiree[]) => {
        this.l = posts;
      });
    }
    catch (error) {
        console.error(error);
    }

};
main().then(() =>{ 
  this.listData = new MatTableDataSource(this.l);
  this.listData.sort = this.sort;
  this.listData.paginator = this.paginator;
  this.listData.filterPredicate = (data, filter) => {
    return this.displayedColumns.some(ele => {
      return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
    });
  };
});
     
}
  getData(){
    this.soireeServices.getSoireesBase();
    this.postsSub = this.soireeServices.getPostUpdateListener()
      .subscribe((posts: Soiree[]) => {
        this.l = posts;
        this.listData = new MatTableDataSource(this.l);

        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
          });
        };
      });
    
  }

}
