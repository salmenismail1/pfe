import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoireelistComponent } from './soireelist.component';

describe('SoireelistComponent', () => {
  let component: SoireelistComponent;
  let fixture: ComponentFixture<SoireelistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoireelistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoireelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
