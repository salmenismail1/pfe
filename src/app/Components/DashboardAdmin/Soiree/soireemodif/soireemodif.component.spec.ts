import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoireemodifComponent } from './soireemodif.component';

describe('SoireemodifComponent', () => {
  let component: SoireemodifComponent;
  let fixture: ComponentFixture<SoireemodifComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoireemodifComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoireemodifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
