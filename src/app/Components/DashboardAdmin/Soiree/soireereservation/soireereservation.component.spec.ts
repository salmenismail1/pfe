import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoireereservationComponent } from './soireereservation.component';

describe('SoireereservationComponent', () => {
  let component: SoireereservationComponent;
  let fixture: ComponentFixture<SoireereservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoireereservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoireereservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
