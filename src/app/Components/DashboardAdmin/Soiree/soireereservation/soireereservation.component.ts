import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;
import { AddSoireeComponent as ModalComponent } from '../add-soiree/add-soiree.component';
import { SoireemodifComponent as ModalComponent2 } from '../soireemodif/soireemodif.component';

import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Soiree } from '../../../../Models/soiree';
import { SoireesService } from '../../../../Services/soirees.service';
import { LoginService } from '../../../../Services/login.service';
@Component({
  selector: 'app-soireereservation',
  templateUrl: './soireereservation.component.html',
  styleUrls: ['./soireereservation.component.scss']
})
export class SoireereservationComponent implements OnInit {

  constructor(private SoireesServices: SoireesService,private userServices: LoginService,public matDialog: MatDialog,private router:Router) { } 

  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;

  listData: MatTableDataSource<any>;
  listeReservationAccepter : MatTableDataSource<any>;
  listeReservationEnAttente : MatTableDataSource<any>;

  ReservationSoireeColumns: string[] = ['Titre', 'Email','Prix','actions'];
  ReservationSoireeColumns2: string[] = ['Titre', 'Email','Prix','actions'];

 

  searchKey2;searchKey3;
  LReservationAccepter ;LReservationEnAttente ;

  

  ngOnInit() {
    
    //this.getData();
    this.GetListeReservationAccepter();
    this.GetListeReservationEnAttente();
     
  }

  onSearchClear2() {
    this.searchKey2 = "";
    this.applyFilter2();
  }
  onSearchClear3() {
    this.searchKey3 = "";
    this.applyFilter3();
  }


  applyFilter2() {
    this.listeReservationAccepter.filter = this.searchKey2.trim().toLowerCase();
  }
  applyFilter3() {
    this.listeReservationEnAttente.filter = this.searchKey3.trim().toLowerCase();
  }

deleteReservationSoiree(id:string){
  console.log(id);
  console.log(id);
  const main=async () => {

    try {
     
      this.SoireesServices.DeleteReservationSoiree(id);

    }
    catch (error) {
        console.error(error);
    }

};
main().then(() =>{ 
  this.GetListeReservationAccepter();
  this.GetListeReservationEnAttente();
});
}


 

  GetListeReservationAccepter(){
  
       
        this.SoireesServices.getReservationAccepter().subscribe(        
          async response =>{
            this.LReservationAccepter=await response;
    
            console.log(this.LReservationAccepter);
            this.listeReservationAccepter = new MatTableDataSource(this.LReservationAccepter);
            console.log(this.LReservationAccepter)
                this.listeReservationAccepter.sort = this.sort;
                this.listeReservationAccepter.paginator = this.paginator;
                this.listeReservationAccepter.filterPredicate = (data, filter) => {
                  console.log("-------"+filter)
                  return this.ReservationSoireeColumns.some(ele => {
                    return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
                  });
                };
          },
          err =>{"error"}
        );
    
    
  }

  GetListeReservationEnAttente(){

         
          this.SoireesServices.getReservationEnAttent().subscribe(        
            async response =>{
              this.LReservationEnAttente= await response;
              this.listeReservationEnAttente = new MatTableDataSource(this.LReservationEnAttente);
              this.listeReservationEnAttente.sort = this.sort;
              this.listeReservationEnAttente.paginator = this.paginator;
              this.listeReservationEnAttente.filterPredicate = (data, filter) => {
                return this.ReservationSoireeColumns2.some(ele => {
                  return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
                });
              };
              console.log(this.LReservationEnAttente);
            },
            err =>{"error"}
          );    
    
  }

  ReservationAccepter(id : string){
    
    const main=async () => {

      try {
       
        this.SoireesServices.ReservationAccepter(id);
  
      }
      catch (error) {
          console.error(error);
      }
  
  };
  main().then(() =>{ 
    this.GetListeReservationAccepter();
    this.GetListeReservationEnAttente();
  });
  }

  UpgradePayement(id:any){
    const main=async () => {

      try {
       
        this.SoireesServices.UpgradePayement(id);
        this.GetListeReservationAccepter();

      }
      catch (error) {
          console.error(error);
      }
  
  };
  main().then(() =>{ 
    this.GetListeReservationAccepter();

  });
  }

}
