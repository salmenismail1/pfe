import { Component, OnInit, Inject } from '@angular/core';
import { Contact } from '../../../Models/contact';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ContactService } from '../../../Services/contact.service';
import * as jwt_decode from 'jwt-decode';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-contact-response',
  templateUrl: './contact-response.component.html',
  styleUrls: ['./contact-response.component.scss']
})
export class ContactResponseComponent implements OnInit {

  contact=new Contact;
  reactiveform :FormGroup;
  form:FormGroup;
  constructor(public dialogRef: MatDialogRef<ContactResponseComponent>,private formbuilder:FormBuilder,private contactserv:ContactService,@Inject(MAT_DIALOG_DATA) public data:any) { }
  ngOnInit() {
    console.log(  this.data.comp);

    this.contact=this.data.comp;
    console.log(  this.contact);
    this.reactiveform=this.formbuilder.group({
      'response' : new FormControl('', [Validators.required]),
    });
    

  }


  envoyer(){
    if( this.reactiveform.valid ){

      console.log(this.contact);
      this.contactserv.update(this.contact,this.data.response);
      this.close();
    }else{
      console.log("error");
    }
    
  
  }
  close(){
    this.dialogRef.close();


}

}
