import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoyagecontroleComponent } from './voyagecontrole.component';

describe('VoyagecontroleComponent', () => {
  let component: VoyagecontroleComponent;
  let fixture: ComponentFixture<VoyagecontroleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoyagecontroleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoyagecontroleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
