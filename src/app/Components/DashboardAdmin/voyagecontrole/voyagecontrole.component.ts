import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;


import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { HotelsService } from '../../../Services/hotels.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Voyages } from '../../../Models/voyages';
import { VoyagesService } from '../../../Services/voyages.service';
@Component({
  selector: 'app-voyagecontrole',
  templateUrl: './voyagecontrole.component.html',
  styleUrls: ['./voyagecontrole.component.scss']
})
export class VoyagecontroleComponent implements OnInit {
  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;

  voyagecheckedlist:Array<any>=[];
  voyagelistData: MatTableDataSource<any>;
  voyagedisplayedColumns: string[] = ['id','titre','actions'];
  voyagesearchKey: string;


  
  vl: Voyages[];
  ListeVoyage;
  private voyagepostsSub: Subscription;

  constructor(private voyageServices: VoyagesService,public matDialog: MatDialog,private router:Router) { } 

  ngOnInit() {
    this.getVoyageData();

  }
  onVoyageSearchClear() {
    this. voyagesearchKey = "";
    this.applyVoyageFilter();
  }

  applyVoyageFilter() {
    this.voyagelistData.filter = this.voyagesearchKey.trim().toLowerCase();
  }
  getVoyageData(){
    let lst: Voyages[]=[];
    let vl=[];
    let ListeVoyage;
    let nmb=0;
    this.voyageServices.getVoyageBase();
    this.voyagepostsSub = this.voyageServices.getPostUpdateListener()
      .subscribe((posts: Voyages[]) => {

        const main=async () => {

          try {
            vl = await posts;


      
          }
          catch (error) {
              console.error(error);
          }
      
      };
      main().then(() =>{ 
        nmb+=vl.length;
        console.log(nmb);
        console.log(vl);
        this.voyageServices.getVoyage().subscribe(        
          response =>{
            const main=async () => {

              try {
    
                ListeVoyage= await response;

          
              }
              catch (error) {
                  console.error(error);
              }
          
          };
          main().then(() =>{ 

 nmb+=ListeVoyage.length;
            console.log(nmb);
            let image:any;
            console.log(ListeVoyage.length);
            console.log("///////////// "+ListeVoyage);
           
            for(let i=0;i<ListeVoyage.length;i++){
              if(ListeVoyage[i].imageVoyages[0]!=undefined){
                image="https://www.freedomtravel.tn/assets/images/4/"+ListeVoyage[i].id+"/"+ListeVoyage[i].imageVoyages[0].nom;

              }
            
            const post: Voyages = {  id :  ListeVoyage[i].id,nom : ListeVoyage[i].nom,pays:ListeVoyage[i].pays.libelle,
              titre:ListeVoyage[i].titre+"",venteadultesingle:ListeVoyage[i].venteadultesingle,venteadultedble:ListeVoyage[i].venteadultedble,
              vente3rdad:ListeVoyage[i].vente3rdad,venteenfant2ad:ListeVoyage[i].venteenfant2ad,
              venteenfant1ad:ListeVoyage[i].venteenfant1ad,tarifbebe:ListeVoyage[i].tarifbebe,
              venteenfant12ans:ListeVoyage[i].venteenfant12ans,venteenfant6ans:ListeVoyage[i].venteenfant6ans,
              departvoyages:ListeVoyage[i].departvoyages,programme:ListeVoyage[i].programme,
              image:image
            };
              vl.push(post);
            }
           
          
  
            
              console.log(nmb);
              console.log("*****"+vl[0].nom);
  
              for(let i=0;i<nmb;i++){
                if(!lst.includes(vl[i]))
                lst.push(vl[i]);
              }
              console.log(lst);
              
              this.voyagelistData = new MatTableDataSource(lst);
              this.voyagelistData.sort = this.sort;
              this.voyagelistData.paginator = this.paginator;
              this.voyagelistData.filterPredicate = (data, filter) => {
                return this.voyagedisplayedColumns.some(ele => {
                  return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
                });
              };
          }).catch(() => console.error('Failed!'));

           
           
          },
          err =>{"error"}
        );
      
      }).catch(() => console.error('Failed!'));
  
      

      });

     
      
  }


  
  voyagecheck(item:any){
    console.log(item);
    if(this.voyagecheckedlist.length<6){
  if(!this.voyagecheckedlist.includes(item)){
  this.voyagecheckedlist.push(item);
  
  }else{
    this.voyagecheckedlist.splice(this.voyagecheckedlist.indexOf(item),1);
  }
  console.log(this.voyagecheckedlist);
}
  }
  
  voyageconfirmer(){
    this.voyageServices.deleteall();
  for(let item of this.voyagecheckedlist ){
    this.voyageServices.voyagehomelistpost(item);
  }
  this.voyagecheckedlist=[];
  this.getVoyageData();
  }
  
}
