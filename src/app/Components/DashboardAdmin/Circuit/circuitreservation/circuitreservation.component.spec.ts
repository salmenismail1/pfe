import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CircuitreservationComponent } from './circuitreservation.component';

describe('CircuitreservationComponent', () => {
  let component: CircuitreservationComponent;
  let fixture: ComponentFixture<CircuitreservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CircuitreservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CircuitreservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
