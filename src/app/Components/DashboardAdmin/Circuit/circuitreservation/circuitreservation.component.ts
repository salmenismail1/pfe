import { Soiree } from './../../../../Models/soiree';
import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;
import { AddCircuitComponent as ModalComponent } from '../add-circuit/add-circuit.component';
import { CircuitmodifComponent as ModalComponent2 } from '../circuitmodif/circuitmodif.component';

import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Circuit } from '../../../../Models/circuit';
import { CircuitService } from '../../../../Services/circuit.service';
import { LoginService } from '../../../../Services/login.service';

@Component({
  selector: 'app-circuitreservation',
  templateUrl: './circuitreservation.component.html',
  styleUrls: ['./circuitreservation.component.scss']
})
export class CircuitreservationComponent implements OnInit {

  constructor(private circuitServices: CircuitService,private userServices: LoginService,public matDialog: MatDialog,private router:Router) { } 

 
  listData: MatTableDataSource<any>;
  listeReservationAccepter : MatTableDataSource<any>;
  listeReservationEnAttente : MatTableDataSource<any>;

  displayedColumns: string[] = ['id', 'nom','actions'];
  ReservationHotelColumns: string[] = ['Titre', 'Email','Prix','actions'];
  ReservationHotelColumns2: string[] = ['Titre', 'Email','Prix','actions'];

  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  
  searchKey;searchKey2;searchKey3;

  l: Soiree[];
  private postsSub: Subscription;

  LReservationAccepter ;LReservationEnAttente ;
  
  ngOnInit() {
 
    //this.getData();
    this.GetListeReservationAccepter();
    this.GetListeReservationEnAttente();
  }

 
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
  onSearchClear2() {
    this.searchKey2 = "";
    this.applyFilter2();
  }
  onSearchClear3() {
    this.searchKey3 = "";
    this.applyFilter3();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter2() {
    this.listeReservationAccepter.filter = this.searchKey2.trim().toLowerCase();
  }
  applyFilter3() {
    this.listeReservationEnAttente.filter = this.searchKey3.trim().toLowerCase();
  }


deleteReservationHotel(id:string){
  console.log(id);
  const main=async () => {

    try {
     
      this.circuitServices.DeleteReservationHotel(id);

    }
    catch (error) {
        console.error(error);
    }

};
main().then(() =>{ 
  this.GetListeReservationAccepter();
  this.GetListeReservationEnAttente();
});
 
}
  

GetListeReservationAccepter(){
  this.circuitServices.getReservationAccepter().subscribe(        
    async response =>{
      this.LReservationAccepter= await response;
      console.log(this.LReservationAccepter);
      this.listeReservationAccepter = new MatTableDataSource(this.LReservationAccepter);
      this.listeReservationAccepter.sort = this.sort;
      this.listeReservationAccepter.paginator = this.paginator;
      this.listeReservationAccepter.filterPredicate = (data, filter) => {
        console.log("-------"+filter)
        return this.ReservationHotelColumns.some(ele => {
          return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
        });
      };
      console.log(response);
    },
    err =>{"error"}
  );
  
   
}

GetListeReservationEnAttente(){
  this.circuitServices.getReservationEnAttent().subscribe(        
    async response =>{
      this.LReservationEnAttente= await response;
      console.log(this.LReservationEnAttente);
      this.listeReservationEnAttente = new MatTableDataSource(this.LReservationEnAttente);
      this.listeReservationEnAttente.sort = this.sort;
      this.listeReservationEnAttente.paginator = this.paginator;
      this.listeReservationEnAttente.filterPredicate = (data, filter) => {
        return this.ReservationHotelColumns2.some(ele => {
          return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
        });
      };
      console.log(response);
    },
    err =>{"error"}
  );
  
    
}


  ReservationAccepter(id : string){
  
    const main=async () => {

      try {
       
        this.circuitServices.ReservationAccepter(id);
  
      }
      catch (error) {
          console.error(error);
      }
  
  };
  main().then(() =>{ 
    this.GetListeReservationAccepter();
    this.GetListeReservationEnAttente();
  });
  }
  UpgradePayement(id:any){
    const main=async () => {

      try {
       
        this.circuitServices.UpgradePayement(id);
        this.GetListeReservationAccepter();

      }
      catch (error) {
          console.error(error);
      }
  
  };
  main().then(() =>{ 
    this.GetListeReservationAccepter();

  });
  }

}
