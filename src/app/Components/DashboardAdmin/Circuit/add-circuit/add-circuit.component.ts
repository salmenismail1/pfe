import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { CircuitService } from '../../../../Services/circuit.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-circuit',
  templateUrl: './add-circuit.component.html',
  styleUrls: ['./add-circuit.component.scss']
})
export class AddCircuitComponent implements OnInit {

  title = 'fileUpload';
  images;
  multipleImages = [];
  imagePreview: string='';
  imagePreview2:string[]=[];

  form: FormGroup;
  imageverif=false;
  ListeCircuit;
  formvalid=false;

  constructor(private toastr: ToastrService,public dialogRef: MatDialogRef<AddCircuitComponent>,private circuitServices:CircuitService,private http: HttpClient){}
  
   
  ngOnInit(){
    this.circuitServices.getCircuit().subscribe(        
      response =>{

           
            this.ListeCircuit=  response;

            console.log(response);

      
         
        });

    this.form = new FormGroup({
      id: new FormControl(null, {
        validators: [Validators.required]
      }),
      nom: new FormControl(null, { validators: [Validators.required] }),
      description: new FormControl(null, { validators: [Validators.required] }),
      libelle: new FormControl(null, { validators: [Validators.required] }),
      programme: new FormControl(null, { validators: [Validators.required] }),
      venteadultesingle: new FormControl(null, { validators: [Validators.required] }),
      venteadultedble: new FormControl(null, { validators: [Validators.required] }),
      vente3rdad: new FormControl(null, { validators: [Validators.required] }),
      venteenfant2ad: new FormControl(null, { validators: [Validators.required] }),
      tarifbebe: new FormControl(null, { validators: [Validators.required] }),
      venteenfant1ad: new FormControl(null, { validators: [Validators.required] }),
      datedepart: new FormControl(null, { validators: [Validators.required] }),
      dateretour: new FormControl(null, { validators: [Validators.required] }),
      image: new FormControl(null, {
        validators: [Validators.required]
      })
    });
  }
 
  selectImage(event) {

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
      reader.onload = () => {
        this.imagePreview = reader.result as string;
      };
      reader.readAsDataURL(file);
      if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

      this.imageverif=true;
      console.log(this.imageverif);

      }else{
        
        this.imageverif=false;
        console.log(this.imageverif);

      }


    }

  }
  


  selectMultipleImage(event){
    this.valid();
    this.imagePreview2=[];
    if (event.target.files.length > 0) {
      this.multipleImages = event.target.files;
      for (var i = 0; i < event.target.files.length; i++) {  
        let file=event.target.files[i];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
        reader.onload = () => {
          this.imagePreview2.push( reader.result as string);
        };
        reader.readAsDataURL(event.target.files[i]);
        if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

          this.imageverif=true;
          this.valid();

          console.log(this.imageverif);
    
          }else{
            this.valid();

            this.imageverif=false;
            console.log(this.imageverif);
    
          }
      }  
      console.log(this.imagePreview2);
     
    }

  }
  valid(){
    if(this.form.value.id!="" && this.form.value.nom!="" && this.form.value.description!="" && this.form.value.libelle!="" &&
    this.form.value.programme!="" && this.form.value.venteadultesingle!=null && this.form.value.venteadultedble!=null && this.form.value.vente3rdad!=null &&
    this.form.value.venteenfant2ad!=null && this.form.value.tarifbebe!=null && this.form.value.venteenfant1ad!=null && this.form.value.datedepart!=null &&
    this.form.value.dateretour!=null && this.imageverif==true
    ){
    this.formvalid=true;
    console.log(this.form.valid+"***");
    }else{
      this.formvalid=false;
      console.log(this.formvalid+"***"+this.form.value.pcvente);

    }
  }
  onSubmit(){
    if(this.form.value.id!="" && this.form.value.nom!="" && this.form.value.description!="" && this.form.value.libelle!="" &&
    this.form.value.programme!="" && this.form.value.venteadultesingle!=null && this.form.value.venteadultedble!=null && this.form.value.vente3rdad!=null &&
    this.form.value.venteenfant2ad!=null && this.form.value.tarifbebe!=null && this.form.value.venteenfant1ad!=null && this.form.value.datedepart!=null &&
    this.form.value.dateretour!=null && this.imageverif==true
    ){

      this.circuitServices.addPost(this.form.value,this.imagePreview2);
      for(let i of this.ListeCircuit){
        console.log(i.id);
        if(i.id==this.form.value.id){
          this.toastr.success('', "Circuit existe in the external api it will be choosen by the lowest prix");
          return;
        }
      }
console.log(this.imagePreview2);

  this.close();


    }else{
if(this.imageverif==false){
  alert('image non valid');

}else{
  alert('remplir tous les correctement');

}
    }
   
  }

  onMultipleSubmit(){
    const formData = new FormData();
    for(let img of this.multipleImages){
      formData.append('files', img);
    }

    this.http.post<any>('http://localhost:3000/multipleFiles', formData).subscribe(
      (res) => console.log(res),
      (err) => console.log(err)
    );
  }
  close(){
        this.dialogRef.close();


  }

}
