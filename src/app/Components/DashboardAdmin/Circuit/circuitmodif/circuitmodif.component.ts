import { Component, OnInit, Inject, ɵɵqueryRefresh } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CircuitService } from '../../../../Services/circuit.service';
import { toBase64String } from '@angular/compiler/src/output/source_map';
import { Subscription } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-circuitmodif',
  templateUrl: './circuitmodif.component.html',
  styleUrls: ['./circuitmodif.component.scss']
})
export class CircuitmodifComponent implements OnInit {
  title = 'fileUpload';
  images;
  multipleImages = [];
  imagePreview: string='';
  form: FormGroup;
  imageverif=false;
  lstimage: any[] = [];
img;
imageurl;
imagePreview2:string[]=null;
formvalid=false;

  private postsSub: Subscription;

  constructor(public dialogRef: MatDialogRef<CircuitmodifComponent>,private sanitization:DomSanitizer,private circuitServices:CircuitService,private http: HttpClient,@Inject(MAT_DIALOG_DATA) public data:any){}

  ngOnInit(){
    this.imagePreview2=null;


    this.form = new FormGroup({
      id: new FormControl(null, {
        validators: [Validators.required]
      }),
      nom: new FormControl(null, { validators: [Validators.required] }),
     
      description: new FormControl(null, { validators: [Validators.required] }),
      libelle: new FormControl(null, { validators: [Validators.required] }),
      programme: new FormControl(null, { validators: [Validators.required] }),
      venteadultesingle: new FormControl(null, { validators: [Validators.required] }),
      venteadultedble: new FormControl(null, { validators: [Validators.required] }),
      vente3rdad: new FormControl(null, { validators: [Validators.required] }),
      venteenfant2ad: new FormControl(null, { validators: [Validators.required] }),
      tarifbebe: new FormControl(null, { validators: [Validators.required] }),
      venteenfant1ad: new FormControl(null, { validators: [Validators.required] }),
      datedepart: new FormControl(null, { validators: [Validators.required] }),
      dateretour: new FormControl(null, { validators: [Validators.required] }),
      image: new FormControl(null, {
        validators: [Validators.required]
      })
    });
  }
   
  selectMultipleImage(event){
    this.valid();
    this.imagePreview2=[];
    if (event.target.files.length > 0) {
      this.multipleImages = event.target.files;
      for (var i = 0; i < event.target.files.length; i++) {  
        let file=event.target.files[i];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
        reader.onload = () => {
          this.imagePreview2.push( reader.result as string);
        };
        reader.readAsDataURL(event.target.files[i]);
        if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

          this.imageverif=true;
          this.valid();

          console.log(this.imageverif);
    
          }else{
            this.valid();

            this.imageverif=false;
            console.log(this.imageverif);
    
          }
      }  
      console.log(this.imagePreview2);
     
    }

  }
  valid(){
    if(this.form.value.id!="" && this.form.value.nom!="" && this.form.value.description!="" && this.form.value.libelle!="" &&
    this.form.value.programme!="" && this.form.value.venteadultesingle!=null && this.form.value.venteadultedble!=null && this.form.value.vente3rdad!=null &&
    this.form.value.venteenfant2ad!=null && this.form.value.tarifbebe!=null && this.form.value.venteenfant1ad!=null && this.form.value.datedepart!=null &&
    this.form.value.dateretour!=null && (this.imageverif==true || this.imagePreview2==null)
    ){
    this.formvalid=true;
    console.log(this.form.value.nom+"***");
    }else{
      this.formvalid=false;
      console.log(this.formvalid+"***"+this.form.value.pcvente);

    }
  }
  onSubmit(idg:string){
    console.log(idg);
    if(this.form.value.id!="" && this.form.value.nom!="" && this.form.value.description!="" && this.form.value.libelle!="" &&
    this.form.value.programme!="" && this.form.value.venteadultesingle!=null && this.form.value.venteadultedble!=null && this.form.value.vente3rdad!=null &&
    this.form.value.venteenfant2ad!=null && this.form.value.tarifbebe!=null && this.form.value.venteenfant1ad!=null && this.form.value.datedepart!=null &&
    this.form.value.dateretour!=null && (this.imageverif==true || this.imagePreview2==null)
    ){
      if(this.imagePreview2==null || this.imagePreview2==[]){
        this.circuitServices.update(this.form.value,this.data.comp.image,idg);

}else{
  this.circuitServices.update(this.form.value,this.imagePreview2,idg);

}
      
      /*ng serve --live-reload false
      location.reload();
            window.stop();

      */
     this.close();


    }else{
if(this.imageverif==false){
  alert('image non valid');

}else{
  alert('remplir tous les correctement');

}
    }
   
  }

  onMultipleSubmit(){
    const formData = new FormData();
    for(let img of this.multipleImages){
      formData.append('files', img);
    }

    this.http.post<any>('http://localhost:3000/multipleFiles', formData).subscribe(
      (res) => console.log(res),
      (err) => console.log(err)
    );
  }
  close(){
        this.dialogRef.close();


  }

}
