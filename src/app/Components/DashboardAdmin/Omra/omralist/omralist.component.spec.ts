import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OmralistComponent } from './omralist.component';

describe('OmralistComponent', () => {
  let component: OmralistComponent;
  let fixture: ComponentFixture<OmralistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OmralistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OmralistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
