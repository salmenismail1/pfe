import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OmramodifComponent } from './omramodif.component';

describe('OmramodifComponent', () => {
  let component: OmramodifComponent;
  let fixture: ComponentFixture<OmramodifComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OmramodifComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OmramodifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
