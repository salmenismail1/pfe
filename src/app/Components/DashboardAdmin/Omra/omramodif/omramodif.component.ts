import { OmraService } from './../../../../Services/omra.service';
import { Component, OnInit, Inject, ɵɵqueryRefresh } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CircuitService } from '../../../../Services/circuit.service';
import { toBase64String } from '@angular/compiler/src/output/source_map';
import { Subscription } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-omramodif',
  templateUrl: './omramodif.component.html',
  styleUrls: ['./omramodif.component.scss']
})
export class OmramodifComponent implements OnInit {
  title = 'fileUpload';
  images;
  multipleImages = [];
  imagePreview: string='';
  form: FormGroup;
  imageverif=false;
  lstimage: any[] = [];
img;
imageurl;
imagePreview2:string[]=null;
formvalid=false;

  private postsSub: Subscription;

  constructor(public dialogRef: MatDialogRef<OmramodifComponent>,private sanitization:DomSanitizer,private omraService:OmraService,private http: HttpClient,@Inject(MAT_DIALOG_DATA) public data:any){}

  ngOnInit(){
    this.imagePreview2=null;


    this.form = new FormGroup({
      id: new FormControl(null, {
        validators: [Validators.required]
      }),
      titre: new FormControl(null, { validators: [Validators.required] }),
      date: new FormControl(null, { validators: [Validators.required] }),
      prix: new FormControl(null, { validators: [Validators.required] }),
      programme: new FormControl(null, { validators: [Validators.required] }),
      agent: new FormControl(null, { validators: [Validators.required] }),
      
      image: new FormControl(null, {
        validators: [Validators.required]
      })
    });
  }
   
  selectMultipleImage(event){
    this.valid();
    this.imagePreview2=[];
    if (event.target.files.length > 0) {
      this.multipleImages = event.target.files;
      for (var i = 0; i < event.target.files.length; i++) {  
        let file=event.target.files[i];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
        reader.onload = () => {
          this.imagePreview2.push( reader.result as string);
        };
        reader.readAsDataURL(event.target.files[i]);
        if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

          this.imageverif=true;
          this.valid();

          console.log(this.imageverif);
    
          }else{
            this.valid();

            this.imageverif=false;
            console.log(this.imageverif);
    
          }
      }  
      console.log(this.imagePreview2);
     
    }

  }
  valid(){
    if(this.form.value.id!="" && this.form.value.titre!="" && this.form.value.date!="" && this.form.value.programme!="" &&
    this.form.value.agent!="" && this.form.value.prix!=null && (this.imageverif==true || this.imagePreview2==null)
    ){
    this.formvalid=true;
    console.log(this.form.value.nom+"***");
    }else{
      this.formvalid=false;
      console.log(this.formvalid+"***"+this.form.value.pcvente);

    }
  }
  onSubmit(idg:string){
    console.log(idg);
    if(this.form.value.id!="" && this.form.value.titre!="" && this.form.value.date!="" && this.form.value.programme!="" &&
    this.form.value.agent!="" && this.form.value.prix!=null && (this.imageverif==true || this.imagePreview2==null)
    ){
      if(this.imagePreview2==null || this.imagePreview2==[]){
        this.omraService.update(this.form.value.id,this.form.value.titre,this.form.value.agent,this.form.value.programme,this.form.value.date,this.form.value.prix,this.data.comp.image,idg);

}else{
  this.omraService.update(this.form.value.id,this.form.value.titre,this.form.value.agent,this.form.value.programme,this.form.value.date,this.form.value.prix,this.imagePreview2,idg);

}
      
      /*ng serve --live-reload false
      location.reload();
            window.stop();

      */
     this.close();


    }else{
if(this.imageverif==false){
  alert('image non valid');

}else{
  alert('remplir tous les correctement');

}
    }
   
  }


  close(){
        this.dialogRef.close();


  }

}
