import { Omra } from './../../../../Models/omra';
import { OmraService } from './../../../../Services/omra.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HotelsService } from '../../../../Services/hotels.service';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material';
import { Hotels } from '../../../../Models/hotels';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-omra',
  templateUrl: './add-omra.component.html',
  styleUrls: ['./add-omra.component.scss']
})
export class AddOmraComponent implements OnInit {

  title = 'fileUpload';
  images;
  multipleImages = [];
  imagePreview: string='';
  form: FormGroup;
  imageverif=false;
  imagePreview2:string[]=[];
  ListeHotels;
  formvalid=false;
  constructor(private toastr: ToastrService,private _snackBar: MatSnackBar,public dialogRef: MatDialogRef<AddOmraComponent>,private omraServices:OmraService,private http: HttpClient){}

  ngOnInit(){
    console.log("zebinon "+this.omraServices.getHexiste());
      this.omraServices.getOmra().subscribe(        
        response =>{
  
             
              this.ListeHotels=  response;
  
              console.log(response);
  
        
           
          });
    

    this.form = new FormGroup({
      id: new FormControl(null, {
        validators: [Validators.required]
      }),
      titre: new FormControl(null, { validators: [Validators.required] }),
      date: new FormControl(null, { validators: [Validators.required] }),
      prix: new FormControl(null, { validators: [Validators.required] }),
      programme: new FormControl(null, { validators: [Validators.required] }),
      agent: new FormControl(null, { validators: [Validators.required] }),
      

      image: new FormControl(null, {
        validators: [Validators.required]
      })
    });
  }
 
  selectImage(event) {

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
      reader.onload = () => {
        this.imagePreview = reader.result as string;
      };
      reader.readAsDataURL(file);
      if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

      this.imageverif=true;
      console.log(this.imageverif);
      
      }else{
        
        this.imageverif=false;
        console.log(this.imageverif);

      }


    }
    this._snackBar.open('Cannonball!!', '', {
      duration: 5000,
      horizontalPosition: 'end',
      verticalPosition: 'top',
    });

  }
  


  selectMultipleImage(event){
    this.valid();
    this.imagePreview2=[];
    if (event.target.files.length > 0) {
      this.multipleImages = event.target.files;
      for (var i = 0; i < event.target.files.length; i++) {  
        let file=event.target.files[i];
        this.form.patchValue({ image: file });
        this.form.get("image").updateValueAndValidity();
        this.images = file;
        const reader = new FileReader();
        reader.onload = () => {
          this.imagePreview2.push( reader.result as string);
        };
        reader.readAsDataURL(event.target.files[i]);
        if(file.type=="image/png" || file.type=="image/jpeg" || file.type=="image/jpg" || file.type=="image/svg"){

          this.imageverif=true;
          this.valid();

          console.log(this.imageverif);
    
          }else{
            this.valid();

            this.imageverif=false;
            console.log(this.imageverif);
    
          }
      }  
      console.log(this.imagePreview2);
     
    }

  }

  valid(){
    if(this.form.value.id!="" && this.form.value.titre!="" && this.form.value.date!="" && this.form.value.programme!="" &&
    this.form.value.agent!="" && this.form.value.prix!=null && this.imageverif==true
    ){
    this.formvalid=true;
    console.log(this.form.valid+"***");
    }else{
      this.formvalid=false;
      console.log(this.formvalid+"***"+this.form.value.pcvente);

    }
  }

  onSubmit(){
console.log(this.form.valid);


    if(this.form.value.id!="" && this.form.value.titre!="" && this.form.value.date!="" && this.form.value.programme!="" &&
    this.form.value.agent!="" && this.form.value.prix!=null && this.imageverif==true
    ){
    

    let hotel=new Omra;
hotel.id=this.form.value.id;
hotel.titre=this.form.value.titre;
hotel.agent=this.form.value.agent;
hotel.date=this.form.value.date;
hotel.programme=this.form.value.programme;
hotel.prix=this.form.value.prix;

hotel.image=this.imagePreview2;

      this.omraServices.addPost(hotel,this.imagePreview2);
         console.log(hotel);
         for(let i of this.ListeHotels){
          if(i.id==this.form.value.id){
            this.toastr.success('', "Hotel existe in the external api it will be choosen by the lowest prix");
            
            return;
          }
        }
      
    }else{
if(this.imageverif==false){
  alert('image non valid');

}else{
  alert('remplir tous les correctement');

}
    }
   
  }

  onMultipleSubmit(){
    const formData = new FormData();
    for(let img of this.multipleImages){
      formData.append('files', img);
    }

    this.http.post<any>('http://localhost:3000/multipleFiles', formData).subscribe(
      (res) => console.log(res),
      (err) => console.log(err)
    );
  }
  close(){
        this.dialogRef.close();


  }

}
