import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OmrareservationComponent } from './omrareservation.component';

describe('OmrareservationComponent', () => {
  let component: OmrareservationComponent;
  let fixture: ComponentFixture<OmrareservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OmrareservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OmrareservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
