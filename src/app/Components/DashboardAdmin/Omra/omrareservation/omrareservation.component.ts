import { LoginService } from './../../../../Services/login.service';
import { OmraService } from './../../../../Services/omra.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource,MatSort,MatPaginator, MatDialog } from '@angular/material';
import { HotelsService } from '../../../../Services/hotels.service';
import { Subscription } from 'rxjs';
import { Hotels } from '../../../../Models/hotels';
import { Router } from '@angular/router';
@Component({
  selector: 'app-omrareservation',
  templateUrl: './omrareservation.component.html',
  styleUrls: ['./omrareservation.component.scss']
})
export class OmrareservationComponent implements OnInit {

  constructor(private omraService: OmraService,private userServices: LoginService,public matDialog: MatDialog,private router:Router) { } 

  listData: MatTableDataSource<any>;
  listeReservationAccepter : MatTableDataSource<any>;
  listeReservationEnAttente : MatTableDataSource<any>;

  displayedColumns: string[] = ['id', 'nom','actions'];
  ReservationHotelColumns: string[] = ['Titre', 'Email','Prix','actions'];
  ReservationHotelColumns2: string[] = ['Titre', 'Email','Prix','actions'];

  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  
  searchKey;searchKey2;searchKey3;


  LReservationAccepter ;LReservationEnAttente ;
  
  ngOnInit() {
 
    //this.getData();
    this.GetListeReservationAccepter();
    this.GetListeReservationEnAttente();
  }

 
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
  onSearchClear2() {
    this.searchKey2 = "";
    this.applyFilter2();
  }
  onSearchClear3() {
    this.searchKey3 = "";
    this.applyFilter3();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }
  applyFilter2() {
    this.listeReservationAccepter.filter = this.searchKey2.trim().toLowerCase();
  }
  applyFilter3() {
    this.listeReservationEnAttente.filter = this.searchKey3.trim().toLowerCase();
  }


  deleteReservationOmra(id:string){
    console.log(id);
    const main=async () => {
  
      try {
       
        this.omraService.deleteReservationOmra(id);
  
      }
      catch (error) {
          console.error(error);
      }
  
  };
  main().then(() =>{ 
    this.GetListeReservationAccepter();
    this.GetListeReservationEnAttente();
  });
   
  }
    
  
  GetListeReservationAccepter(){
  
    this.omraService.getReservationAccepter().subscribe(        
      async response =>{
        this.LReservationAccepter= await response;
        this.listeReservationAccepter = new MatTableDataSource(this.LReservationAccepter);
        this.listeReservationAccepter.sort = this.sort;
        this.listeReservationAccepter.paginator = this.paginator;
        this.listeReservationAccepter.filterPredicate = (data, filter) => {
          console.log("-------"+filter)
          return this.ReservationHotelColumns.some(ele => {
            return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
          });
        };
        console.log(response);
      },
      err =>{"error"}
    );
      
 



}

GetListeReservationEnAttente(){
     
      this.omraService.getReservationEnAttent().subscribe(        
        async response =>{
          this.LReservationEnAttente= await response;
          this.listeReservationEnAttente = new MatTableDataSource(this.LReservationEnAttente);
          this.listeReservationEnAttente.sort = this.sort;
          this.listeReservationEnAttente.paginator = this.paginator;
          this.listeReservationEnAttente.filterPredicate = (data, filter) => {
            return this.ReservationHotelColumns2.some(ele => {
              return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
            });
          };
          console.log(response);
        },
        err =>{"error"}
      );    
}

  
    ReservationAccepter(id : string){
    
      const main=async () => {
  
        try {
         
          this.omraService.ReservationAccepter(id);
    
        }
        catch (error) {
            console.error(error);
        }
    
    };
    main().then(() =>{ 
      this.GetListeReservationAccepter();
      this.GetListeReservationEnAttente();
    });
    }
    UpgradePayement(id:any){
      const main=async () => {
  
        try {
         
          this.omraService.UpgradePayement(id);
          this.GetListeReservationAccepter();

        }
        catch (error) {
            console.error(error);
        }
    
    };
    main().then(() =>{ 
      this.GetListeReservationAccepter();
  
    });
    }
  
}
