import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OmracontroleComponent } from './omracontrole.component';

describe('OmracontroleComponent', () => {
  let component: OmracontroleComponent;
  let fixture: ComponentFixture<OmracontroleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OmracontroleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OmracontroleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
