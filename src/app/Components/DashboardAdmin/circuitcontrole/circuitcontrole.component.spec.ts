import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CircuitcontroleComponent } from './circuitcontrole.component';

describe('CircuitcontroleComponent', () => {
  let component: CircuitcontroleComponent;
  let fixture: ComponentFixture<CircuitcontroleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CircuitcontroleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CircuitcontroleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
