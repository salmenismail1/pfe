import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;


import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { Subscription } from 'rxjs';
import { Hotels } from '../../../Models/hotels';
import { Router } from '@angular/router';
import { CircuitService } from '../../../Services/circuit.service';
import { Circuit } from '../../../Models/circuit';

@Component({
  selector: 'app-circuitcontrole',
  templateUrl: './circuitcontrole.component.html',
  styleUrls: ['./circuitcontrole.component.scss']
})
export class CircuitcontroleComponent implements OnInit {
  @ViewChild(MatSort,null) sort: MatSort;
  @ViewChild(MatPaginator,null) paginator: MatPaginator;
  checkedlist:Array<any>=[];
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['id', 'nom','actions'];
  searchKey: string;
  voyagesearchKey: string;
  soireesearchKey: string;


  
  l: Hotels[]=[];

  ListeHotels;
  private postsSub: Subscription;

  constructor(private circuitServices: CircuitService,public matDialog: MatDialog,private router:Router) { } 


  ngOnInit() {
    this.getData();

  }

  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
    console.log(this.l);

    this.listData._updatePaginator;

  }
  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }




  getData(){
    let lst: Hotels[]=[];
    let l=[];
    let ListeHotels=null;
    let nmb=0;
    
console.log(ListeHotels);
    this.circuitServices.getCircuitBase();
    this.postsSub = this.circuitServices.getPostUpdateListener()
      .subscribe((posts: Circuit[]) => {

        const main=async () => {

          try {
            l = await posts;


      
          }
          catch (error) {
              console.error(error);
          }
      
      };
      main().then(() =>{ 
        nmb+=l.length;

      console.log("hethi el posts"+l+" nbm="+l.length);
      console.log(nmb);
      this.circuitServices.getCircuit().subscribe(        
        response =>{
          const main=async () => {

            try {
              console.log("hethi el ListeHotels 9bal matet3aba el ListeHotels");
              console.log(ListeHotels);
              console.log("hethi el l 9bal matet3aba el ListeHotels");
              console.log(l);
              ListeHotels= await response;

  
        
            }
            catch (error) {
                console.error(error);
            }
        
        };
        main().then(() =>{ 
          console.log("hethi el ListeHotels ba3d matet3aba el ListeHotels");
          console.log(ListeHotels);
          nmb+=ListeHotels.length;
          console.log(nmb);

          console.log("hethi el l ba3d matet3aba el ListeHotels");
          console.log(l);
          let image:any;
     

          for(let i=0;i<ListeHotels.length;i++){
            image="https://freedomtravel.tn/assets/images/hotel_miniature/"+ListeHotels[i].id+".jpg";
             const post: Hotels = {  id : ListeHotels[i].id,nom : ListeHotels[i].nom,adultOnly : ListeHotels[i].adultOnly,ville : ListeHotels[i].ville,categorie : ListeHotels[i].categorie,type : ListeHotels[i].type,
        lpdvente : ListeHotels[i].lpdvente,dpvente : ListeHotels[i].dpvente,pcvente : ListeHotels[i].pcvente,allinsoftvente : ListeHotels[i].allinsoftvente,allinvente : ListeHotels[i].allinvente,
        ultraallinvente : ListeHotels[i].ultraallinvente,age_enf_gratuit : ListeHotels[i].age_enf_gratuit,image:image
        };
      
          l.push(post);
        
            
          }
         
            console.log(nmb);
            console.log(l);

            for(let i=0;i<nmb;i++){
              if(!lst.includes(l[i]))
              lst.push(l[i]);
            }
            console.log(lst);
            console.log(this.listData);
        
            this.listData = new MatTableDataSource(lst);
            this.listData.sort = this.sort;
            this.listData.paginator = this.paginator;
            this.listData.filterPredicate = (data, filter) => {
              return this.displayedColumns.some(ele => {
                return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
              });
            };
            this.listData._updatePaginator;
         
        }).catch(() => console.error('Failed!'));
  
         
        
  
    
  
 
        },
        err =>{"error"}
      );
    }).catch(() => console.error('Failed!'));

      
      
      });
 
  }

  check(item:any){
    console.log(item);
    if(this.checkedlist.length<6){

if(!this.checkedlist.includes(item)){
  this.checkedlist.push(item);

}else{
  this.checkedlist.splice(this.checkedlist.indexOf(item),1);
}
    }
console.log(this.checkedlist);
}

confirmer(){
  this.circuitServices.deleteall();
for(let item of this.checkedlist ){
  this.circuitServices.circuithomelistpost(item);
}
this.checkedlist=[];

this.getData();
}

 

}
