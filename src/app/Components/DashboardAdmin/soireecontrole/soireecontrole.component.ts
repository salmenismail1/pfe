import { Component, OnInit,ViewChild } from '@angular/core';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog' ;


import { MatTableDataSource,MatSort,MatPaginator } from '@angular/material';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { Soiree } from '../../../Models/soiree';
import { SoireesService } from '../../../Services/soirees.service';
@Component({
  selector: 'app-soireecontrole',
  templateUrl: './soireecontrole.component.html',
  styleUrls: ['./soireecontrole.component.scss']
})
export class SoireecontroleComponent implements OnInit {

  @ViewChild(MatSort,null) sortsoiree: MatSort;
  @ViewChild(MatPaginator,null) paginatorsoiree: MatPaginator;
  soireesearchKey: string;



  sl: Soiree[];
  ListeSoiree;



  private soireepostsSub: Subscription;

  soireecheckedlist:Array<any>=[];
  soireelistData: MatTableDataSource<any>;
  soireedisplayedColumns: string[] = ['id', 'titre','actions'];

  constructor(private soireeServices: SoireesService,public matDialog: MatDialog,private router:Router) { } 

  ngOnInit() {
    this.getSoireeData();

  }
  onSoireeSearchClear() {
    this. soireesearchKey = "";
    this.applySoireeFilter();
  }




  applySoireeFilter() {
    this.soireelistData.filter = this.soireesearchKey.trim().toLowerCase();
  }



  getSoireeData(){
    let lst: Soiree[]=[];
    let sl=[];
    let ListeSoiree;
    let nmb=0;
console.log(ListeSoiree);

  
    this.soireeServices.getSoireesBase();
    this.soireepostsSub = this.soireeServices.getPostUpdateListener()
      .subscribe((posts: Soiree[]) => {
        
        const main=async () => {

          try {
            sl = await posts;


      
          }
          catch (error) {
              console.error(error);
          }
      
      };
      main().then(() =>{ 
        console.log("hethi el posts");
        console.log(sl);
        nmb+=sl.length;
        console.log(nmb);

        this.soireeServices.getSoiree().subscribe(        
          response =>{

            const main=async () => {

              try {
               
  
                ListeSoiree= await response;
          
              }
              catch (error) {
                  console.error(error);
              }
          
          };
          main().then(() =>{ 
            nmb+=ListeSoiree.length;
            console.log(nmb);
  
            let image:any;
            for(let i=0;i<ListeSoiree.length;i++){
              console.log(""+ListeSoiree[i]);
              image=""+ListeSoiree[i].image;
              const post: Soiree = {  id :  ListeSoiree[i].id,titre:ListeSoiree[i].titre,image : image, date : ListeSoiree[i].date,venteadultesingle : ListeSoiree[i].venteadultesingle,
                venteadultedble :ListeSoiree[i].venteadultedble, vente3rdad : ListeSoiree[i].vente3rdad,venteenfant2ad : ListeSoiree[i].venteenfant2ad,tarifbebe : ListeSoiree[i].tarifbebe,
                venteenfant1ad : ListeSoiree[i].venteenfant1ad,programme:ListeSoiree[i].programme
            };
              sl.push(post);
            }
            console.log(nmb);
  
        for(let i=0;i<nmb;i++){
          lst.push(sl[i]);
        }
        console.log(lst);
    
        this.soireelistData = new MatTableDataSource(lst);
        this.soireelistData.sort = this.sortsoiree;
        this.soireelistData.paginator = this.paginatorsoiree;
        this.soireelistData.filterPredicate = (data, filter) => {
          return this.soireedisplayedColumns.some(ele => {
            return ele != 'actions' && data[ele].toString().toLowerCase().indexOf(filter) != -1;
          });
        };

          }).catch(() => console.error('Failed!'));
          
          },
          err =>{"error"}
        );
      }).catch(() => console.error('Failed!'));

       
      });


      
  }
  
  soireecheck(item:any){
    console.log(item);
    if(this.soireecheckedlist.length<2){

  if(!this.soireecheckedlist.includes(item)){
  this.soireecheckedlist.push(item);
  
  }else{
    this.soireecheckedlist.splice(this.soireecheckedlist.indexOf(item),1);
  }
}
  console.log(this.soireecheckedlist);
  }
  
  soireeconfirmer(){
    this.soireeServices.deleteall();
  for(let item of this.soireecheckedlist ){
    this.soireeServices.soireehomelistpost(item);
  }
  this.soireecheckedlist=[];
  this. getSoireeData();
  }
  

}
