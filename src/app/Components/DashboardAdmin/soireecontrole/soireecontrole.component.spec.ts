import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoireecontroleComponent } from './soireecontrole.component';

describe('SoireecontroleComponent', () => {
  let component: SoireecontroleComponent;
  let fixture: ComponentFixture<SoireecontroleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoireecontroleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoireecontroleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
