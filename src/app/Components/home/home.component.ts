import { Component, OnInit, Input, Sanitizer, ViewChild } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { Caroussel } from '../../Models/caroussel';
import { Observable, Subject, Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HotelsService } from '../../Services/hotels.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { VoyagesService } from '../../Services/voyages.service';
import { Voyages } from '../../Models/voyages';
import { User } from '../../Models/user';
import { Hotels } from '../../Models/hotels';
import { Circuit } from '../../Models/circuit';
import { CircuitService } from '../../Services/circuit.service';
import { SoireesService } from '../../Services/soirees.service';
import { Soiree } from '../../Models/soiree';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild('ngcarousel', { static: true }) ngCarousel: NgbCarousel;

backgroundUrl="http://www.wa11papers.com/assets/previews/nature-sea-landscape-ocean-sunset-sunrise-wallpaper-1082-preview-0e49f9f0.jpg";
url2;
ListeHotels;
num=0;
voyage = new Voyages;
circuit = new Circuit;
ListeCircuits: Circuit[] = [];
l: User[] = [];
  private postsSub: Subscription;
  private voyagepostsSub: Subscription;
  private circuitpostsSub: Subscription;

  listpersohotel: Hotels[] = [];
  ListeVoyages: Voyages[] = [];
  ListeSoiree: Soiree[] = [];

  constructor(private circuitServices : CircuitService,private soireeServices : SoireesService,private sanitization:DomSanitizer,private httpClient:HttpClient,private hotelServices:HotelsService,private voyageService:VoyagesService) { 

  }
  ngOnInit() {
    this.circuitServices.getCircuithomelist();

    this.circuitpostsSub=this.circuitServices.gethomecircuitUpdateListener().subscribe((posts: Circuit[]) => {
        this.ListeCircuits=posts;
        console.log("****////***"+posts);

      }
    );
    console.log("*******"+this.ListeCircuits);

    this.hotelServices.getHotelhomelist();
    this.postsSub = this.hotelServices.gethomehotelUpdateListener()
      .subscribe((posts: Hotels[]) => {
        this.listpersohotel = posts;
        this.next();
      });
  console.log(this.listpersohotel);
    


  this.voyageService.getVoyagehomelist();
  this.voyagepostsSub = this.voyageService.gethomevoyageUpdateListener()
    .subscribe((posts: Voyages[]) => {
      this.ListeVoyages = posts;
      this.next();
    });
console.log(this.ListeVoyages);

    
    this.hotelServices.getHotel().subscribe(        
      response =>{
        this.ListeHotels=response;
        console.log(response)
        console.log(this.listpersohotel[0]);
        console.log(this.listpersohotel);

      },
      err =>{"error"}
    )
    this.soireeServices.getSoireehomelist();
    this.voyagepostsSub = this.soireeServices.gethomesoireeUpdateListener()
      .subscribe((posts: Soiree[]) => {
        this.ListeSoiree = posts;
        this.next();
      });
  console.log(this.ListeSoiree);
  }
  
  getPrix(hotel: Hotels)
  {
    return this.hotelServices.getPrix(hotel);
  }
  prev(){
    if(this.num==0){
      this.num=4;

    }else{
      this.num--;
    }

  }
  
  next(){
if(this.num==4){
  this.num=0;

}else{
  this.num++;

}

  }
  sauve(item:any){
    localStorage.setItem("hotel",JSON.stringify(item));
    console.log( localStorage.getItem("hotel"));
    const hotel:any=JSON.parse( localStorage.getItem("hotel"));
    console.log( hotel.id);
    console.log( hotel);


      }

  /*<ngb-carousel #ngcarousel style="width: 100%;" class="d-flex flex-wrap justify-content-center">


        <div *ngFor="let item of ListeHotels ; let i = index" style="width: 33%;"  > 
          <ng-template ngbSlide >

            <div class="card" style="width: 18rem;" *ngIf="i < (5)">
                <img class="card-img-top" src="https://freedomtravel.tn/assets/images/hotel_miniature/{{item.id}}.jpg" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
              </div>    
            </ng-template>

            </div>
      
</ngb-carousel>*/
 

}