import { Component, OnInit } from '@angular/core';
import { HotelsService } from '../../Services/hotels.service';
import { Hotels } from '../../Models/hotels';
import { DomSanitizer } from '@angular/platform-browser';
import { FiltersPipe } from '../../Filters/filters.pipe';
import { Router } from '@angular/router';
@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {

  loading = true;
  ListeHotels;
  text;
  listVille;
  ville ;
  test="";
  etoile;
  max;
  min;
  ListeHotelsBase=[];
  d;
  minDate;maxDate;
  showFiller = false;
  constructor(private hotelServices:HotelsService,private sanitizer:DomSanitizer,private router:Router) { }
  ngOnInit() {
    this.hotelServices.getHotelBase();
    this.hotelServices.getPostUpdateListener().subscribe(        
  response =>{
    if(this.ListeHotels!=undefined){
      this.loading = false;
    }
        this.ListeHotelsBase=response;
        console.log(this.ListeHotelsBase)
        this.compare(this.ListeHotelsBase,this.ListeHotels);
  },
  err =>{"error"}
);
    this.hotelServices.getHotel().subscribe(        
      response =>{
        if(this.ListeHotelsBase!=undefined){
          this.loading = false;
        }
        this.ListeHotels=response;
        this.compare(this.ListeHotelsBase,this.ListeHotels);
        console.log(response);
      },
      err =>{"error"}
    )
    this.hotelServices.getVille().subscribe( 
      ville =>{
        this.listVille=ville;},
      error => console.log(error) 
    );

  }
  getSanitizerUrl(url:string){
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
  getPrix(hotel: Hotels)
  {
    return this.hotelServices.getPrix(hotel);
  }
  testing1(){
    console.log(this.test);
    this.test= ""; 
    this.reset();
  }
  testing2(){
    console.log(this.test);
    this.test= "ville";
    this.test= "f"; 
    this.text= ""; 
  }
    testing3(s:string){
      this.etoile=s;
      console.log(this.test);
      this.test= "etoile"; 
      this.test= "f";
      this.text= ""; 
    }
    testing4(){
      this.test="prix";
      console.log("min : "+this.min+"max : "+this.max);
      this.test= "f" ;
          this.text= ""; 
    }
    reset(){
      this.min=0;
      this.max=300;
      this.ville="Liste des Villes";
    }
    compare(lstbase:any,lstapi:any){
      if(lstbase.length>0 && lstapi.length>0){
        console.log("nam");

      for(let item of lstbase){
      for(let item2 of lstapi){
        if(item.id==item2.id){
          if(this.hotelServices.getPrix(item2)>=this.hotelServices.getPrix(item)){
            lstapi[lstapi.indexOf(item2)]=[];
          }else{
            lstbase[lstbase.indexOf(item)]=[];

          }
        } 
      }
    }
  }
  this.ListeHotels=lstapi;
  this.ListeHotelsBase=lstbase;
}
sourceBase(item:Hotels){
  let data=JSON.stringify(item);
  localStorage.setItem('source',"base");
  localStorage.setItem('item',data);
}
sourceApi(item:Hotels){
  let data=JSON.stringify(item);
  localStorage.setItem('source',"api");
  localStorage.setItem('item',data);
}

}