import { Component, OnInit } from '@angular/core';
import { SoireesService } from '../../Services/soirees.service';

@Component({
  selector: 'app-soiree',
  templateUrl: './soiree.component.html',
  styleUrls: ['./soiree.component.css']
})
export class SoireeComponent implements OnInit {
ListeSoirees;
l;
min;max;test="";text;d;

ListeSoireesBase;
  constructor(private soireeService :SoireesService) { }

  ngOnInit() {
    this.soireeService.getSoiree().subscribe(        
      response =>{
        this.ListeSoirees=response;
        console.log(response)
      },
      err =>{"error"}
    )

    this.soireeService.getSoireesBase();
      this.soireeService.getPostUpdateListener().subscribe(        
        response =>{
      
        this.ListeSoireesBase=response;
        console.log(this.ListeSoireesBase)
        this.compare(this.ListeSoireesBase,this.ListeSoirees);
      },
      err =>{"error"}
    );
  }



  testing1(){
    this.test= ""; 
    this.reset();
  }
  
    testing4(){
      this.test="prix";
      this.test= "f" ;
      this.text= ""; 
    }
    reset(){
      this.min=0;
      this.max=300;

    }

    compare(lstbase:any,lstapi:any){
      if(lstbase.length>0 && lstapi.length>0){
  
      for(let item of lstbase){
      for(let item2 of lstapi){
        if(item.id==item2.id){
          if(item2.venteadultesingle>=item.venteadultesingle){
            lstapi[lstapi.indexOf(item2)]=[];
          }else{
            lstbase[lstbase.indexOf(item)]=[];

  
          }
        } 
      }
    }
  }
  this.ListeSoirees=lstapi;
  this.ListeSoireesBase=lstbase;
    }


    sourceBase(item:any){
      let data=JSON.stringify(item);
      localStorage.setItem('source',"base");
      localStorage.setItem('item',data);
    
    }
    sourceApi(item:any){
      let data=JSON.stringify(item);
      localStorage.setItem('source',"api");
      localStorage.setItem('item',data);
    }
}