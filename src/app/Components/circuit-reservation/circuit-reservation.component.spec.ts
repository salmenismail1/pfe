import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CircuitReservationComponent } from './circuit-reservation.component';

describe('CircuitReservationComponent', () => {
  let component: CircuitReservationComponent;
  let fixture: ComponentFixture<CircuitReservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CircuitReservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CircuitReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
