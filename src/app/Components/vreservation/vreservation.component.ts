import { Component, OnInit,  AfterViewInit,ViewChild, ElementRef} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate } from '@angular/common';
import {HttpHeaders, HttpClient } from '@angular/common/http';
import { Voyages } from '../../Models/voyages';
import { Chambre } from '../../Models/chambre';
import * as html2pdf  from 'html2pdf.js';
import { Observable } from 'rxjs';
import { ReservationService } from '../../Services/reservation.service';
import { Reservation } from '../../Models/reservation';
import {FormControl, FormGroup, Validators, FormBuilder} from "@angular/forms";
import * as jwt_decode from 'jwt-decode';
import { ToastrService } from 'ngx-toastr';
const AjoutReservationUrl = "http://localhost:3000/voyage/api/reservation";
const httpOptions = {   headers : new HttpHeaders({'Content-Type':'application/json'})};
@Component({
  selector: 'app-vreservation',
  templateUrl: './vreservation.component.html',
  styleUrls: ['./vreservation.component.css']
})
export class VreservationComponent implements OnInit,AfterViewInit {

  @ViewChild('content', { static: true }) content:ElementRef;
  source = localStorage.getItem('source')
  reactiveform:FormGroup;
  reservation = new Reservation();
  id;
  e;
  unvoyage;
  voyage;
  x;ListeDate=[];
  nb;
  TChambre;nbChambre = new Array(5);
  prix : number;
  venteadultesingle:number;venteadultedble:number;tarifbebe:number;
  datedepart;dateretour;
  Adulte;Enfant;Bebe;nbrAdulte ; nbrEnfant ; nbrBebe; TAdulte ; TEnfant; TBebe;TnbrAdulte;TnbrEnfant;TnbrBebe;
  ListeImage ;
  constructor(private router:Router, private toastr: ToastrService,private Activate:ActivatedRoute,private httpClient:HttpClient,private serviceReservation:ReservationService) { }
  ngOnInit() {
    console.log(this.x)
    this.nb=4;
    this.id =this.Activate.snapshot.paramMap.get("id");

    
    console.log(this.source)
    if(this.source=="base"){
      this.x=JSON.parse( localStorage.getItem('item'));
      this.ListeDate.push(this.x.departvoyages)
        this.ListeImage=JSON.parse( localStorage.getItem('item')).image ;
    }
    else{
      this.unvoyage = "https://topresa.ovh/api/departvoyages.json?departvoyages.datedepart[after]="+formatDate(new Date(), 'yyyy-MM-dd', 'en')+"&voyage.id="+this.id;
    this.getUnVoyage().subscribe(        
      response =>{
        this.x=response[0];
        this.voyage=response;
        console.log(this.x)
        console.log(response)
      }
    )
      console.log(this.x)
       this.ListeImage=JSON.parse( localStorage.getItem('item')).imageVoyages;
       console.log(this.ListeImage)
    }
    this.reactiveform= new FormGroup({
      'Nom' : new FormControl('',[Validators.required]),
      'Email' : new FormControl('', [Validators.email,Validators.required]),
      'Tel' : new FormControl('', [Validators.required, Validators.minLength(8),Validators.pattern('[0-9]*')]),
    })
  }
  get Nom() { return this.reactiveform.get('Nom'); }
  get Email() { return this.reactiveform.get('Email'); }
  get Tel() { return this.reactiveform.get('Tel'); }
  getUnVoyage():Observable<Voyages>{
    return this.httpClient.get<Voyages>(this.unvoyage,httpOptions);
  }
  chambre(){
    for(let i=0;i<this.nb;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+i);
      if(selectElement.hidden==true){
        selectElement.hidden=false;
        console.log(selectElement);
        return;
      }
    }
  }
  delchambre(i:number){
    const selectElement = <HTMLSelectElement>document.getElementById('rowid'+i);
    selectElement.hidden=true;
    const select1 = <HTMLSelectElement>document.getElementById('homme'+i);
    select1.selectedIndex=0;
    const select2 = <HTMLSelectElement>document.getElementById('enfant'+i);
    console.log(select2.selectedIndex)
    select2.selectedIndex=0;
    const select3 = <HTMLSelectElement>document.getElementById('bebe'+i);
    select3.selectedIndex=0;
    console.log(selectElement);

  }
  ngAfterViewInit(): void {
    const selectElement = <HTMLSelectElement>document.getElementById('rowid'+0);
    selectElement.hidden=false;
    console.log(selectElement);
  }

  

  Prix(venteadultesingle:any,venteadultedble:any,tarifbebe:any,datedepart:any,dateretour:any){
    this.venteadultesingle=venteadultesingle;
    this.venteadultedble = venteadultedble;
    this.tarifbebe = tarifbebe;
    this.datedepart=datedepart;
    this.dateretour=dateretour;
  }
  calculPrix(){
    this.prix=0;
    for(let j=0;j<this.nb;j++){
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+j);
      if(selectElement.hidden==false){
        const select1 = <HTMLSelectElement>document.getElementById('homme'+j);
        const select2 = <HTMLSelectElement>document.getElementById('enfant'+j);
        const select3 = <HTMLSelectElement>document.getElementById('bebe'+j);

        
        this.prix+= (select1.selectedIndex*this.venteadultesingle)+( select2.selectedIndex*this.venteadultedble)+(select3.selectedIndex*this.tarifbebe)
        console.log(this.prix);
      }
    }
    console.log(this.prix);
  }
  clear() {
    this.reactiveform.reset();
    for(let i=0;i<this.nb;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+i);
      selectElement.hidden=true;
      const firstElement = <HTMLSelectElement>document.getElementById('rowid'+0);
      firstElement.hidden=false;
        const select1 = <HTMLSelectElement>document.getElementById('homme'+i);
        select1.selectedIndex=0;
        const select2 = <HTMLSelectElement>document.getElementById('enfant'+i);
        select2.selectedIndex=0;
        const select3 = <HTMLSelectElement>document.getElementById('bebe'+i);
        select3.selectedIndex=0;
    }
  }
  pdf(){
    const options = {
      margin:       15,
      filename:     'myfile.pdf',
      image:        { type: 'jpeg' },
      jsPDF:        {orientation: 'landscape' }
    };
    var content = document.getElementById('content');
    html2pdf()
      .from(content)
      .set(options)
      .save();
  }

  affiche(){
    this.TnbrAdulte= new Array();  this.TnbrEnfant= new Array();  this.TnbrBebe= new Array();
    this.nbrAdulte=0; this.nbrEnfant=0; this.nbrBebe=0;
    this.TChambre=[] ;
    let cmp = 0;
    for(let j=0;j<this.nb;j++){
      const selectElement = <HTMLSelectElement>document.getElementById('rowid'+j);

      if(selectElement.hidden==false){
         this.Adulte = <HTMLSelectElement>document.getElementById('homme'+j);
         this.Enfant = <HTMLSelectElement>document.getElementById('enfant'+j);
         this.Bebe = <HTMLSelectElement>document.getElementById('bebe'+j);
         this.nbrAdulte +=this.Adulte.selectedIndex ;
         this.nbrEnfant+= this.Enfant.selectedIndex;
         this.nbrBebe += this.Bebe.selectedIndex;
         this.TnbrAdulte[cmp]=this.Adulte.selectedIndex;
         this.TnbrEnfant[cmp]=this.Enfant.selectedIndex;
         this.TnbrBebe[cmp]=this.Bebe.selectedIndex;
         cmp+=1;
        let chambre= new Chambre();
      chambre.num=j+1+"";
      console.log(chambre)
      this.TChambre.push(chambre);
      }
      this.TAdulte=new Array(this.nbrAdulte)
      this.TEnfant=new Array(this.nbrEnfant)
      this.TBebe=new Array(this.nbrBebe)
    }
    let decoded : any = jwt_decode(localStorage.getItem('token'));
    this.reservation.IdClient=decoded.id+"";
    console.log(this.reservation.IdClient);
    this.reservation.Prix=this.prix;
    this.reservation.Titre=this.x.titre;
    this.reservation.DateReservation= new Date().toString();
    this.reservation.DateDepart=formatDate(this.datedepart, 'yyyy-MM-dd', 'en');
    this.reservation.DatRetour=formatDate(this.dateretour, 'yyyy-MM-dd', 'en');
    this.reservation.Nom=this.reactiveform.value.Nom;
    this.reservation.Email=this.reactiveform.value.Email;
    this.reservation.Tel=this.reactiveform.value.Tel;
    console.log(this.reservation.Nom);
  }
  test(){
    if(this.nbrAdulte==0){
      this.TAdulte=new Array(this.nbrAdulte)
    }
    else{
      this.TAdulte=new Array(this.nbrAdulte-1)
    }
    if(this.nbrEnfant==0){
      this.TEnfant=new Array(this.nbrEnfant)
    }
    else{
      this.TEnfant=new Array(this.nbrEnfant-1)
    }
    let j=1;
    let k = 0;
    let t = [];
    for(let i=0;i<this.nbrAdulte;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('Adulte'+i);
      if(j<this.TnbrAdulte[k] ){
        t.push(selectElement.value+"")
        j+=1;
      }
        else{
        t.push(selectElement.value+"")
        this.TChambre[k].adulte=t;
        t=[];
        j=1;
        k+=1;
        }

        
    }
    j=1;
    k = 0;
    t=[];
    for(let i=0;i<this.nbrEnfant;i++){
      console.log(i)
      const selectElement2 = <HTMLSelectElement>document.getElementById('Enfant'+i);
        if(j<this.TnbrEnfant[k] ){
          t.push(selectElement2.value+"")
          j+=1;
        }
          else{
          t.push(selectElement2.value+"")
          this.TChambre[k].enfant=t;
          t=[];
          j=1;
          k+=1;
      }
    }
    j=1;
    k = 0;
    t=[];
    for(let i=0;i<this.nbrBebe;i++){
      const selectElement = <HTMLSelectElement>document.getElementById('Bebe'+i);
      if(j<this.TnbrBebe[k] ){
        t.push(selectElement.value+"")
        j+=1;
      }
      else{
        t.push(selectElement.value+"")
        this.TChambre[k].bb=t;
        t=[];
        j=1;
        k+=1;
    }

        
    }
    this.reservation.Chambre=this.TChambre;
  }
  AjoutCambre(){
    this.serviceReservation.AjoutReservation(AjoutReservationUrl,this.reservation).subscribe(        
      response =>{
        console.log(response)
       this.toastr.success('Merci d\'attendre l\'acceptation de votre reservation', 'Réservation Voyage avec succée');

       
      },

      
      err =>{this.toastr.error('', 'Verifiez vos Donnees')}
    )
  }
}