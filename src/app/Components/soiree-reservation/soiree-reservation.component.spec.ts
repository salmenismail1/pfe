import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoireeReservationComponent } from './soiree-reservation.component';

describe('SoireeReservationComponent', () => {
  let component: SoireeReservationComponent;
  let fixture: ComponentFixture<SoireeReservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoireeReservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoireeReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
