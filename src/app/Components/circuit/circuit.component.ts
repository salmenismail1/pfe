import { Component, OnInit } from '@angular/core';
import { CircuitService } from '../../Services/circuit.service';
import { Circuit } from '../../Models/circuit';


@Component({
  selector: 'app-circuit',
  templateUrl: './circuit.component.html',
  styleUrls: ['./circuit.component.css']
})
export class CircuitComponent implements OnInit {
  circuit = new Circuit;
  ListeCircuits;ListeCircuitsBase

  min;max;text;test="";
  

  constructor(private circuitServices : CircuitService) { }

  ngOnInit() {
    this.circuitServices.getCircuit().subscribe(        
      response =>{
        this.ListeCircuits=response;
        console.log(response)
      },
      err =>{"error"}
    )

    this.circuitServices.getCircuitBase();
      this.circuitServices.getPostUpdateListener().subscribe(        
        response =>{
      
        this.ListeCircuitsBase=response;
        console.log(this.ListeCircuitsBase)
        this.compare(this.ListeCircuitsBase,this.ListeCircuits);
      },
      err =>{"error"}
    );
  }

  compare(lstbase:any,lstapi:any){
    if(lstbase.length>0 && lstapi.length>0){

    for(let item of lstbase){
    for(let item2 of lstapi){
      if(item.id==item2.id){
        if(item2.venteadultesingle>=item.venteadultesingle){
          lstapi[lstapi.indexOf(item2)]=[];
        }else{
          lstbase[lstbase.indexOf(item)]=[];


        }
      } 
    }
  }
}
this.ListeCircuits=lstapi;
this.ListeCircuitsBase=lstbase;
  }

  sourceBase(item:any){
    let data=JSON.stringify(item);
    localStorage.setItem('source',"base");
    localStorage.setItem('item',data);
  
  }
  sourceApi(item:any){
    let data=JSON.stringify(item);
    localStorage.setItem('source',"api");
    localStorage.setItem('item',data);
  }

  testing1(){
    console.log(this.test);
    this.test= ""; 
    this.reset();
  }
  
    testing4(){
      this.test="prix";
      console.log("min : "+this.min+"max : "+this.max);
      this.test= "f" ;
      this.text= ""; 
    }
    reset(){
      this.min=0;
      this.max=3000;

    }

}