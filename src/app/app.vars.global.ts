import { Injectable } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import {FormControl,AbstractControl, FormGroup, Validators, FormBuilder} from "@angular/forms";
import { ModalDirective } from 'angular-bootstrap-md';
import { MatDialogRef } from '@angular/material/dialog';
import { LoginService } from './Services/login.service';
import { Login } from './Models/login';
import { User } from './Models/user';
import * as jwt_decode from 'jwt-decode';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Injectable()

export class AppVarsGlobal{
    public  type = "";
    log = new Login  ;
    user = new User;

    constructor(private toastr: ToastrService,private loginserv:LoginService,private router:Router) { }
    login(){
        
                let decoded : any = jwt_decode(localStorage.getItem('token'));
    
                this.loginserv.getOnUser(decoded.id).subscribe(
                    response => {
                        this.type = response.type;
                
                    });
    
          
        return this.type ;
      }
    
      
       

}