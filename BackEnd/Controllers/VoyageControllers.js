const express = require('express');
const bodyParser = require('body-parser');

const { RVoyage }  = require('../Models/ReservationVoyage');
const multer = require('multer');
const { Voyage }  = require('../Models/Voyage');
const { Voyagehomeliset }  = require('../Models/voyagehomeliset');

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST,PUT, PATCH, DELETE, OPTIONS"
  );
  next();
});

const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, 'src/assets')
    },
    filename: (req, file, callBack) => {
        callBack(null, `${file.originalname}`)
    }
  })
  const upload = multer({ storage: storage })

  
  app.get("/api/voyage", (req, res, next) => {
    Voyage.find().then(documents => {
      res.status(200).json({
        message: "Posts fetched successfully!",
        posts: documents
      });
    });
  });

  app.get("/api/existe/:id", (req, res, next)=> {
    Voyage.findOne({  id: req.params.id  }).then(documents => {
      if(documents==null){
       console.log(false);
  
       res.send(false);
      }else{
       console.log(true);
  
       res.send(true);
  
      }
    
  
    });
    
  
  });

app.post("/api/voyage",(req,res,next) => {
    const h = new Voyage ({

        id :  req.body.id,
        nom : req.body.nom,
        pays : req.body.pays,
        titre : req.body.titre,
        venteadultesingle : req.body.venteadultesingle,
        venteadultedble : req.body.venteadultedble,
        vente3rdad : req.body.vente3rdad,
        venteenfant2ad : req.body.venteenfant2ad,
        venteenfant1ad : req.body.venteenfant1ad,
        tarifbebe : req.body.tarifbebe,
        venteenfant12ans : req.body.venteenfant12ans,
        venteenfant6ans : req.body.venteenfant6ans,

        departvoyages : req.body.departvoyages,
        image : req.body.image,
        programme: req.body.programme,

    });
    h.save();
    res.status(201).json({
      message:"Voyage added successfully"    });
});



  app.delete("/api/posts/:id", (req, res, next) => {
    Voyage.deleteOne({ id: req.params.id }).then(result => {
      res.status(200).json({ message: "Voyage deleted!" });
    });
  });
  
  app.put("/api/voyageModif/:id", (req, res) => {
    console.log(req.params.id);

    const h = new Voyage({
  
        id :  req.body.id,
        nom : req.body.nom,
        pays : req.body.pays,
        titre : req.body.titre,
        venteadultesingle : req.body.venteadultesingle,
        venteadultedble : req.body.venteadultedble,
        vente3rdad : req.body.vente3rdad,
        venteenfant2ad : req.body.venteenfant2ad,
        venteenfant1ad : req.body.venteenfant1ad,
        tarifbebe : req.body.tarifbebe,
        venteenfant12ans : req.body.venteenfant12ans,
        venteenfant6ans : req.body.venteenfant6ans,
        departvoyages : req.body.departvoyages,
        image : req.body.image,
        programme: req.body.programme,


  });
  Voyage.findOneAndUpdate({

        _id: req.params.id
    }, { $set: req.body }, { new: true }, (err, doc) => {
        if (!err) { res.status(200).json({
          data:doc,
          message:"Voyage modified successfully"
      });
     }
        else {
            res.status(400).send(console.log("erreur de mise a jour" + err));
        }
    })

});

app.delete("/api/deleteall/:id", (req, res, next) => {
  Voyagehomeliset.remove({}).then(result => {
    res.status(200).json({ message: "Post deleted!" });
  });
});

app.post("/api/voyagehomelist",(req,res,next) => {
  const h = new Voyagehomeliset({

      id :  req.body.id,
      nom : req.body.nom,
      pays : req.body.pays,
      titre : req.body.titre,
      venteadultesingle : req.body.venteadultesingle,
      venteadultedble : req.body.venteadultedble,
      vente3rdad : req.body.vente3rdad,
      venteenfant2ad : req.body.venteenfant2ad,
      venteenfant1ad : req.body.venteenfant1ad,
      tarifbebe : req.body.tarifbebe,
      venteenfant12ans : req.body.venteenfant12ans,
      venteenfant6ans : req.body.venteenfant6ans,
    departvoyages : req.body.departvoyages,
      image : req.body.image

  });
  h.save();
  res.status(201).json({
      message:"fdvbfdbdf hdfvbjkd bdfj jdfkb jkdfb kjdf jkdf jk dfjk kdfj kjdf kjfd kjdf kjdf kjdf kjdf jk"
  });
});

app.get("/api/voyagehomelist", (req, res, next) => {
  Voyagehomeliset.find().then(documents => {
    res.status(200).json({
      message: "Posts fetched successfully!",
      posts: documents
    });
    console.log("-------------------------------------");

  });
});
  
  //******************** Reservation Hotel ********************* */

app.post("/api/reservation",(req,res,next) => {
  const r = new RVoyage({

    Etat:"En attente" ,
    Titre: req.body.Titre,
    Nom: req.body.Nom,
    Email: req.body.Email,
    Tel: req.body.Tel,
    Localisation:  req.body.Localisation,
    DateReservation:  req.body.DateReservation,
    DateDepart:  req.body.DateDepart,
    DatRetour:  req.body.DatRetour,
    Prix:  req.body.Prix,
    Chambre :req.body.Chambre 

  });
  r.save();
  res.status(201).json({
    message:"reservation ajoutée"
  });
});



app.delete("/api/DeleteReservation/:id", (req, res) => {

  RVoyage.findOneAndRemove(
      {
          _id: req.params.id,
      },

      (err, doc) => {
        if (!err) {
          res.status(200).json({ message: "Reservation deleted!" });
         }
        else {
        res.status(400).send({message:("error"+err)});
         }
      });
});

app.get('/api/ReservationVoyage', (req, res) => {

  RVoyage.find().then((RVoyage) => {
      if (RVoyage) {
          res.status(200).send(RVoyage);
      }
      else { console.log("not found" + err.message) }

  })

});

app.get('/api/ReservationVoyageAccepter', (req, res) => {
  
  RVoyage.find({Etat:"accepter",Payement:false},(err,ReservationVoyage) => {
    res.send(ReservationVoyage);
});

});

app.get('/api/UneReservation/:id', (req, res) => {
  
  RVoyage.findOne({_id: req.params.id},(err,RVoyage) => {
    res.status(200).send(RVoyage);
});

});


app.get('/api/ReservationVoyageEnAttente', (req, res) => {

  RVoyage.find({Etat:"En attente"},(err,ReservationVoyage) => {
    res.send(ReservationVoyage);
});

});



app.put('/api/ReservationAccepter/:id', (req, res) => {
  var resVoyage = {
    Etat: "accepter",
};

  RVoyage.findByIdAndUpdate({

      _id: req.params.id
  }, { $set: resVoyage }, { new: true }, (err, doc) => {
    if (!err) {
      res.status(200).json({
       data:doc,
       message:"Réservation accepter"
   });
      }
   else {
       res.status(400).send({message:("error"+err)});

   }
  })

});

app.put('/api/UpgradePayement/:id', (req, res) => {
  var resHotel = {
    Payement: true,
};

RVoyage.findByIdAndUpdate({

      _id: req.params.id
  }, { $set: resHotel }, { new: true }, (err, doc) => {
      if (!err) {
         res.status(200).json({
          data:doc,
          message:"Paiment réussi"
      });
         }
      else {
          res.status(400).send({message:("error"+err)});

      }
  })

});
module.exports = app;