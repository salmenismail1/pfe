const express = require("express");
const bodyParser = require("body-parser");

const multer = require('multer');

const app = express();
const { Hotel } = require('../Models/hotel');
const hotelhomeliset = require('../Models/hotelhomeliset');
const { Hotelhomeliset } = require('../Models/hotelhomeliset');

const { RHotel } = require('../Models/ReservationHotel');
//const nodemailer = require("nodemailer");

const hotel = require('../Models/hotel');

var inlineBase64 = require('nodemailer-plugin-inline-base64');
const nodemailer = require("nodemailer");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, 'src/assets')
    },
    filename: (req, file, callBack) => {
        callBack(null, `${file.originalname}`)
    }
  })
  
  const upload = multer({ storage: storage })
  
  app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );
    res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST,PUT, PATCH, DELETE, OPTIONS"
    );
    next();
  });


  
  app.post("/api/hotelhomelist",(req,res,next) => {
    const h = new Hotelhomeliset({

        id :  req.body.id,
        nom : req.body.nom,
        adultOnly : req.body.adultOnly,
        ville : req.body.ville,
        categorie : req.body.categorie,
        type : req.body.type,
        lpdvente : req.body.lpdvente,
        dpvente : req.body.dpvente,
        pcvente : req.body.pcvente,
        allinsoftvente : req.body.allinsoftvente,
        allinvente : req.body.allinvente,
        ultraallinvente : req.body.ultraallinvente,
        age_enf_gratuit : req.body.age_enf_gratuit,
        image:req.body.image

    });
    h.save();
    res.status(201).json({
        message:"fdvbfdbdf hdfvbjkd bdfj jdfkb jkdfb kjdf jkdf jk dfjk kdfj kjdf kjfd kjdf kjdf kjdf kjdf jk"
    });
});

  app.post("/api/posts", (req, res, next) => {
    const post = req.body;
    res.status(201).json({
      message: 'Post added successfully'
    });
  });
  app.get("/api/hotel", (req, res, next) => {
    hotel.Hotel.find().then(documents => {
      res.status(200).json({
        message: "Posts fetched successfully!",
        posts: documents
      });
    });
  });
  
  app.get("/api/hotelhomelist", (req, res, next) => {
    hotelhomeliset.Hotelhomeliset.find().then(documents => {
      res.status(200).json({
        message: "Posts fetched successfully!",
        posts: documents
      });

    });
  });
  app.post("/api/hotel",(req,res,next) => {
      const h = new Hotel({
  
          id :  req.body.id,
          nom : req.body.nom,
          titre:req.body.nom,
          adultOnly : req.body.adultOnly,
          ville : req.body.ville,
          categorie : req.body.categorie,
          type : req.body.type,
          lpdvente : req.body.lpdvente,
          dpvente : req.body.dpvente,
          pcvente : req.body.pcvente,
          allinsoftvente : req.body.allinsoftvente,
          allinvente : req.body.allinvente,
          ultraallinvente : req.body.ultraallinvente,
          age_enf_gratuit : req.body.age_enf_gratuit,
          image:req.body.image
  
      });
      h.save();
      res.status(201).json({
          message:"Hotel added successfully"
      });
  });
  
  app.delete("/api/posts/:id", (req, res, next) => {
    Hotel.deleteOne({ id: req.params.id }).then(result => {
      res.status(200).json({ message: "Soiree deleted!" });
    });
  });
  app.delete("/api/deleteall/:id", (req, res, next) => {
    Hotelhomeliset.remove({}).then(result => {
      res.status(200).json({ message: "Hotels deleted!" });
    });
  });
  
  
  app.put("/api/hotelModif/:id", (req, res) => {
    console.log(req.params.id);

    const h = new Hotel({
  
      id :  req.body.id,
      nom : req.body.nom,
      adultOnly : req.body.adultOnly,
      ville : req.body.ville,
      categorie : req.body.categorie,
      type : req.body.type,
      lpdvente : req.body.lpdvente,
      dpvente : req.body.dpvente,
      pcvente : req.body.pcvente,
      allinsoftvente : req.body.allinsoftvente,
      allinvente : req.body.allinvente,
      ultraallinvente : req.body.ultraallinvente,
      age_enf_gratuit : req.body.age_enf_gratuit,
      image:req.body.image

  });
  hotel.Hotel.findOneAndUpdate({

        _id: req.params.id
    }, { $set: req.body }, { new: true }, (err, doc) => {
        if (!err) {
           res.status(200).json({
            data:doc,
            message:"Hotel modified successfully"
        });
           }
        else {
            res.status(400).send({message:("error"+err)});
        }
    })

});

app.get("/api/existe/:id", (req, res, next)=> {
   hotel.Hotel.findOne({  id: req.params.id  }).then(documents => {
     if(documents==null){
      console.log(false);

      res.send(false);
     }else{
      console.log(true);

      res.send(true);

     }
   

   });
   

});
app.post("/api/pub", async (req, res) => {
  
  

  let item=req.body;
let image=item.image[0];
let categorie;
if(item.categorie=="1"){
  categorie='<span class="fa fa-star checked"></span><span class="fa fa-star"></span><span class="fa fa-star "></span><span class="fa fa-star "></span><span class="fa fa-star"></span>'
console.log(categorie);
}else if(item.categorie=="2"){
  categorie='<span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star "></span><span class="fa fa-star "></span><span class="fa fa-star"></span>'
console.log(categorie);
}else if(item.categorie=="3"){
  categorie='<span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star "></span><span class="fa fa-star"></span>'
console.log(categorie);
} else if(item.categorie=="4"){
  categorie='<span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star"></span>'
console.log(categorie);
}else{
  categorie='<span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span>'
  console.log(categorie);
}

  const maill = `
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
  <head>
  <base href='/'>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png" />
  <link rel="icon" type="image/png" href="./assets/img/favicon.png" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>Now UI Dashboard Angular by Creative Tim</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  <meta name="viewport" content="width=device-width" />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style type="text/css">
  .rating a {
    float: right;
    color: #aaa;
    text-decoration: none;
    font-size: 3em;
    transition: color .4s;
    color: orange;
 }
 

  body {
  margin: 0;
  padding: 0;
  }
  
  table,
  td,
  tr {
  vertical-align: top;
  border-collapse: collapse;
  }
  
  * {
  line-height: inherit;
  }
  
  a[x-apple-data-detectors=true] {
  color: inherit !important;
  text-decoration: none !important;
  }
  .checked{
    color:orange;
}
  </style>
 
  </head>
  <body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #FFFFFF;">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<table>

<tr>
<td width="50%">
<img src="cid:unique@kreata.ee"   alt="image" style="margin-top: 8px;width: 100%;height: 50%;"/>
</td>
<td width="50%">

<div class="row">

<img src="https://www.freedomtravel.tn/assets/images/logo_1_freedomtravel.png" width="100%" height="200px" />


</div>


<div class="row">
<h5 >${item.nom}
<div class="rating">
`
+
categorie
+
`
 
</div>
</h5>
</div>

<div class="row">

<i class="fa fa-map-marker cyan-text" >${item.nom}</i>
</div>

<div class="row">

<input  type="button"  value="réserver"  style="margin:auto;"/>
</div>


</td>
</tr>


</table>

  </body>
  </html>
  `;
 
  let transporter = nodemailer.createTransport({
 
  service: 'gmail',
  auth: {
  user: "njeimi27@gmail.com",
  pass: "njeimiphp"
  }
  });
 
 
  /***********//***********//***********/
  let mailoptions = {
  from: 'njeimi27@gmail.com',
  to: 'njeimi26@gmail.com',
  subject: 'Formation Register',
  html: maill,
  attachments: [{
    filename: 'image.png',
    path: ''+image,
    cid: 'unique@kreata.ee' //same cid value as in the html img src
}]

 
  };
  transporter.use('compile', inlineBase64({cidPrefix: 'somePrefix_'}));

  transporter.sendMail(mailoptions, (error, info) => {
  if (error) {
  return console.log(error);
  }
  else {
  console.log("email sent");
  }
  });
 
 });



 app.post("/api/reservation",(req,res) => {
  const r = new RHotel({

    IdClient:req.body.IdClient,
    Etat:"En attente" ,
    Payement:false ,
    Titre: req.body.Titre,
    Nom: req.body.Nom,
    Email: req.body.Email,
    Tel: req.body.Tel,
    Localisation:  req.body.Localisation,
    DateReservation:  req.body.DateReservation,
    DateDepart:  req.body.DateDepart,
    DatRetour:  req.body.DatRetour,
    Prix:  req.body.Prix,
    Chambre :req.body.Chambre 

  });
  r.save();
  res.status(201).json({
      message:"reservation ajoutée"
  });
});



app.delete("/api/DeleteReservation/:id", (req, res) => {

  RHotel.findOneAndRemove(
      {
          _id: req.params.id,
      },

      (err, doc) => {
          if (!err) {
              res.status(200).json({ message: "Reservation deleted!" });
          }
          else {
            res.status(400).send({message:("error"+err)});
          }
      });
});

app.get('/api/ReservationHotel', (req, res) => {

  RHotel.find().then((ReservationHotel) => {
      if (ReservationHotel) {
          res.status(200).send(ReservationHotel);
      }
      else { console.log("not found" + err.message) }

  })

});

app.get('/api/ReservationHotelAccepter', (req, res) => {

  RHotel.find({Etat:"accepter",Payement:false},(err,ReservationHotel) => {
    res.send(ReservationHotel);
});

});


app.get('/api/ReservationHotelEnAttente', (req, res) => {

  RHotel.find({Etat:"En attente"},(err,ReservationHotel) => {
    res.send(ReservationHotel);
});

});



app.put('/api/ReservationAccepter/:id', (req, res) => {
  var resHotel = {
    Etat: "accepter",
};

    RHotel.findByIdAndUpdate({

      _id: req.params.id
  }, { $set: resHotel }, { new: true }, (err, doc) => {
      if (!err) {
         res.status(200).json({
          data:doc,
          message:"Réservation accepter"
      });
         }
      else {
          res.status(400).send({message:("error"+err)});

      }
  })

});

app.put('/api/UpgradePayement/:id', (req, res) => {
  var resHotel = {
    Payement: true,
};

    RHotel.findByIdAndUpdate({

      _id: req.params.id
  }, { $set: resHotel }, { new: true }, (err, doc) => {
      if (!err) {
         res.status(200).json({
          data:doc,
          message:"Paiment réussi"
      });
         }
      else {
          res.status(400).send({message:("error"+err)});

      }
  })

});

app.put("/ReservationHotelModif/:id", (req, res) => {
  
  var resHotel = {
      Nom: req.body.Nom,
      Email: req.body.Email,
      Date:  req.body.Date

  };

  RHotel.findByIdAndUpdate({

      _id: req.params.id
  }, { $set: resHotel }, { new: true }, (err, doc) => {
      if (!err) { res.status(200).send(doc); }
      else {
          res.status(400).send(console.log("erreur de mise a jour" + err));
      }
  })

});

app.post('/send/:email', (req, res) => {

const h = new Hotel({

  id :  req.body.id,
  nom : req.body.nom,
  adultOnly : req.body.adultOnly,
  ville : req.body.ville,
  categorie : req.body.categorie,
  type : req.body.type,
  lpdvente : req.body.lpdvente,
  dpvente : req.body.dpvente,
  pcvente : req.body.pcvente,
  allinsoftvente : req.body.allinsoftvente,
  allinvente : req.body.allinvente,
  ultraallinvente : req.body.ultraallinvente,
  age_enf_gratuit : req.body.age_enf_gratuit,
  image:req.body.image

});

const output = `
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width" name="viewport"/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<title></title>
<style type="text/css">

  .rating a {
  float: center;
  color: #aaa;
  text-decoration: none;
  font-size: 3em;
  transition: color .4s;
  color: orange;
}

</style>
</head>
<body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #FFFFFF;">
  <img style="margin: 1%; text-align: right;width: 5%;height: 5%" src="https://www.freedomtravel.tn/assets/images/logo_1_freedomtravel.png" width="100%" height="200px" />
  <h1 style="color: blue"><center>Promotion  !!!!!</center></h1>
  <h2 style="color: orangered"><center>${h.nom}</center></h2>
  <center><img src="cid:unique@kreata.ee" /></center>

  <div class="row">
    <h5 ><center>
    <div class="rating">
       <a href="#5" title="Donner 5 étoiles">☆</a>
       <a href="#4" title="Donner 4 étoiles">☆</a>
       <a href="#3" title="Donner 3 étoiles">☆</a>
       <a href="#2" title="Donner 2 étoiles">☆</a>
       <a href="#1" title="Donner 1 étoile">☆</a>
    </div></center>
    </h5>
  </div>

  <h2><center> Prix : ${h.lpdvente} DT </center></h2>

</body>
</html>
`;

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'salmenismail3@gmail.com',
    pass: 'salmen.ismail3'
  }
});

// setup email data with unicode symbols
let mailOptions = {
    from: '"FreedomTravel" <salmenismail3@gmail.com>', // sender address
    to: req.params.email, // list of receivers
    subject: 'Promotion du jour', // Subject line
    text: 'Hello world?', // plain text body
    html: output,
    attachments: [{
      filename: 'image.png',
      path: ''+image,
      cid: 'unique@kreata.ee' //same cid value as in the html img src
  }]
};
transporter.use('compile', inlineBase64({cidPrefix: 'somePrefix_'}));
// send mail with defined transport object
transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
        return console.log(error);
    }
    console.log('Message sent: %s', info.messageId);   
    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

    res.render('contact', {msg:'Email has been sent'});
});
});

module.exports = app;
