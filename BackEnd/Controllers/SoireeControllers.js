const express = require("express");
const bodyParser = require("body-parser");


const app = express();
const { ReservationSoiree } = require('../Models/ReservationSoiree');
const multer = require('multer');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
const { Soiree } = require('../Models/Soiree');
const { Soireehomeliset }  = require('../Models/soirehomeliset');


const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, 'src/assets')
    },
    filename: (req, file, callBack) => {
        callBack(null, `${file.originalname}`)
    }
  })
  
  const upload = multer({ storage: storage })
  
  app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );
    res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST,PUT, PATCH, DELETE, OPTIONS"
    );
    next();
  });
  
  app.get("/api/soiree", (req, res, next) => {
    Soiree.find().then(documents => {
      res.status(200).json({
        message: "Posts fetched successfully!",
        posts: documents
      });
    });
  });
  app.get("/api/existe/:id", (req, res, next)=> {
    Soiree.findOne({  id: req.params.id  }).then(documents => {
      if(documents==null){
       console.log(false);
  
       res.send(false);
      }else{
       console.log(true);
  
       res.send(true);
  
      }
    
  
    });
    
  
  });

  app.get("/api/existe/:id", async (req, res) => {
    Soiree.find({  id: req.params.id  }).then(documents => {
      if (!err) { res.status(200).send(doc); }
          else {
              res.status(400).send(console.log("erreur de mise a jour" + err));
          }
     });
   
  
  });

app.post("/api/soiree",(req,res,next) => {
    const h = new Soiree ({

        id :  req.body.id,
        titre : req.body.titre,
        image : req.body.image,
        date : req.body.date,
        venteadultesingle : req.body.venteadultesingle,
        venteadultedble : req.body.venteadultedble,
        vente3rdad : req.body.vente3rdad,
        venteenfant2ad : req.body.venteenfant2ad,
        tarifbebe : req.body.tarifbebe,
        venteenfant1ad : req.body.venteenfant1ad,
        programme: req.body.programme,


    });
    h.save();
    res.status(201).json({
      message:"Soiree added successfully"
    });
});


  app.delete("/api/posts/:id", (req, res, next) => {
    Soiree.deleteOne({ id: req.params.id }).then(result => {
      res.status(200).json({ message: "Soiree deleted!" });
    });
  });
  
  app.put("/api/soireeModif/:id", (req, res) => {
    console.log(req.params.id);

    const h = new Soiree({
  
        id :  req.body.id,
        titre : req.body.titre,
        image : req.body.image,
        date : req.body.date,
        venteadultesingle : req.body.venteadultesingle,
        venteadultedble : req.body.venteadultedble,
        vente3rdad : req.body.vente3rdad,
        venteenfant2ad : req.body.venteenfant2ad,
        tarifbebe : req.body.tarifbebe,
        venteenfant1ad : req.body.venteenfant1ad,
        programme: req.body.programme,

  });
Soiree.findOneAndUpdate({

        _id: req.params.id
    }, { $set: req.body }, { new: true }, (err, doc) => {
      if (!err) {
        res.status(200).json({
         data:doc,
         message:"Soiree modified successfully"
     });
        }
     else {
         res.status(400).send({message:("error"+err)});
     }
    })

});

app.delete("/api/deleteall/:id", (req, res, next) => {
  Soireehomeliset.remove({}).then(result => {
    res.status(200).json({ message: "Soiree deleted!" });
  });
});

app.post("/api/soireehomelist",(req,res,next) => {
  const h = new Soireehomeliset({

      id :  req.body.id,
      titre : req.body.titre,
      image : req.body.image,
      date : req.body.date,
      venteadultesingle : req.body.venteadultesingle, 
      venteadultedble : req.body.venteadultedble,
      vente3rdad : req.body.vente3rdad,
      venteenfant2ad : req.body.venteenfant2ad,
      tarifbebe : req.body.tarifbebe,
      venteenfant1ad : req.body.venteenfant1ad

  });
  h.save();
  res.status(201).json({
      message:"fdvbfdbdf hdfvbjkd bdfj jdfkb jkdfb kjdf jkdf jk dfjk kdfj kjdf kjfd kjdf kjdf kjdf kjdf jk"
  });
});

app.get("/api/soireehomelist", (req, res, next) => {
  Soireehomeliset.find().then(documents => {
    res.status(200).json({
      message: "Posts fetched successfully!",
      posts: documents
    });
    console.log("-------------------------------------");

  });
});

  //******************** Reservation Soiree ********************* */

  app.post("/api/reservation",(req,res,next) => {
    const s = new ReservationSoiree({
        IdClient:req.body.IdClient,
        Etat:"En attente" ,
        Payement:false ,
        Titre: req.body.Titre,
        Nom: req.body.Nom,
        Email: req.body.Email,
        Tel: req.body.Tel,
        DateDepart:  req.body.DateDepart,
        DatRetour:  req.body.DatRetour,
        Prix:  req.body.Prix,
        Chambre :req.body.Chambre 
  
    });
    s.save();
    res.status(201).json({
      message:"reservation ajoutée"
    });
  });
  
  
  
  app.delete("/api/reservation/:id", (req, res) => {
  
    ReservationSoiree.findOneAndRemove(
        {
          _id: req.params.id,
        },
  
        (err, doc) => {
          if (!err) {
            res.status(200).json({ message: "Reservation deleted!" });
        }
        else {
          res.status(400).send({message:("error"+err)});
        }
        });
  });
  
  app.get('/api/ReservationSoiree', (req, res) => {
  
    ReservationSoiree.find().then((RVoyage) => {
        if (RVoyage) {
            res.status(200).send(RVoyage);
        }
        else { console.log("not found" + err.message) }
  
    })
  
  });
  

  app.get('/api/ReservationSoireeAccepter', (req, res) => {
  
    ReservationSoiree.find({Etat:"accepter",Payement:false},(err,ReservationHotel) => {
      res.send(ReservationHotel);
  });
  
  });
  

  app.get('/api/ReservationSoireeEnAttente', (req, res) => {
  
    ReservationSoiree.find({Etat:"En attente"},(err,ReservationHotel) => {
      res.send(ReservationHotel);
  });
  
  });
  
  

  app.put('/api/ReservationAccepter/:id', (req, res) => {
    var resHotel = {
      Etat: "accepter",
  };

  ReservationSoiree.findByIdAndUpdate({

        _id: req.params.id
    }, { $set: resHotel }, { new: true }, (err, doc) => {
      if (!err) {
        res.status(200).json({
         data:doc,
         message:"Réservation accepter"
     });
        }
     else {
         res.status(400).send({message:("error"+err)});

     }
    })
  
  });

  app.put('/api/UpgradePayement/:id', (req, res) => {
    var resHotel = {
      Payement: true,
  };
  
  ReservationSoiree.findByIdAndUpdate({
  
        _id: req.params.id
    }, { $set: resHotel }, { new: true }, (err, doc) => {
        if (!err) {
           res.status(200).json({
            data:doc,
            message:"Paimment réussi"
        });
           }
        else {
            res.status(400).send({message:("error"+err)});
  
        }
    })
  
  });




module.exports = app;