const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const {Contact} = require("../Models/Contact");
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST,PUT, PATCH, DELETE, OPTIONS"
  );
  next();
});


app.get("/api/contact/:response", (req, res, next) => {
  console.log(req.params.response);

  if(req.params.response=="no response"){
    Contact.find({ response: "" }).then(documents => {
      console.log(documents);

      res.status(200).json({
        message: "Posts fetched successfully!",
        posts: documents
      });
    });

  }else{
  Contact.find({ response: { $ne: "" } }).then(documents => {
      res.status(200).json({
        message: "Posts fetched successfully!",
        posts: documents
      });
    });
  }
    
  });


app.post("/api/contact",(req,res,next) => {
    const h = new Contact ({

        idClient :  req.body.idClient,
        nom : req.body.nom,
        email:req.body.email,
        message : req.body.message,
        sujet : req.body.sujet,
        tel : req.body.tel,
        response:"",

    });
    h.save();
    res.status(201).json({
        message:"fdvbfdbdf hdfvbjkd bdfj jdfkb jkdfb kjdf jkdf jk dfjk kdfj kjdf kjfd kjdf kjdf kjdf kjdf jk"
    });
});

app.delete("/api/posts/:id", (req, res, next) => {
  Contact.deleteOne({ _id: req.params.id }).then(result => {
    res.status(200).json({ message: "Post deleted!" });
  });
});

app.put("/api/contactModif/:id", (req, res) => {
  console.log(req.params.id);

  const h = new Contact({
    idClient :  req.body.idClient,
    nom : req.body.nom,
    email:req.body.email,
    message : req.body.message,
    sujet : req.body.sujet,
    tel : req.body.tel,
    response : req.body.response,


});
Contact.findOneAndUpdate({

      _id: req.params.id
  }, { $set: req.body }, { new: true }, (err, doc) => {
      if (!err) { res.status(200).send(doc); }
      else {
          res.status(400).send(console.log("erreur de mise a jour" + err));
      }
  })

});

module.exports = app;