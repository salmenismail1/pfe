const express = require("express");
const bodyParser = require("body-parser");


const app = express();
const { Omra } = require('../Models/Omra');
const { ROmra } = require('../Models/ReservationOmra');
const omra = require('../Models/Omra');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST,PUT, PATCH, DELETE, OPTIONS"
  );
  next();
});


app.post("/api/AjoutOmra",(req,res,next) => {
    
  const h = new Omra({

      id :  req.body.id,
      agent : req.body.agent,
      programme : req.body.programme,
      date : req.body.date,
      titre : req.body.titre,
      prix : req.body.prix,
      image:req.body.image

  });
  h.save();
  res.status(201).json({
      message:"L'ajout de l'hotel avec succée"
  });
});



app.get("/api/omra", (req, res, next) => {
    omra.Omra.find().then(documents => {
      res.status(200).json({
        message: "Posts fetched successfully!",
        posts: documents
      });
    });
  });

  app.put("/api/omraModif/:id", (req, res) => {
    console.log(req.params.id);

    const h = new Omra({
      id :  req.body.id,
      agent : req.body.agent,
      programme : req.body.programme,
      date : req.body.date,
      titre : req.body.titre,
      prix : req.body.prix,
      image:req.body.image
       

  });
  Omra.findOneAndUpdate({

        _id: req.params.id
    }, { $set: req.body }, { new: true }, (err, doc) => {
        if (!err) { res.status(200).send(doc); }
        else {
            res.status(400).send(console.log("erreur de mise a jour" + err));
        }
    })

});
app.delete("/api/posts/:id", (req, res) => {

  Omra.findOneAndRemove(
      {
          _id: req.params.id,
      },

      (err, doc) => {
          if (!err) {
              res.status(200).send(doc);
              console.log(doc);
          }
          else { console.log('Error in Hotel Delete :' + err) }
      });
});



//******************** Reservation Omra ********************* */

app.post("/api/reservationOmra",(req,res) => {
    const r = new ROmra({
        IdClient:req.body.IdClient,
        Etat:"En attente" ,
        Payement:false ,
        Titre: req.body.Titre,
        Nom: req.body.Nom,
        Email: req.body.Email,
        Tel: req.body.Tel,
        DateDepart:  req.body.DateDepart,
        DatRetour:  req.body.DatRetour,
        Prix:  req.body.Prix,
        Chambre :req.body.Chambre  
  
    });
    r.save();
    res.status(201).json({
        message:"L'ajout du reservation de Omra avec succée"
    });
  });
  
  
  
  app.delete("/api/DeleteReservationOmra/:id", (req, res) => {
  
    ROmra.findOneAndRemove(
        {
            _id: req.params.id,
        },
  
        (err, doc) => {
            if (!err) {
                res.status(200).send(doc);
                console.log(doc);
            }
            else { console.log('Error in Reservation Omra  Delete :' + err) }
        });
  });
  
  app.get('/api/ListerReservationOmra', (req, res) => {
  
    ROmra.find().then((ReservationHotel) => {
        if (ReservationHotel) {
            res.status(200).send(ReservationHotel);
        }
        else { console.log("not found" + err.message) }
  
    })
  
  });
  app.get('/api/ReservationOmraAccepter', (req, res) => {

    ROmra.find({Etat:"accepter",Payement:false},(err,ReservationHotel) => {
      res.send(ReservationHotel);
  });
  
  });
  
  
  app.get('/api/ReservationOmraEnAttente', (req, res) => {
  
    ROmra.find({Etat:"En attente"},(err,ReservationHotel) => {
      res.send(ReservationHotel);
  });
  
  });
  
  
  
  app.put('/api/ReservationAccepter/:id', (req, res) => {
    var resHotel = {
      Etat: "accepter",
  };
  
  ROmra.findByIdAndUpdate({
  
        _id: req.params.id
    }, { $set: resHotel }, { new: true }, (err, doc) => {
        if (!err) {
           res.status(200).json({
            data:doc,
            message:"Réservation accepter"
        });
           }
        else {
            res.status(400).send({message:("error"+err)});
  
        }
    })
  
  });
  
  app.put('/api/UpgradePayement/:id', (req, res) => {
    var resHotel = {
      Payement: true,
  };
  
  ROmra.findByIdAndUpdate({
  
        _id: req.params.id
    }, { $set: resHotel }, { new: true }, (err, doc) => {
        if (!err) {
           res.status(200).json({
            data:doc,
            message:"Paiment réussi"
        });
           }
        else {
            res.status(400).send({message:("error"+err)});
  
        }
    })
  
  });

  app.get("/api/existe/:id", (req, res, next)=> {
    Omra.findOne({  id: req.params.id  }).then(documents => {
      if(documents==null){
       console.log(false);
 
       res.send(false);
      }else{
       console.log(true);
 
       res.send(true);
 
      }
    
 
    });
    
 
 });

  app.put("/ReservationOmraModif/:id", (req, res) => {
    
    var resOmra = {
        Nom: req.body.Nom,
        Email: req.body.Email,
        Date:  req.body.Date

    };

    ROmra.findByIdAndUpdate({

        _id: req.params.id
    }, { $set: resOmra }, { new: true }, (err, doc) => {
        if (!err) { res.status(200).send(doc); }
        else {
            res.status(400).send(console.log("erreur de mise a jour" + err));
        }
    })

});





module.exports = app;