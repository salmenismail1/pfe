const mongooose = require('mongoose');
const { Chambre } = require('./Chambre');

const ReservationVoyage = new mongooose.Schema(
    {
        Etat: {
            type: String,
            require: true,
            trim: true
        },
        Titre: {
            type: String,
            require: true,
            trim: true
        },
        Nom: {
            type: String,
            require: true,
            trim: true
        },
        Email: {
            type: String,
            require: true,
            trim: true
        },
        Tel: {
            type: String,
            require: true,
            trim: true
        },
        Localisation: {
            type: String,
            require: true,
            trim: true
        },
        DateReservation: {
            type: Date,
            require: true,
            trim: true
        },
        DateDepart: {
            type: Date,
            require: true,
            trim: true
        },
        DatRetour: {
            type: Date,
            require: true,
            trim: true
        },
        Prix: {
            type: Number,
            require: true,
            trim: true
        },
        Chambre :[
            
                {
                    num: {
                        type: String,
                        require: true
            
                    },
                    adulte: {
                        type: Array,
                        require: true
                    },
                    enfant: {
                        type: Array,
                        require: true
                    },
                    bb: {
                        type: Array,
                        require: true
                    },
                }
            
        ]
    }

);


RVoyage = mongooose.model('ReservationVoyage', ReservationVoyage);
module.exports = { RVoyage };