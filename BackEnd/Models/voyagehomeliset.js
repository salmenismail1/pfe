const mongooose = require('mongoose');


const Voyagehomelisetschema = new mongooose.Schema(
    {
        
        id: {
            type: String,
            require: true

        },
        nom: {
            type: String,
            require: true
        },
        pays: {
            type: String,
            require: true
        },
        titre: {
            type: String,
            require: true
        },
        prix: {
            type: Number,
            require: true
        },
        departvoyages: 
            {
                datedepart:String,
                dateretour:String
            }
        ,
        image: {
            type: Array,
            require: true
        }
        
 
   
        
    }

);

Voyagehomeliset = mongooose.model('Voyagehomeliset', Voyagehomelisetschema);
module.exports = { Voyagehomeliset };



