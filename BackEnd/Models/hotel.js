const mongooose = require('mongoose');


const Hotelschema = new mongooose.Schema(
    {
        
        id: {
            type: String,
            require: true

        },
        
        nom: {
            type: String,
            require: true
        },
        adultOnly: {
            type: String,
            require: true
        },
        ville: {
            type: String,
            require: true
        },
        categorie: {
            type: String,
            require: true
        },
        type: {
            type: String,
            require: true
        },
        lpdvente: {
            type: Number,
            require: true
        },
        dpvente: {
            type: Number,
            require: true
        },
        pcvente: {
            type: Number,
            require: true
        },
        allinsoftvente: {
            type: Number,
            require: true
        },
        allinvente: {
            type: Number,
            require: true
        },
        ultraallinvente: {
            type: Number,
            require: true
        },
        age_enf_gratuit: {
            type: Number,
            require: true
        },
        image: {
            type: Array,
            require: true
        }
        
    }

);

Hotel = mongooose.model('Hotel', Hotelschema);
module.exports = { Hotel };
