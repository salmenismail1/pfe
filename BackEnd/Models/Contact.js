
const mongooose = require('mongoose');


const Contactschema = new mongooose.Schema(
    {
        idClient: {
            type: String,
            require: true
        },
        nom: {
            type: String,
            require: true
        },
        email: {
            type: String,
            require: true

        },
        message: {
            type: String,
            require: true

        },
     

        sujet:{
            type: String,
            require: true
        },
        tel: {
            type: Number,
            require: true

        },
        response: {
            type: String,
            require: true

        }
        
    }

);

Contact = mongooose.model('Contact', Contactschema);
module.exports = { Contact };
