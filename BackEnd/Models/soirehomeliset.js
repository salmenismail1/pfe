const mongooose = require('mongoose');


const Soireehomelisetschema = new mongooose.Schema(
    {
          
        id: {
            type: String,
            require: true

        },
        titre: {
            type: String,
            require: true
        },
        image: {
            type: Array,
            require: true
        },
        date: {
            type: String,
            require: true
        },
        venteadultesingle: {
            type: Number,
            require: true
        },
        venteadultedble: {
            type: Number,
            require: true
        },
        vente3rdad: {
            type: Number,
            require: true
        },
        venteenfant2ad: {
            type: Number,
            require: true
        },
        tarifbebe: {
            type: Number,
            require: true
        },
        venteenfant1ad: {
            type: Number,
            require: true
        },
        
    }

);

Soireehomeliset = mongooose.model('Soireehomeliset', Soireehomelisetschema);
module.exports = { Soireehomeliset };


