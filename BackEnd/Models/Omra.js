const mongooose = require('mongoose');


const Omraschema = new mongooose.Schema(
    {
        
        id: {
            type: String,
            require: true

        },
        
        agent: {
            type: String,
            require: true
        },
        programme: {
            type: String,
            require: true
        },
        date: {
            type: String,
            require: true
        },
        titre: {
            type: String,
            require: true
        },
        prix: {
            type: Number,
            require: true
        },
        image: {
            type: Array,
            require: true
        }
    }

);

Omra = mongooose.model('Omra', Omraschema);
module.exports = { Omra };
