const mongooose = require('mongoose');


const ReservationCircuitschema = new mongooose.Schema(
    {
        Etat: {
            type: String,
            require: true,
            trim: true
        },
        Titre: {
            type: String,
            require: true,
            trim: true
        },
        Nom: {
            type: String,
            require: true,
            trim: true
        },
        Email: {
            type: String,
            require: true,
            trim: true
        },
        Num: {
            type: String,
            require: true,
            trim: true
        },
        DateDepart: {
            type: Date,
            require: true,
            trim: true
        },
        DatRetour: {
            type: Date,
            require: true,
            trim: true
        },
        Prix: {
            type: Number,
            require: true,
            trim: true
        },
        Chambre :[
            
                {
                    num: {
                        type: String,
                        require: true
            
                    },
                    adulte: {
                        type: Array,
                        require: true
                    },
                    enfant: {
                        type: Array,
                        require: true
                    },
                    bb: {
                        type: Array,
                        require: true
                    },
                }
            
        ]
    }

);


RCircuit = mongooose.model('ReservationCircuit', ReservationCircuitschema);
module.exports = { RCircuit };
