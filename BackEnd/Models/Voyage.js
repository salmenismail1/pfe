const mongooose = require('mongoose');


const Voyageschema = new mongooose.Schema(
    {
        
        id: {
            type: String,
            require: true

        },
        nom: {
            type: String,
            require: true
        },
        pays: {
            type: String,
            require: true
        },
        titre: {
            type: String,
            require: true
        },
        
        venteadultesingle: {
            type: Number,
            require: true
        },
        venteadultedble: {
            type: Number,
            require: true
        },
        vente3rdad: {
            type: Number,
            require: true
        },
        venteenfant2ad: {
            type: Number,
            require: true
        },
        venteenfant1ad: {
            type: Number,
            require: true
        },
        tarifbebe: {
            type: Number,
            require: true
        },
        venteenfant12ans: {
            type: Number,
            require: true
        },
        venteenfant6ans: {
            type: Number,
            require: true
        },
        departvoyages: 
            {
                datedepart:String,
                dateretour:String
            }
        ,
        image: {
            type: Array,
            require: true
        },
        programme: 
        {
            programme:String,
        }
        
 
   
        
    }

);

Voyage = mongooose.model('Voyage', Voyageschema);
module.exports = { Voyage };



