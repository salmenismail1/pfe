const mongooose = require('mongoose');


const ReservationOmraschema = new mongooose.Schema(
    {
        IdClient: {
            type: String,
            require: true,
            trim: true
        },
        Etat: {
            type: String,
            require: true,
            trim: true
        },
        Payement: {
            type: Boolean,
            require: true,
            trim: true
        },
        Titre: {
            type: String,
            require: true,
            trim: true
        },
        Nom: {
            type: String,
            require: true,
            trim: true
        },
        Email: {
            type: String,
            require: true,
            trim: true
        },
        Tel: {
            type: String,
            require: true,
            trim: true
        },
        DateDepart: {
            type: Date,
            require: true,
            trim: true
        },
        DatRetour: {
            type: Date,
            require: true,
            trim: true
        },
        Prix: {
            type: Number,
            require: true,
            trim: true
        },
        Chambre :[
            
                {
                    num: {
                        type: String,
                        require: true
            
                    },
                    adulte: {
                        type: Array,
                        require: true
                    },
                    enfant: {
                        type: Array,
                        require: true
                    },
                    bb: {
                        type: Array,
                        require: true
                    },
                }
            
        ]
    }

);

ROmra = mongooose.model('ReservationOmra', ReservationOmraschema);
module.exports = { ROmra };
