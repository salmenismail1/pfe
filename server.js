const app = require("./BackEnd/app");
const debug = require("debug")("node-angular");
const http = require("http");
const socket = require('socket.io');


const normalizePort = val => {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
};

const onError = error => {
  if (error.syscall !== "listen") {
    throw error;
  }
  const bind = typeof addr === "string" ? "pipe " + addr : "port " + port;
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
};

const onListening = () => {
  const addr = server.address();
  const bind = typeof addr === "string" ? "pipe " + addr : "port " + port;
  debug("Listening on " + bind);
};

const port = normalizePort(process.env.PORT || "3000");
app.set("port", port);

const server = http.createServer(app);
server.on("error", onError);
server.on("listening", onListening);
server.listen(port);

const io = socket(server);

io.sockets.on('connection', (socket) => {
    console.log(`new connection id: ${socket.id}`);
    sendData(socket);
})




const Voyage= require('./BackEnd/Controllers/VoyageControllers');
const Hotel= require('./BackEnd/Controllers/HotelControllers');
const circuit= require('./BackEnd/Controllers/CircuitControllers');
const soiree= require('./BackEnd/Controllers/SoireeControllers');
const omra= require('./BackEnd/Controllers/OmraControllers');
const user= require('./BackEnd/Controllers/UserController');
const contact= require('./BackEnd/Controllers/ContactController');
const chat= require('./BackEnd/Controllers/ChatController');

const { Circuit } = require('./BackEnd/Models/Circuit');

app.use('/voyage',Voyage);
app.use('/hotel',Hotel);
app.use('/Circuit',circuit);
app.use('/omra',omra);
app.use('/soiree',soiree);
app.use('/user',user);
app.use('/contact',contact);
app.use('/chat',chat);

const { RVoyage } = require('./BackEnd/Models/ReservationVoyage');
const { RHotel } = require('./BackEnd/Models/ReservationHotel');
const { RCircuit } = require('./BackEnd/Models/ReservationCircuit');
const { ReservationSoiree } = require('./BackEnd/Models/ReservationSoiree');
const { Contact } = require('./BackEnd/Models/Contact');
const { Chat } = require('./BackEnd/Models/chat');



function sendData(socket){

  let nbHotels;
  let nbVoyages;
  let nbCircuits;
  let nbSoiree;
 /*
  Circuit.find({  venteadultesingle: { $gt: 0 }  }).then(documents => {
   console.log(documents.length);
  });*/

     
      
      RHotel.find({Etat:"accepter",Payement:true},(err,ReservationHotel) => {
        let tab=[0,0,0,0,0,0,0,0,0,0,0,0];
        if(ReservationHotel!=undefined){
          nbHotels=ReservationHotel.length;
          socket.emit('data6',[nbHotels,nbVoyages,nbCircuits,nbSoiree,0]);
  
          for (let index = 0; index < ReservationHotel.length; index++) {
           let  element =parseInt(ReservationHotel[index].DateDepart.toISOString().substr(5,2));
           tab[element-1]=tab[element-1]+1;
      
          }
          socket.emit('data1', tab);
        }
      
    
      });
    
      
      RVoyage.find({Etat:"accepter",Payement:true},(err,ReservationHotel) => {
        let tab=[0,0,0,0,0,0,0,0,0,0,0,0];
        if(ReservationHotel!=undefined){

        nbVoyages=ReservationHotel.length;
        socket.emit('data6',[nbHotels,nbVoyages,nbCircuits,nbSoiree,0]);

        for (let index = 0; index < ReservationHotel.length; index++) {
         let  element =parseInt(ReservationHotel[index].DateDepart.toISOString().substr(5,2));
         tab[element-1]=tab[element-1]+1;
    
        }
        socket.emit('data2', tab);
      }
      });
    
      RCircuit.find({Etat:"accepter",Payement:true},(err,ReservationHotel) => {
        let tab=[0,0,0,0,0,0,0,0,0,0,0,0];
        if(ReservationHotel!=undefined){

        nbCircuits=ReservationHotel.length;
        socket.emit('data6',[nbHotels,nbVoyages,nbCircuits,nbSoiree,0]);

        for (let index = 0; index < ReservationHotel.length; index++) {
         let  element =parseInt(ReservationHotel[index].DateDepart.toISOString().substr(5,2));
         tab[element-1]=tab[element-1]+1;
    
        }
        socket.emit('data3', tab);
      }
      });
      ReservationSoiree.find({Etat:"accepter",Payement:true},(err,ReservationHotel) => {
        let tab=[0,0,0,0,0,0,0,0,0,0,0,0];
        if(ReservationHotel!=undefined){

        nbSoiree=ReservationHotel.length;
        socket.emit('data6',[nbHotels,nbVoyages,nbCircuits,nbSoiree,0]);

        for (let index = 0; index < ReservationHotel.length; index++) {
         let  element =parseInt(ReservationHotel[index].DateDepart.toISOString().substr(5,2));
         tab[element-1]=tab[element-1]+1;
    
        }
        socket.emit('data5', tab);
      }
      });

      Contact.find({ response: { $ne: "" } },(err,data) => {
        if(data!=undefined){

        socket.emit('notif',data );
        }
      });
      
      Chat.find((err,data) => {
        if(data!=undefined){

        socket.emit('chat',data );
        }
      });
  



  socket.emit('data4', Array.from({length: 12}, () => Math.floor(Math.random() * 590)+ 10));
  

setTimeout(() => {
sendData(socket);
}, 20000);
}